# Algebra und Zahlentheorie -- Übungsblatt 7

\newcommand\PP{\mathbb{P}}
\newcommand\Nil{\mathrm{Nil}}

## Aufgabe 1

\paragraph{Lemma:} Seien $R, S$ kommutative Ringe. Dann gilt
$$(R \times S)[x] \cong R[x] \times S[x].$$
\paragraph{Beweis.} Betrachte die Inklusion $\iota : R \times S \xhookrightarrow
R[x] \times S[x]$. Die universelle Eigenschaft des Polynomrings erweitert
$\iota$ zu einem Ringhomomorphismus $\vfi : (R \times S)[x] \to R[x] \times
S[x]$ mit $\vfi(x) = (x, x)$.

Für die Injektivität sei $f = \sum_{i=0}^{k} (r_i, s_i)x^i \in (R \times S)[x]$
mit $(r_i, s_i) \in R \times S$ für $i \in \set{0, \dots, k}$.
Dann gilt
$$\begin{aligned}
&\vfi(f) = \sum_{i=0}^{k} \vfi(r_i, s_i) \vfi(x)^i = \sum_{i=0}^{k} (r_ix^i,
s_ix^i) = \left( \sum_{i=0}^{k} r_ix^i, \sum_{i=0}^{k} s_ix^i \right) = (0, 0)
\\
\iff &(r_i, s_i) = (0, 0) \text{ für } i \in \set{0, \dots, k} \\
\iff &f = 0.
\end{aligned}$$
Für die Surjektivität seien $f_R = \sum_{i=0}^{k} r_ix^i \in R[x]$ und $f_S =
\sum_{i=0}^{l} s_ix^i \in S[x]$ mit $r_i \in R, s_j \in S$. Wir können $k = l$
annehmen. Dann gilt
$$\vfi\left( \sum_{i=0}^{k} (r_i, s_i)x^i \right) = (f_R, f_S).$$
Damit ist $\vfi: (R\times S)[x] \to R[x] \times S[x]$ ein Ringisomorphismus.
\qed

Wegen $\Z/\ideal{6} \cong \F_2 \times \F_3$ (gültig nach dem Chinesischen
Restsatz) folgt die zu zeigende Aussage direkt aus dem Lemma.

\newpage

## Aufgabe 2

\paragraph{Lemma 1:} Setze $J = \ideal{(x+\overline{1})}^2 \subseteq \F_2[x]$.
Dann ist $\F_2[x] / \ideal{x^2} \cong \F_2[x] / J$. \vspace{-0.5cm}
\paragraph{Beweis.} Die universelle Eigenschaft des Polynomrings erweitert die
kanonische Einbettung
$$\alpha: \F_2 \xhookrightarrow{\iota} \F_2[x] \xrightarrowdbl{\eps} \F_2[x] /
J$$
zu einem Ringhomomorphismus $\vfi: \F_2[x] \to \F_2[x] / J$ mit $\vfi(x) = x +
\overline{1} + J$. Da $x^2$ das Polynom $f \in \F_2[x]$ kleinsten Grades mit
$\vfi(f) = \overline{0} + J$ ist, gilt $\Ker(\vfi) = \ideal{x^2}$.
Die Surjektivität von $\vfi$ ist leicht einzusehen. \qed

\paragraph{Lemma 2:} Seien $R_1, \dots, R_k$ Ringe und $R = \prod_{i=1}^{k}
R_i$.  Genau dann ist $\pp \subsetneq R$ ein Primideal in $R$, wenn es ein $j
\in \set{1, \dots, k}$ gibt, sodass $\pp = \prod_{i=1}^{k} S_i$, wobei $S_j
\subsetneq R_j$ ein Primideal in $R_j$ ist und $S_i = R_i$ für $i \in \set{1,
\dots, k} \setminus \set{j}$.

Insbesondere gilt: Besitzt für $i \in \set{1, \dots, k}$ der Ring $R_i$ genau
$m_i$ verschiedene Primideal, so hat $R$ genau $\sum_{i=1}^{k} m_i$ Primideale.
\vspace{-0.5cm}
\paragraph{Beweis.} Wir haben zwei Richtungen zu zeigen.
\begin{enumerate}
    \item[$\Rightarrow$] Für $j \in \set{1, \dots, k}$ schreibe $e_j = (0,
        \dots, 0, 1, 0, \dots, 0)$ mit $1$ genau im $j$-ten Eintrag. Es gilt $R
        = \ideal{e_1, \dots, e_k}$.

        Wegen $\pp \subsetneq R$ gibt es ein $j \in \set{1, \dots, k}$ mit $e_j
        \notin \pp$. Für alle $i \in \set{1, \dots, k} \setminus \set{j}$ ist
        aber $e_je_i = 0 \in \pp$, also $e_i \in \pp$.

        Damit ist $\ideal{e_1, \dots, e_{j-1}, e_{j+1}, \dots, e_k} \subseteq
        \pp$ und $\pp$ ist von der Form $\pp = \prod_{i=1}^{k} S_i$
        mit $S_i = R_i$ für $i \in \set{1, \dots, k} \setminus \set{j}$ und $S_j
        \subsetneq R_j$.

        Nun ist noch die Primalität von $S_j$ in $R_j$ zu zeigen. Seien
        $a, b \in S_j$ mit $ab \in S_j$. Es gilt
        $$(1, \dots, 1, a, 1, \dots, 1) \cdot (1, \dots, 1, b, 1, \dots, 1) =
        (1, \dots, 1, ab, 1, \dots, 1) \in \pp,$$
        also $(1, \dots, 1, a, 1, \dots, 1) \in \pp$ oder $(1, \dots, 1, b, 1,
        \dots, 1) \in \pp$ und daher $a \in S_j$ oder $b \in S_j$.

    \item[$\Leftarrow$] Sei $\pp$ von der angegebenen Form und seien $a = (a_1,
        \dots, a_k), b = (b_1, \dots, b_k) \in R$ mit $ab \in \pp$. Da $S_j$ ein
        Primideal in $R_j$ ist, gilt $a_j \in S_j$ oder $b_j \in S_j$. Ferner
        gilt $a_i, b_i \in R_i = S_i$ für alle $i \in \set{1, \dots, k}
        \setminus \set{j}$. Also ist $a \in \pp$ oder $b \in \pp$ und $\pp$ ist
        ein Primideal.
\end{enumerate}

Jedes Primideal von $R$ entspricht also einem Primideal eines der $R_i$. \qed

---

Setze $f = x^4 - 2x^3 + x^2 = x^2(x-1)^2$ und $g = x^6 - 2x^4 + x^2 - 2$.
Dann gilt
$$2 = (x^2+2x+1) \cdot f - g \in I,$$
also $I = \ideal{f, 2}$. Laut Blatt 6, Aufgabe 3, kann man die Angelegenheit
auch über $\F_2$ betrachten. Unter Zuhilfenahme der dort bewiesenen Aussagen,
dem dritten Isomorphiesatz und dem Chinesischen Restsatz erhalten wir:
$$\begin{aligned}
\Z[x]/I &\cong (\Z[x]/\ideal{2})/(\ideal{f, 2}/\ideal{2}) \\
&\cong \F_2[x] / \ideal{\overline{f}} \\
&= \F_2[x] / \underbrace{(\ideal{x^2} \cap
\ideal{(x+\overline{1})^2})}_{\text{\makebox[0pt]{teilerfremd wegen
$x^2+(x+\overline{1})^2 = \overline{1}$.}}} \\
&\cong \underbrace{(\F_2[x] / \ideal{x^2})}_{\eqqcolon R} \times
\underbrace{(\F_2[x] / \ideal{(x+\overline{1})^2})}_{\cong R \text{ nach Lemma
1}} \\
&\cong R \times R.
\end{aligned}$$

(a) Es ist $R = \set{\overline{0}+\ideal{x^2}, \overline{1}+\ideal{x^2},
    x+\ideal{x^2}, x+\overline{1}+\ideal{x^2}}$, also
    $$\abs{\Z[x] / I} = \abs{R \times R} = \abs{R}^2 = 4^2 = 16.$$

(b) Wie man leicht nachprüft, ist $\ideal{x} = \set{\overline{0}+\ideal{x^2},
    x+\ideal{x^2}} \subseteq R$ das einzige Primideal in $R$. Laut dem zweiten
    Lemma besitzt dann $\Z[x] / I \cong R^2$ zwei Primideale.

(c) Es gilt $\abs{(\Z[x] / I)^\times} = \abs{(R^2)^\times} = \abs{(R^\times)^2}
    = \abs{R^\times}^2 = \abs{\set{\overline{1}+\ideal{x^2},
    x+\overline{1}+\ideal{x^2}}}^2 = 4$.

(d) Es gilt $\abs{\Nil(\Z[x] / I)} = \abs{\Nil(R^2)} = \abs{\Nil(R)^2} =
    \abs{\Nil(R)}^2 = \abs{\set{\overline{0}+\ideal{x^2}, x+\ideal{x^2}}}^2 =
    4$.

\newpage

## Aufgabe 3

Betrachte den kanonischen Ringhomomorphismus
$$\psi: R \to R/I_1 \times R/I_2, \; r \mapsto (r + I_1, r + I_2).$$
Zeigen wir nun, dass $\Ker(\psi) = I_1 \cap I_2$ und $\Img(\psi) = F$, so sind
wir nach dem ersten Isomorphiesatz fertig.

Ist $a \in R$, so gilt
$$\begin{aligned}
a \in \Ker(\psi) &\iff \psi(a) = (0 + I_1, 0 + I_2) \\
&\iff (a + I_1, a + I_2) = (I_1, I_2) \\
&\iff a \in I_1 \text{ und } a \in I_2 \\
&\iff a \in I_1 \cap I_2,
\end{aligned}$$
also $\Ker(\psi) = I_1 \cap I_2$.

Nun sei $b = (r_1 + I_1, r_2 + I_2) \in R/I_1 \times R/I_2$.

\begin{enumerate}
    \item[$\subseteq$] Ist $b \in \Img(\psi)$, so gibt es ein $a \in R$ mit
        $$(a + I_1, a + I_2) = \psi(a) = b = (r_1 + I_1, r_2 + I_2).$$
        Es folgt
        $$\vfi_1(r_1 + I_1) = \vfi_1(a + I_1) = a + I_1 + I_2 = \vfi_2(a + I_2)
        = \vfi_2(r_2 + I_2),$$
        also $b \in F$.
    \item[$\supseteq$] Ist umgekehrt $b \in F$, so gilt
        $$r_1 + I_1 + I_2 = \vfi_1(r_1 + I_1) = \vfi_2(r_2 + I_2) = r_2 + I_1 +
        I_2,$$
        und damit $r_1 - r_2 \in I_1 + I_2$. Wir finden $t_1 \in I_1, t_2 \in
        I_2$ mit $r_1 - r_2 = t_1 + t_2$ und erhalten
        $$\psi(r_1 - t_1) = (r_1 - t_1 + I_1, r_1 - t_1 + I_2) = (r_1 + I_1, r_2
        + t_2 + I_2) = (r_1 + I_1, r_2 + I_2) = b,$$
        ergo $b \in \Img(\psi)$.
\end{enumerate}

\newpage

## Aufgabe 4

\paragraph{Lemma:} Seien $V_1, \dots, V_k$ endlich-dimensionale Vektorräume über
$K$. Dann gilt
$$\dim_K\left( \prod_{i=1}^{k} V_i \right) = \sum_{i=1}^{k} \dim_K(V_i).$$
\paragraph{Beweis.} Es ist $V_i \cong K^{\dim_K(V_i)}$ für $i \in \set{1, \dots,
k}$. Es folgt
$$\dim_K\left( \prod_{i=1}^{k} V_i \right) = \dim_K\left( \prod_{i=1}^{k}
K^{\dim_K(V_i)} \right) = \dim_K\left(K^{\sum_{i=1}^{k} \dim_K(V_i)}\right) =
\sum_{i=1}^{k} \dim_K(V_i).$$ \qed

\paragraph{Lemma:} Ist $I \subseteq R$ ein Ideal in $R$, so ist der
Restklassenring\footnote{Wer kam auf die Idee, Restklassenkonstruktionen und
Algebren über Ringen und in diesem Sinne auch Körpererweiterungen auf dieselbe
Art und Weise zu notieren? Und warum wird die Notation weiterhin verwendet, wenn
man statt $R / K$ auch "Algebra über $K$" oder "$K$-Algebra" sagen könnte?} $S =
R/I$ eine endlich-dimensionale Algebra über $K$ mit $\dim_K(S) \leqslant d$.
\vspace{-0.5cm}
\paragraph{Beweis.} Laut Beispiel 13.13 ist nur die endliche Dimension zu
zeigen. Sei $\set{a_1, \dots, a_d} \subseteq R$ eine $K$-Basis von $R$ und
$a + I \in S$. Dann gibt es $\lambda_1, \dots, \lambda_d \in K$ mit $a =
\lambda_1a_1 + \cdots + \lambda_da_d$,
also
$$a + I = \lambda_1(a_1 + I) + \cdots + \lambda_d(a_d + I).$$
Daher ist $\ideal{a_1 + I, \dots, a_d + I}_K = S$ als $K$-Vektorraum, und es
gilt $\dim_K(S) \leqslant d < \infty$. \qed

---

(a) Sei $\pp \subsetneq R$ ein Primideal in $R$. Dann ist $S = R/\pp$ ein
    Integritätsbereich. Wir wollen zeigen, dass $S$ ein Körper
    ist.

    Dazu sei $a + \pp \in S \setminus \set{0 + \pp}$. Es ist ein multiplikatives
    Inverses für $a + \pp$ zu finden. Wegen $\dim_K(S) < \infty$
    ist die Menge
    $$\set{1+\pp, a+\pp, a^2+\pp, \dots}$$
    $K$-linear abhängig und es gibt ein $k \in \N$ und $\lambda_0, \dots,
    \lambda_k \in K$ (nicht alle $0$) mit
    $$0 + \pp = \lambda_0 \cdot (1 + \pp) + \lambda_1 \cdot (a + \pp) + \cdots
    + \lambda_k \cdot (a^k + \pp) = \lambda_0 \cdot 1 + \lambda_1a + \cdots +
    \lambda_ka^k + \pp.$$
    Wähle das kleinste solche $k$. Dann ist
    $$\lambda_1 \cdot 1 + \cdots + \lambda_ka^{k-1} + \pp \ne 0 + \pp$$
    wegen der Minimalität von $k$ und wir erhalten
    $$-\lambda_0 + \pp = \lambda_1a + \cdots + \lambda_ka^k + \pp =
    \underbrace{(a + \pp)}_{\ne 0 + \pp}\underbrace{(\lambda_1 \cdot 1 + \cdots
    + \lambda_ka^{k-1} + \pp)}_{\ne 0 + \pp}.$$
    Da $S$ ein Integritätsbereich, folgt $-\lambda_0 \ne 0 + \pp$. Somit ist
    $$1 + \pp = \frac{-\lambda_0}{-\lambda_0} + \pp = (a+\pp)\underbrace{\left(
    \frac{\lambda_1}{-\lambda_0} \cdot 1 + \cdots +
    \frac{\lambda_k}{-\lambda_0}a^{k-1} + \pp\right)}_{= a^{-1} + \pp}.$$

(b) Seien $\mm_1, \dots, \mm_k \subsetneq R$ paarweise verschiedene maximale
    Ideale in $R$. Dann sind die Ideale auch paarweise teilerfremd, und der
    Chinesische Restsatz (Satz 12.16) liefert einen Ringepimorphismus
    $$\vfi \colon R \xrightarrowdbl{} \prod_{i=1}^k R/\mm_i, \qquad
    a \mapsto (a + \mm_1, \dots, a + \mm_k).$$
    Dieser ist $K$-linear: Sind $a, b \in R$ und $\lambda \in K$, so gilt
    $$\begin{aligned}
    \vfi(\lambda a + b) &= (\lambda a + b + \mm_1, \dots, \lambda a + b +
    \mm_k) \\
    &= \lambda (a + \mm_1, \dots, a + \mm_k) + (b + \mm_1, \dots, b + \mm_k) =
    \lambda\vfi(a) + \vfi(b).
    \end{aligned}$$
    Mit der Surjektivität von $\vfi$ erhalten wir
    $$\sum_{i=1}^{k} \dim_K(R/\mm_i) = \dim_K\left(\prod_{i=1}^k
    R/\mm_i\right) = \dim_K(\vfi(R)) \leqslant \dim_K(R) = d.$$
    Für jedes $i \in \set{1, \dots, k}$ ist $\mm_i \subsetneq R$, also
    $\set{0 + \mm_i} \subsetneq R/\mm_i$, und daher $0 < \dim_K(R/\mm_i)$.
    Dies zeigt $k \leqslant d$.

\newpage

## Aufgabe 5

(a) Da $\Z$ unendlich und $R_n$ endlich ist, kann $\vfi_n$ nicht injektiv sein.

    Sei $q = \prod_{p \in \PP_n} p$. Dann ist
    $$\vfi_n \colon \Z \xrightarrowdbl{\;\;\eps\;\;} \Z/q\Z
    \xrightarrow{\;\;\cong\;\;} R_n,$$
    und $\vfi_n$ ist als Komposition einer Surjektion und einer Bijektion
    surjektiv.

    Für die Injektivität von $\vfi$ seien $a, b \in \Z$ mit $a \ne b$. Die Menge
    $\PP \subseteq \N$ ist unendlich und daher nach oben unbeschränkt. Es gibt
    also ein $p \in \PP$ mit $p > a - b > 0$. Daher gilt $a - b \notin p\Z$,
    d.h. $a + p\Z \ne b + p\Z$. Dies zeigt $\vfi(a)(p) \ne \vfi(b)(p)$, also
    $\vfi(a) \ne \vfi(b)$.

    Betrachte nun die Folge $a = (a_p)_{p\in\PP}$ mit $a_2 = 1 + 2\Z$ und
    $a_p = 0 + p\Z$ für alle $p \in \PP \setminus \set{2}$. Gäbe es ein $k \in
    \Z$ mit $\vfi(k) = a$, so hätten wir $k \ne 0$ wegen $k + 2\Z = a_2 = 1 +
    2\Z$, aber $p \mid k$ für alle $p \in \PP \setminus \set{2}$ wegen $k + p\Z
    = a_p = p\Z$. In einem faktoriellen Ring wie $\Z$ geht das aber nicht. Somit
    ist $\vfi^{-1}(a) = \varnothing$, und $\vfi$ ist nicht surjektiv.

(b) Seien $a, b \in I$ und $r \in R$. Es gibt $n, m \in \N$ mit $a(p) = 0 + p\Z$
    und $b(q) = 0 + q\Z$ für alle $p, q \in \PP$ mit $p > n$ und $q > m$.

    Es gilt $(-a)(p) = -a(p) = 0 + p\Z$ und $(r \cdot a)(p) = r(p) \cdot a(p) =
    0 + p\Z$ für alle $p \in \PP$ mit $p > n$, also $-a \in I$ und $r \cdot a
    \in I$.

    Ferner gilt $(a + b)(p) = a(p) + b(p) = (0 + p\Z) + (0 + p\Z) = 0 + p\Z$ für
    alle $p \in \PP$ mit $p > \max\set{n, m}$, also $a + b \in I$.

(c) Für die Injektivität von $\overline{\vfi}$ seien $a, b \in \Z$ mit $a \ne
    b$. Sei $p \in \PP$ mit $p > a - b > 0$. Dann gilt $a - b \notin p\Z$,
    d.h. $a + p\Z \ne b + p\Z$. Dies zeigt $\vfi(a)(p) - \vfi(b)(p) \ne 0 +
    p\Z$.

    Daher gilt $\vfi(a) - \vfi(b) \notin I$, also
    $$\overline{\vfi}(a) = \vfi(a) + I \ne \vfi(b) + I = \overline{\vfi}(b),$$
    und die Injektivität ist gezeigt.

    Für die Widerlegung der Surjektivität sei $\pi : \N \to \PP$ eine Bijektion.

    Betrachte nun die Folge $a = (a_p)_{p\in\PP}$ mit $a_{\pi(2m)} = 1 +
    \pi(2m)\Z$ und $a_{\pi(2m+1)} = 0 + \pi(2m+1)\Z$ für alle $m \in \N$.

    Gäbe es ein $k \in \Z$ mit $\overline{\vfi}(k) = a + I$, so gäbe es ein
    $n \in \N$ mit $0+p\Z = \vfi(k)(p) - a_p$ für alle $p \in \PP$ mit $p > n$.

    Wir hätten $k+p\Z = a_p = 0 + p\Z$ für unendlich viele $p \in \PP$,
    aber gleichzeitig $k+p\Z = a_p = 1 + p\Z$ für unendlich viele $p \in \PP$.

    Dies lieferte eine Zahl $k \in \Z \nozero$ mit unendlich vielen
    Primfaktoren, in dem faktoriellen Ring $\Z$ ist das schwer möglich.  Somit
    ist $\overline{\vfi}^{-1}(a) = \varnothing$, und $\overline{\vfi}$ ist nicht
    surjektiv.

\newpage

## Aufgabe 6

(a) Zwei Dinge sind zu zeigen:

    \paragraph{Existenz:} Verwende
    $$\vfi: R^n \to M, \qquad (v_1, \dots, v_n) \mapsto \sum_{i=1}^{n} v_im_i.$$
    Offenbar ist dann $\vfi(e_i) = m_i$ für $i \in \set{1, \dots, n}$.

    Für $v = (v_1, \dots, v_n), w = (w_1, \dots, w_n) \in R^n$ und $\lambda \in
    R$ gilt
    $$\vfi(\lambda v + w) = \sum_{i=1}^{n} (\lambda v_i + w_i) m_i =
    \lambda\left( \sum_{i=1}^{n} v_im_i \right) + \left( \sum_{i=1}^{n} w_im_i
    \right) = \lambda\vfi(v) + \vfi(w).$$
    Ist $v \in M$, so schreibe $v = \sum_{i=1}^{n} r_im_i$ mit $r_1, \dots, r_n
    \in R$. Dann gilt $\vfi(r_1, \dots, r_n) = v$. Die Abbildung $\vfi$ ist
    somit $R$-linear und surjektiv.

    \paragraph{Eindeutigkeit:} Ist $\psi: R^n \to M$ eine Abbildung mit den
    gewünschten Eigenschaften, so gilt für $(v_1, \dots, v_n) \in R^n$:
    $$\psi(v_1, \dots, v_n) = \psi\left( \sum_{i=1}^{n} v_ie_i \right) =
    \sum_{i=1}^{n} v_i \psi(e_i) = \sum_{i=1}^{n} v_im_i = \vfi(v_1, \dots,
    v_m),$$
    also $\psi = \vfi$.

(b) Weil später noch andere Erzeugendensysteme von $M$ vorkommen, soll durch die
    Notation $U(m_1, \dots, m_n) \coloneqq U$ das hier gewählte
    Erzeugendensystem berücksichtigt werden.

    Betrachte $\xi: R^n / U \to M, v+U \mapsto \vfi(v)$. Diese Abbildung ist
    wohldefiniert, denn sind $v, w \in R^n$ mit $v + U = w + U$, so ist
    $$\xi(v+U) - \xi(w+U) = \vfi(v) - \vfi(w) = \vfi(\underbrace{v - w}_{\in
    \Ker(\vfi)}) = 0.$$
    Die $R$-Linearität vererbt sich von $\vfi$, die Surjektivität ebenso.
    Ist $v+U \in \Ker(\xi)$, so ist $\vfi(v) = \xi(v+U) = 0$, also $v \in
    \Ker(\vfi) = U$, und damit $\Ker(\xi) = \set{0+U}$. Damit ist $\xi$ injektiv
    und insgesamt ein Isomorphismus von $R$-Moduln.

(c) Die mehrdeutige Notation $F_0 \subset F_1$ statt $F_0 \subseteq F_1$ ist
    wohl wenig reflektiertem Abschreiben aus Kunz geschuldet.

    Die Inklusion $F_{n-1} \subseteq F_n = R$ ist klar. Sei $k \in \set{2,
    \dots, n}$. Wir wollen $F_{n-k} \subseteq F_{n-k+1} = F_{n-(k-1)}$ zeigen.
    Dazu sei $M_k$ die Menge der $k \times k$-Untermatrizen von $A$
    und $M_{k-1}$ die Menge der $(k-1) \times (k-1)$-Untermatrizen von $A$.

    Ist $B \in M_k$, so kann man zur Berechnung von $\det(B)$ eine
    Laplace-Entwicklung verwenden und $\det(B)$ als $R$-Linearkombinationen von
    Determinanten von $(k-1)\times(k-1)$-Untermatrizen von $B$ darstellen.
    Diese Untermatrizen liegen in $M_{k-1}$.

    Also ist $\det(B) \in \ideal{\set{\det(C) \mid C \in M_{k-1}}}$, und somit
    $\ideal{\det(B)} \subseteq \ideal{\set{\det(C) \mid C \in M_{k-1}}}$. Dies
    zeigt
    $$F_{n-k} = \ideal{\set{\det(B) \mid B \in M_k}} \subseteq
    \ideal{\set{\det(C) \mid C \in M_{k-1}}} = F_{n-(k-1)}.$$

(d) Seien $\set{u_\lambda}_{\lambda \in \Lambda}$ und $\set{w_\gamma}_{\gamma
    \in \Gamma}$ Erzeugendensysteme von $U$ mit $u_\lambda = (r_{\lambda,1},
    \dots, r_{\lambda,n}), \lambda \in \Lambda,$ und $w_\gamma = (s_{\gamma,1},
    \dots, s_{\gamma,n}), \gamma \in \Gamma,$
    und
    $$A = \left(r_{\lambda,i}\right)_{\lambda\in\Lambda,i\in\set{1, \dots, n}},
    \qquad B = \left(s_{\gamma,i}\right)_{\gamma\in\Gamma,i\in\set{1, \dots,
    n}}$$
    die zugehörigen Relationenmatrizen.

    Für $\gamma \in \Gamma$ schreibe $w_\gamma = \sum_{i=1}^{n}
    a_{\gamma,i}u_\lambda$ mit $a_{\gamma,i} \in R$.

    Für $k \in \set{1, \dots, n}$ sei $M_{A,k}$ die Menge der $k\times
    k$-Untermatrizen von $A$ und $M_{B,k}$ die Menge der $k\times
    k$-Untermatrizen von $B$.

    Nun sei $C \in M_{B,k}$. Die Zeilen von $C$ bestehen dann aus
    $R$-Linearkombinationen von Zeilen von Matrizen aus $M_{A,k}$.
    Wegen der Multilinearität der Determinante ist damit
    $\det(C)$ eine $R$-Linearkombination von Determinanten von Untermatrizen aus
    $M_{A,k}$.

    Also ist $\det(C) \in \ideal{\set{\det(D) \mid D \in M_{A,k}}}$, und somit
    $\ideal{\det(C)} \subseteq \ideal{\set{\det(D) \mid D \in M_{A,k}}}$.
    Dies zeigt
    $$\ideal{\set{\det(C) \mid C \in M_{B,k}}} \subseteq \ideal{\set{\det(D)
    \mid D \in M_{A,k}}}.$$
    Durch Vertauschung der Rollen von $A$ und $B$ erhält man
    $$\ideal{\set{\det(D) \mid D \in M_{A,k}}} \subseteq \ideal{\set{\det(C)
    \mid C \in M_{B,k}}}.$$

(e) Für ein Erzeugendensystem $\set{m_1, \dots, m_n}$ von $M$ und $i \in \set{0,
    \dots, n-1}$ bezeichne $F_i(\set{m_1, \dots, m_n})$ das $i$-te
    Fitting-Ideal, wie es sich aus der Definition in (c) aus der Wahl von
    $\set{m_1, \dots, m_n}$ ergibt.

    Wir wollen im Folgenden zeigen, dass $F_i(\set{m_1, \dots, m_n}) =
    F_i(\set{m_1, \dots, m_n, m})$ für alle Erzeugendensysteme $\set{m_1, \dots,
    m_n}$ von $M$ und für alle $m \in M$ gilt. Für alle
    Erzeugendensysteme $\set{a_1, \dots, a_s}$ und $\set{b_1, \dots, b_t}$ von
    $M$ folgt dann
    $$F_i(\set{a_1, \dots, a_s}) = F_i(\set{a_1, \dots, a_s, b_1, \dots, b_t}) =
    F_i(\set{b_1, \dots, b_t}).$$

    Schreibe $m = \sum_{i=1}^{n} r_im_i$ mit $r_i \in R$. Sei
    $\set{u_\lambda}_{\lambda\in\Lambda}$ ein Erzeugendensystem von $U(m_1,
    \dots, m_n)$. Ist $A$ die Relationenmatrix von $M$ bezüglich $\set{m_1,
    \dots, m_n}$, so ist
    $$B \coloneqq \left(\begin{array}{ccc | c}
    r_1 & \cdots & r_n & -1 \\ \hline
        &   &     &  0 \\
        & A &     &  \vdots \\
        &   &     &  0
    \end{array}\right)$$
    die Relationenmatrix bezüglich $\set{m_1, \dots, m_n, m}$, denn für $U(m_1,
    \dots, m_n, m)$ ist $\set{(r_1, \dots, r_n, -1)} \cup
    \set{(r_{\lambda,1}, \dots, r_{\lambda_n}, 0) \mid \lambda \in \Lambda}$ ein
    Erzeugendensystem.

    Wir zeigen nun zwei Inklusionen für die Gleichheit $F_i(\set{m_1, \dots,
    m_n}) = F_i(\set{m_1, \dots, m_n, m})$.

    \begin{enumerate}
        \item[$\subseteq$]
    Ist $c = \det(C) \in F_i(\set{m_1, \dots, m_n})$ Determinante einer
    $(n-i)\times(n-i)$-Untermatrix $C$ von $A$, so ist $-c$ Determinante einer
    $(n+1-i)\times(n+1-i)$-Untermatrix von $B$ der Form
    $$\left(\begin{array}{c c c | c}
    r_{l_1} & \cdots & r_{l_{n-i}} & -1 \\ \hline
        &   &     &  0 \\
        & C &     &  \vdots \\
        &   &     &  0
    \end{array}\right).$$
    Daher ist $c \in F_i(\set{m_1, \dots, m_n, m})$. Weil $c$ ein beliebiger
    Erzeuger von $F_i(\set{m_1, \dots, m_n})$ war, folgt insgesamt
    $$F_i(\set{m_1, \dots, m_n}) \subseteq F_i(\set{m_1, \dots, m_n, m}).$$

        \item[$\supseteq$]

    Ist umgekehrt $c = \det(C) \in F_i(\set{m_1, \dots, m_n, m})$ Determinante
    einer $(n+1-i)\times(n+1-i)$-Untermatrix von $B$, so treten drei Fälle auf:

    \begin{itemize}

        \item Die letzte Spalte von $C$ besteht aus Nullen. Dann ist
            $c = 0 \in F_i(\set{m_1, \dots, m_n})$.
        \item $C$ ist eine $(n+1-i)\times(n+1-i)$-Untermatrix von $A$. Dann gilt
          $$c \in F_{i-1}(\set{m_1, \dots, m_n}) \subseteq F_i(\set{m_1, \dots,
          m_n}).$$
        \item Die erste Zeile von $C$ enthält Einträge aus $(r_1, \dots, r_n,
            -1)$. Dann ist $c = \det(C)$ mittels Entwicklung nach dieser ersten
            Zeile eine $R$-Linearkombination von
            $(n-i)\times(n-i)$-Untermatrizen von $A$ oder Matrizen, deren letzte
            Spalte aus Nullen besteht. Dann ist aber wiederum
            $$c \in F_i(m_1, \dots, m_n).$$
    \end{itemize}

    In jedem Fall gilt $c \in F_i(\set{m_1, \dots, m_n})$. Weil $c$ ein
    beliebiger Erzeuger von $F_i(\set{m_1, \dots, m_n, m})$ war, folgt
    $$F_i(\set{m_1, \dots, m_n, m}) \subseteq F_i(\set{m_1, \dots, m_n}).$$
    \end{enumerate}
