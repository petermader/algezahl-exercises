# Algebra und Zahlentheorie -- Übungsblatt 5

\newcommand\ideal[1]{\left<#1\right>}

## Aufgabe 1

(a) Setze $f = x^2 + n_1x + n_2 \in \Z[x]$ und $\overline{f} = x^2 +
    \overline{n_1}x + \overline{n_2} \in \F_2[x]$.

    Dann gilt $\overline{f} = x^2 + x + \overline{1}$, und wegen
    $\overline{f}(0) = \overline{f}(1) = 1$ ist $\overline{f}$ in $\F_2[x]$
    irreduzibel. Mit dem Reduktionskriterium ist $f$ dann in $\Z[x]$ irreduzibel
    und daher auch in $\Q[x]$.

(b) Analog erhält man $\overline{f} = x^2 + x^3 + \overline{1} \in \F_2[x]$, ein
    laut Blatt 4, Aufgabe 6, in $\F_2[x]$ irreduzibles Polynom. Somit ist $f =
    x^4 + n_1x^3 n_2x^2 + n_3x + n_4 \in \Q[x]$ irreduzibel.

(c) Ja. Für eine Primzahl $p$ ist $x - p \in \Z[x]$ irreduzibel. Nach dem
    Eisenstein-Kriterium ist für $n \geqslant 2$ das Polynom $x^n + p \in \Z[x]$
    irreduzibel.

## Aufgabe 2

\paragraph{Lemma:} Sei $a \in \Z$. Genau dann ist $f = \sum_{i=0}^{n}
a_ix^i \in \Z[x]$ mit $n\geqslant 0, a_i \in \Z$ irreduzibel, wenn $$\tilde{f} =
\sum_{i=0}^{n} a_n(x-a)^i$$ irreduzibel ist. \vspace{-0.5cm}
\paragraph{Beweis.} Für jede Produktdarstellung $f = gh$ mit $g = \sum_{i=0}^{m}
b_ix^i, h = \sum_{i=0}^{k} c_ix^i \in \Z[x]$ kann man
$$\tilde{g} = \sum_{i=0}^{m} b_i(x-a)^i, \qquad \tilde{h} = \sum_{i=0}^{k}
c_i(x-a)^i$$
setzen und erhält $\tilde{f} = \tilde{g}\tilde{h}$. Für die Umkehrung verwende
$-a$ statt $a$. \qed \vspace{1cm}

(a) Das Polynom $f = x^4 + 1 \in \Z[x]$ ist irreduzibel.

    Wir betrachten zunächst die Primfaktorzerlegung über dem laut Satz 8.13
    faktoriellen Ring $\R[x]$. Wegen $a^4+1 \geqslant 1 > 0$ für alle $a \in
    \R$ besitzt $f$ über $\R$ keinen Linearfaktor. Somit ist $f = (x^2 +
    \sqrt{2}x + 1)(x^2 - \sqrt{2}x + 1)$ die bis auf Reihenfolge und Einheiten
    eindeutige Primfaktorzerlegung von $f$. Daher kann $f$ auch keine echten
    Faktoren über $\Z \subseteq \R$ besitzen.

(b) Das primitive Polynom $f = x^4 + x + 1 \in \Z[x]$ ist irreduzibel.

    Laut Blatt 4, Aufgabe 6, ist $\overline{f} = x^4 + x + \overline{1} \in
    \F_2[x]$ irreduzibel; das Reduktionskriterium liefert die Irreduzibilität
    von $f \in \Z[x]$.

(c) Das primitive Polynom $f = x^4 + 6x^2 + 1 \in \Z[x]$ ist irreduzibel.

    Wir betrachten zunächst die Primfaktorzerlegung über dem laut Satz 8.13
    faktoriellen Ring $\R[x]$. Wegen $a^4+6a^2+1 \geqslant 1 > 0$ für alle $a
    \in \R$ besitzt $f$ über $\R$ keinen Linearfaktor. Somit ist $f = (x^2 +
    2\sqrt{2}x + 3)(x^2 - 2\sqrt{2}x + 3)$ die bis auf Reihenfolge und Einheiten
    eindeutige Primfaktorzerlegung von $f$. Somit kann $f$ auch keine echten
    Faktoren über $\Z \subseteq \R$ besitzen.

(d) Das primitive Polynom $f = x^3 + 2x^2 + 3x + 3 \in \Z[x]$ ist irreduzibel.

    Laut Blatt 4, Aufgabe 6, ist $\overline{f} = x^3 + x + \overline{1} \in
    \F_2[x]$ irreduzibel; das Reduktionskriterium liefert die Irreduzibilität
    von $f \in \Z[x]$.

(e) Das primitive Polynom
    $$f = \sum_{i=0}^{p-1} x^i = \frac{x^p - 1}{x - 1} \in \Z[x]$$
    ist irreduzibel.

    Nach dem Lemma kann man äquivalent auch die Irreduzibilität von
    $$\tilde{f} = \sum_{i=0}^{p-1} (x+1)^i = \frac{1}{x}((x+1)^p - 1)$$
    zeigen. Es gilt
    $$\tilde{f}
    = \frac{1}{x}\left(\left(\sum_{i=0}^{p} \binom{p}{i} x^i\right) - 1 \right)
    = \frac{1}{x}\left(\sum_{i=1}^{p} \binom{p}{i} x^i\right)
    = \sum_{i=0}^{p-1} \binom{p}{i+1} x^i.$$
    Wegen $p \mid \binom{p}{i+1}$ für alle $i \in \set{0, \dots, p-2}$ sowie $p
    \nmid \binom{p}{p} = 1$ und $p^2 \nmid \binom{p}{1} = p$ ist $\tilde{f}$
    nach dem Eisenstein-Kriterium irreduzibel.

## Aufgabe 3

(a) Eine Multiplikation mit der Einheit $\frac{10}{3}$ ändert nichts an der
    Irreduzibilität von $f = 3x^3 - 6x^2 + \frac{3}{2}x - \frac{3}{5}$.

    Betrachte daher das primitive Polynom $\tilde{f} = \frac{10}{3}f = 10x^3 -
    20x^2 + 5x - 2 \in \Q[x]$. Das Polynom $\overline{\tilde{f}} = x^3 + x^2 +
    \overline{2}x + \overline{1} \in \F_3[x]$ besitzt in $\F_3[x]$ keine
    Nullstelle, also keinen Linearfaktor, und ist damit aus Gradgründen in
    $\F_3[x]$ irreduzibel. Nach dem Reduktionskriterium ist dann $\tilde{f} \in
    \Z[x]$, also in $\Q[x]$ irreduzibel.

(b) Das primitive Polynom $f = 8x^3 - 6x - 1 \in \Q[x]$ ist irreduzibel.

    Da $\overline{f} = \overline{4}x^3 + \overline{4}x + \overline{4} \in
    \F_5[x]$ in $\F_5$ keine Nullstelle und damit keinen Linearfaktor besitzt,
    ist $\overline{f}$ aus Gradgründen in $\F_5[x]$ irreduzibel. Nach dem
    Reduktionskriterium ist dann $f \in \Z[x]$, also in $\Q[x]$ irreduzibel.

(c) Das primitive Polynom $f = x^5 - 10x^4 + 10x^3 - 80x^2 + 75x - 17 \in \Q[x]$
    ist irreduzibel.

    Laut Blatt 4, Aufgabe 6, ist $\overline{f} = x^5 + x + \overline{1} \in
    \F_2[x]$ irreduzibel; das Reduktionskriterium liefert die Irreduzibilität
    von $f \in \Z[x]$, und damit in $\Q[x]$.

(d) Das Polynom $f = x^4 - 6x^2 + 5$ besitzt wegen $f(1) = 0$ einen Linearfaktor
    und ist daher reduzibel.

(e) Das primitive Polynom $f = x^3 + 6x^2 + 8x + 4 \in \Q[x]$ ist irreduzibel.

    Da $\overline{f} = x^3 + \overline{2}x + \overline{1} \in
    \F_3[x]$ in $\F_3$ keine Nullstelle und damit keinen Linearfaktor besitzt,
    ist $\overline{f}$ aus Gradgründen in $\F_3[x]$ irreduzibel. Nach dem
    Reduktionskriterium ist dann $f \in \Z[x]$, also in $\Q[x]$ irreduzibel.

\newpage

## Aufgabe 4

(a) Die Abbildung
    $$\delta: \Z[\sqrt{2}] \nozero \to \N, a+b\sqrt{2} \mapsto
    \abs{a^2 - 2b^2} = \abs{(a+b\sqrt{2})(a-b\sqrt{2})} =
    \abs{a+b\sqrt{2}}\abs{a-b\sqrt{2}}$$
    ist multiplikativ. Wäre $f = x^3 - 3$ in $K[x] = Q(\Z[\sqrt{2}])[x]$
    reduzibel, so auch in $\Z[\sqrt{2}][x]$. Dann müsste $f$ eine Nullstelle $a
    \in \Z[\sqrt{2}]$ besitzen. Wir hätten $9 = \delta(3) = \delta(a^3) =
    \delta(a)^3$, im Widerspruch zu $\delta(a) \in \N$.

(b) Die Abbildung
    $$\delta: \Z[\ii] \nozero \to \N, a+b\ii \mapsto \abs{a^2 + b^2} =
    \abs{(a+b\ii)(a-b\ii)} = \abs{a+b\ii}\abs{a-b\ii}$$
    ist multiplikativ. Wäre $f = x^3 - 2$ in $K[x] = Q(\Z[\ii])[x]$
    reduzibel, so auch in $\Z[\ii][x]$. Dann müsste $f$ eine Nullstelle $a
    \in \Z[\ii]$ besitzen. Wir hätten $4 = \delta(2) = \delta(a^3) =
    \delta(a)^3$, im Widerspruch zu $\delta(a) \in \N$.

## Aufgabe 5

\paragraph{Lemma 1:} Sei $R$ ein kommutativer Ring. Für $f = \sum_{i=0}^{n}
a_ix^i \in R[x]$ mit $n \geqslant 0, a_0, \dots, a_n \in R$ und $a_n \ne 0$
setze $\rho(f) = \sum_{i=0}^{n} a_{n-i}x^i$ sowie $\rho(0) = 0$.
Dann ist $\rho$ multiplikativ und es gilt $\deg(\rho(f)) \leqslant \deg(f)$ für
$f \in R[x] \nozero$. \vspace{-0.2cm}
\paragraph{Beweis.} Seien $f = \sum_{i=0}^{n} a_ix^i, g = \sum_{j=0}^{m} b_jx^j
\in R[x]$. Es gilt
$$\begin{aligned}
\rho(fg) &= \rho\left(\sum_{k=0}^{n+m} x^k
\sum_{\substack{i\in\set{0,\dots,n},\\j\in\set{0,\dots,m},\\i+j=k}}
a_ib_j\right)
= \sum_{k=0}^{n+m} x^k
\sum_{\substack{i\in\set{0,\dots,n},\\j\in\set{0,\dots,m},\\i+j=n+m-k}}
a_ib_j \\
&= \sum_{k=0}^{n+m} x^k
\sum_{\substack{i\in\set{0,\dots,n},\\j\in\set{0,\dots,m},\\(n-i)+(m-j)=k}}
a_ib_j
= \sum_{k=0}^{n+m} x^k
\sum_{\substack{i\in\set{0,\dots,n},\\j\in\set{0,\dots,m},\\i+j=k}}
a_{n-i}b_{m-j} \\
&= \left(\sum_{i=0}^{n} a_{n-i}x^i\right) \cdot \left(\sum_{j=0}^{m}
b_{m-j}x^j\right) = \rho(f) \cdot \rho(g).
\end{aligned}$$
Die Eigenschaft $\deg(\rho(f)) \leqslant \deg(f)$ für $f \in R[x] \nozero$
ergibt sich aus der Definition von $\rho$. \qed

\paragraph{Lemma 2:} Sei $R$ ein faktorieller Ring und $f \in R[x]$ mit $\deg(f)
\geqslant 2$. Ist $\rho(f)$ irreduzibel, so auch $f$. \vspace{-0.2cm}
\paragraph{Beweis.} Ist $\rho(f)$ irreduzibel, so verschwinden Leitkoeffizient
und konstanter Koeffizient von $\rho(f)$ nicht. Daher ist $\rho(\rho(f)) = f$
und es gilt $\deg(f) = \deg(\rho(\rho(f))) \leqslant \deg(\rho(f)) \leqslant
\deg(f)$, also $\deg(\rho(f)) = \deg(f)$.

Angenommen, es gibt eine Faktorisierung $f = gh$ mit $g, h \in R[x]$ und
$0 < \deg(g), \deg(h) < \deg(f)$. Dann gilt
$$\deg(\rho(g)) \leqslant \deg(g) < \deg(f) \qquad \text{und} \qquad
\deg(\rho(h)) \leqslant \deg(h) < \deg(f)$$
und damit
$$\deg(\rho(g)) + \deg(\rho(h)) = \deg(\rho(g)\rho(h)) = \deg(\rho(gh)) =
\deg(\rho(f)) = \deg(f).$$
Wir erhalten $\deg(\rho(g)) > 0, \deg(\rho(h)) > 0$, und wegen $\rho(f) =
\rho(gh) = \rho(g) \rho(h)$ ist $\rho(f)$ nicht irreduzibel. \qed
\vspace{1cm}

(a) Ist $\deg(f) = n = 0$, so haben wir $f = a_0 = \ggT(a_0) = \ggT(a_0, \dots,
    a_n) \in R^\times$ und es gilt $p \mid a_i$ für $i \in \set{1, \dots, n}$
    und $p \nmid a_n = a_0$ für jedes Primelement $p \in R$. Wir werden diesen
    Fall mal ignorieren.

    Ist $\deg(f) = n > 0$, so haben wir $p \nmid a_0$, denn sonst wäre
    $p$ gemeinsamer Teiler aller Koeffizienten $a_0, \dots, a_n$. Für $n = 1$
    ist $f$ dann bereits irreduzibel. Für $n \geqslant 2$ ist $\rho(f)$ nach dem
    Eisenstein-Kriterium irreduzibel, und mit Lemma 2 auch $f$.

(b) Eine Multiplikation mit der Einheit $6 \in \Q$ ändert nichts an der
    Irreduzibilität von $f = x^4 - \frac{1}{2}x^2 + \frac{3}{2}x - \frac{4}{3}
    \in \Q[x]$.

    Betrachte daher $\tilde{f} = 6f = 6x^4 - 3x^2 + 9x - 8 \in \Z[x]$. Dann ist
    $\tilde{f}$ primitiv und wegen $3 \mid 9, 3 \mid -3, 3 \mid 6$ und $3^2 = 9
    \nmid 6$ liefert (a), dass $\tilde{f}$ in $\Z[x]$ irreduzibel ist. Daher ist
    $\tilde{f}$ und auch $f$ in $\Q[x]$ irreduzibel.

## Aufgabe 6

(a) Laut Satz 9.10 ist $K[x, y]$ faktoriell und wir können Primfaktorzerlegungen
    $$f = p_1^{\alpha_1} \cdots p_s^{\alpha_s}, \qquad g = q_1^{\beta_1} \cdots
    q_t^{\beta_t}$$
    betrachten, mit Primelementen (d.h. irreduziblen Elementen) $p_1, \dots,
    p_s, q_1, \dots, q_t \in K[x, y]$. Weil $f$ und $g$ teilerfremd sind, sind
    $p_i$ und $q_j$ nichtassoziiert für alle $i \in \set{1, \dots, s}$ und $j
    \in \set{1, \dots, t}$.

    Zeigt man, dass für nichtassoziierte irreduzible $p, q \in K[x, y]$ die
    Lösungsmenge
    $$\mathbb{L}(p, q) = \set{(a, b) \in K^2 \mid p(a, b) = q(a, b) = 0}$$
    endlich ist,
    so muss auch die Lösungsmenge für $f$ und $g$ endlich sein: Ist $(a, b) \in
    K[x, y]$ eine Lösung, so muss $p_i(a, b) = q_j(a, b) = 0$ für ein $(i, j)
    \in \set{1, \dots, s} \times \set{1, \dots, t}$ gelten, und von solchen
    Lösungen gibt es dann nur endlich viele.

(b) Seien $f, g \in K[x, y] = K[x][y]$ irreduzibel und nichtassoziiert. Dann
    sind $f$ und $g$ auch in $Q(K[x])[y] = K(x)[y]$ irreduzibel. Dann kann es
    aber keine Nichteinheit als gemeinsamen Teiler von $f$ und $g$ geben, und
    die Polynome sind teilerfremd.

    Da $L = K(x)$ ein Körper ist, ist $L[y] = K(x)[y]$ ein Hauptidealbereich.
    Daher ist $\ideal{f, g} = \ideal{\ggT(f, g)} = \ideal{1} \subseteq L[y]$,
    und es gibt $\frac{p}{q}, \frac{r}{s} \in L[y]$ mit $p, r \in K[x, y], q, s
    \in K[x] \nozero$ und $1 = \frac{p}{q}f + \frac{r}{s}g$.

    Setze nun $h = qs \in K[x] \nozero$ sowie $a = ps \in K[x, y], b = qr \in
    K[x, y]$. Dann gilt
    $$h = h \cdot 1 = qs \cdot \left(\frac{p}{q}f + \frac{r}{s}g\right) = psf +
    qrg = af + bg.$$

(c) Seien $h, a, b$ wie in (b). Ist $(c, d) \in \mathbb{L}(f, g)$ eine
    gemeinsame Nullstelle von $f$ und $g$, so gilt $h(c) = h(c, d) = a(c, d)f(c,
    d) + b(c, d)g(c, d) = 0$.
    Da $h$ höchstens $\deg(h) \in \N$ Nullstellen besitzen kann, gibt es nur
    endlich viele Möglichkeiten für $c$. Wegen $K[x, y] = K[y, x]$ liefert
    dasselbe Argument, dass es nur endlich viele Möglichkeiten für $d$ gibt.
