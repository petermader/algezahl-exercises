# Algebra und Zahlentheorie -- Übungsblatt 12

## Aufgabe 1

(b) Die Polynome $f = x-1$ und $g = x^2+x+2$ sind über $\Q$ irreduzibel (für $g$
    verwende das Reduktionskriterium mit $p = 3$). Daher sind
    $K = \Q[x] / \ideal{f}$ und $L = \Q[x] / \ideal{g}$ Körper und der
    Chinesische Restsatz liefert
    $$\Q[x] / \ideal{x^3 + x - 2} = \Q[x] / \ideal{fg} = \Q[x] / (\ideal{f} \cap
    \ideal{g}) \cong (\Q[x] / \ideal{f}) \times (\Q[x] / \ideal{g}) = K \times
    L.$$

\newpage

## Aufgabe 2

\begin{enumerate}
    \item[{\includegraphics[width=7pt,height=7pt]{galgen.png}}]
    Es gibt $s, t \in R$ mit $1 = sc + td$. In $K$ gilt nun
    $$\frac{b}{d} = \frac{b}{d}(sc + td) = sb \cdot \frac{c}{d} + tb = sb \cdot
    \frac{a}{b} + tb = sa + tb \in \ideal{a, b} \subseteq R,$$
    also $\ideal{\frac{b}{d}} \subseteq \ideal{a, b}$. Ferner ist $b =
    \frac{b}{d} \cdot d \in \ideal{\frac{b}{d}}$ und
    $a = \frac{b}{d} \cdot c \in \ideal{\frac{b}{d}}$, also $\ideal{a, b}
    \subseteq \ideal{\frac{b}{d}}$.

    Somit ist $\ideal{a, b} = \ideal{\frac{b}{d}}$ ein Hauptideal.

    \item[$\Leftarrow$] Es gibt ein $r \in R$ mit $\ideal{a, b} =
    \ideal{r}$ und daher $c, d \in R$ mit $a = rc, b = rd$. Wegen $b \ne 0$ sind
    $r \ne 0$ und $d \ne 0$. In $K$ gilt nun $\frac{a}{b} = \frac{rc}{rd} =
    \frac{c}{d}$. Ferner ist
    $$cR + dR = \frac{rc}{r}R + \frac{rd}{r}R = \frac{1}{r}(rcR + rdR) =
    \frac{1}{r}(aR+bR) = \frac{1}{r}(rR) = R.$$
\end{enumerate}


\newpage

## Aufgabe 3

(a) Es sei $f = x^3 + 2x^2 + 3x + 3$. Das Polynom $\overline{f} = x^3 + x +
    \overline{1} \in \F_2[x]$ besitzt über $\F_2$ keine Nullstelle und ist daher
    über $\F_2$ irreduzibel.

    Mit dem Reduktionskriterium ist dann auch $f$ in $\Z[x]$ und damit in
    $\Q[x]$ irreduzibel.

(b) Genau dann ist $f$ über $\Q$ irreduzibel, wenn $f$ eine Nullstelle $q =
    \frac{r}{s} \in \Q$ mit $r \in \Z$ und $s \in \N_+$ besitzt.

    Nach dem Satz über rationale Nullstellen (Satz 9.1) gilt für eine solche
    Nullstelle $r \mid 2$ und $s \mid 1$, also $q \in \set{1, -1, 2, -2}$.

    Diese vier Nullstellen treten jeweils für genau einen Wert von $n$ auf:

    * $q = 1$: $f(q) = 0 \iff n = -4$.
    * $q = -1$: $f(q) = 0 \iff n = 2$.
    * $q = 2$: $f(q) = 0 \iff n = -7$.
    * $q = -2$: $f(q) = 0 \iff n = -1$.

    Für genau die $n$ in $\Z \setminus \set{-7, -4, -1, 2}$ ist $f$ damit
    irreduzibel über $\Q$.

(c) Laut dem Eisensteinkriterium mit $p = 2 \in \Z$ ist $x^2 + 2$ irreduzibel
    über $\Z$ und damit über $\Q$. Weil $\Q[x]$ ein faktorieller Ring ist, ist
    $x^2 + 2$ auch ein Primelement.

    Eine zweite Anwendung des Eisensteinkriteriums mit $p = x^2 + 2$ liefert nun
    die Irreduzibilität von $y^3 + x^2 + 2$ in $\Q[x, y]$.

(d) Formuliert man das Reduktionskriterium nicht wie in der Vorlesung nur für
    den Ring $\Z[x]$, sondern wie in [Fischer, 2.3.8(c)] für beliebige
    faktorielle Ringe, so lautet es:

    \paragraph{Reduktionskriterium.} Sei $R$ ein faktorieller Ring,
    $$f \coloneqq a_0 + a_1x + \cdots + a_nx^n \in R[x]$$
    ein primitives Polynom vom Grad $n \geqslant 1$ und $\pp \subseteq R$ ein
    Primideal derart, dass $a_n \notin \pp$. Ist
    $$\overline{R} \coloneqq R / \pp \qquad \text{und} \qquad \rho: R[x] \to
    \overline{R}[x], \quad f \mapsto \overline{f}$$
    der kanonische Homomorphismus und ist $\overline{f}$ irreduzibel in
    $\overline{R}[x]$, so ist $f$ irreduzibel in $R[x]$ und in $Q(R)[x]$.

    Von dieser Aussage werden wir nun Gebrauch machen, ohne dass sie in der
    Vorlesung bewiesen worden wäre. Es sei angemerkt, dass diese Form des
    Reduktionskriteriums auch in den Online-Vorlesungen vorkam (Umfragen zu
    Vorlesung 10, Fragen 2 und 3), ohne dass es in seiner allgemeinen Form
    auch nur formuliert worden wäre.

    Wir verwenden $R = \Q[x]$, $f \coloneqq y^4 + x^2y^2 + 2xy^2 + y^2 + x^2 -
    1 \in \Q[x, y]$ und $\pp = \ideal{x - 2} \subseteq \Q[x]$.

    Dann ist $\overline{f} = y^4 + \overline{9}y^2 + \overline{3} \in
    \overline{R}[y] = (\Q[x] / \ideal{x-2})[y] \cong \Q[y]$ nach dem
    Eisensteinkriterium mit $p = 3$ in $\Z[y]$ und damit in $\Q[y]$ irreduzibel.
    Das Reduktionskriterium liefert die Irreduzibilität von $f$ in $\Q[x, y]$.

\newpage

## Aufgabe 4

(a) Es lässt sich nachrechnen, dass für die formale Ableitung die Summen- und
    Produktregel der Ableitung gelten.

    Offenbar ist $0 \in I$. Sind $f, g \in I$ und $h \in \Q[x]$, so gilt
    $$(f-g)(0) = f(0) - g(0) = 0 = f'(0) - g'(0) = (f' - g')(0)$$
    sowie
    $$(hf)(0) = h(0)f(0) = 0 = h'(0)f(0) + h(0)f'(0) = (hf)'(0)$$
    und daher $f - g \in I$ und $hf \in I$.

(b) Wir zeigen $I = \ideal{x^2}$.

    Offenbar ist $x^2 \in I$, also $\ideal{x^2} \subseteq I$. Ist umgekehrt $f =
    \sum_{i=0}^{n} a_ix^i \in I$ mit $n \geqslant 2$ und $a_i \in \Q$, so gilt
    $0 = f(0) = a_0$ und $0 = f'(0) = a_1$. Dies zeigt
    $$f = x^2 \cdot \sum_{i=0}^{n-2} a_ix^i \in \ideal{x^2}.$$

(c) Da $x^2$ als reduzibles Element in dem faktoriellen Ring $\Q[x]$ kein
    Primelement ist, liefert Satz 11.8, dass $I = \ideal{x^2}$ kein Primideal
    ist.

\newpage

## Aufgabe 5

Zunächst unterscheiden wir nach der additiven Ordnung des Einselements. Es gilt
$\ord_R(1) \in \set{1, p, p^2}$, da die Ordnung ein Teiler von $\abs{R} = p^2$
sein muss.

* Wäre $\ord_R(1) = 1$, so wäre $0 = 1$ und daher
  $$R = \set{a \cdot 1 \mid a \in R} = \set{a \cdot 0 \mid a \in R} = \set{0}.$$

* Dagegen kann der Fall $\ord_R(1) = p^2$ tatsächlich auftreten. Dann ist die
  additive Gruppe von $R$ zyklisch und jedes Element $a \in R$ lässt sich
  eindeutig als $a = k \cdot 1$ für ein $k \in \set{0, \dots, p^2-1}$ schreiben.
  Die Abbildung
  $$\vfi: R \to \Z / p^2\Z, \qquad k \cdot 1 \mapsto k+p^2\Z$$
  ist dann ein Isomorphismus von Gruppen. Sie ist auch multiplikativ, denn für
  $a = k \cdot 1, b = l \cdot 1 \in R$ gilt
  $$\vfi(ab) = \vfi(kl \cdot 1) = \overline{kl} = \overline{k} \cdot
  \overline{l} = \vfi(a) \vfi(b) \qquad \text{und} \qquad \vfi(1) = \vfi(1 \cdot
  1) = 1 + p^2\Z.$$
  Dies zeigt $R \cong \Z / p^2\Z$.

* Nun bleibt noch der Fall $\ord_R(1) = p$. Dann ist die Abbildung
  $$\iota: \Z / p\Z \xhookrightarrow{} R, \qquad \overline{a} \mapsto a \cdot
  1$$
  ein wohldefinierter Monomorphismus von Ringen und $R$ enthält einen zu $\F_p$
  isomorphen Teilring. Somit ist $R$ eine $\F_p$-Algebra, insbesondere ein
  Vektorraum, mit Basis $\set{1, r}$ für ein $r \in R \setminus \iota(\Z /
  p\Z)$. Wegen der 2-Dimensionalität gibt es $a, b \in \F_p$ mit
  $r^2 = ar + b\cdot 1 = ar + b$ (wobei wir $a, b$ via $\iota$ als Elemente von
  $R$ auffassen).

  Nun kommt der nächste Isomorphismus ins Spiel: Wir erweitern $\iota$ mit der
  universellen Eigenschaft des Polynomrings zu
  $$\psi : (\Z / p\Z)[x] \twoheadrightarrow R, \qquad x \mapsto r.$$
  Nach den obigen Ausführungen ist $\psi$ ein Epimorphismus mit $\Ker(\psi) =
  \ideal{x^2-ax-b}$. Es gilt also $R \cong \F_p[x] / \ideal{x^2-ax-b}$. Wir
  unterscheiden nun noch nach den Nullstellen von $f = x^2-ax-b$.

  * Ist $f$ irreduzibel, so ist $\ideal{f}$ nach Beispiel 11.13 (b) ein
    maximales Ideal und $R \cong \F_p[x] / \ideal{f}$ ein Körper.

    Dieser Fall tritt auf, denn $f = x^p - x + 1$ ist irreduzibel in $\Z / p\Z$
    nach dem kleinen Satz von Fermat.
  * Ist $f = (x-c)^2$ für ein $c \in \F_p$, so ist
    $$R \cong \F_p[x] / \ideal{(x-c)^2} \cong \F_p[x] / \ideal{x^2}.$$
    Dieser Fall tritt offenbar für $c = 0$ auf.
  * Andernfalls ist $f = (x - c_1)(x - c_2)$ für $c_1, c_2 \in \F_p$ mit $c_1
    \ne c_2$. Der Chinesische Restsatz liefert
    $$\begin{aligned}
    R &\cong \F_p[x] / \ideal{(x-c_1)(x-c_2)} = \F_p[x] / (\ideal{x-c_1} \cap
    \ideal{x-c_2}) \\
    &\cong (\F_p[x] / \ideal{x-c_1}) \times (\F_p[x] / \ideal{x-c_2}) \cong \F_p
    \times \F_p.
    \end{aligned}$$
    Dieser Fall tritt offenbar für $c_1 = 0, c_2 = 1$ auf.

\newpage

## Aufgabe 6

\paragraph{Lemma 1:} In $S_4$ ist $A_4$ die einzige Untergruppe der Ordnung 12.
\vspace{-.5cm}
\paragraph{Beweis.} Sei $U \subseteq S_4$ eine Untergruppe mit $\ind{S_4}{U} =
2$. Angenommen, $U \ne A_4$. Da $A_4$ von den 3-Zyklen erzeugt wird, gibt es
einen 3-Zykel $\sigma \in A_4 \setminus U$.

Dann gilt $\sigma U \ne U$. Ferner gilt $\sigma^{-1} \notin U$, also
$\sigma^{-1}U \ne U$. Schließlich ist $\sigma U \ne \sigma^{-1}U$, denn
ansonsten wäre $U = \sigma^2 U = \sigma^{-1} U$. Also haben wir drei
verschiedene Nebenklassen, im Widerspruch zu $\ind{S_4}{U} = 2$. \qed

\paragraph{Lemma 2:} Die Gruppe $A_4$ operiert transitiv auf $M = \set{1, 2, 3,
4}$. \vspace{-.5cm}
\paragraph{Beweis.} Seien $i, j \in M$. Es gibt ein $k \in M \setminus \set{i,
j}$. Dann ist $\sigma = (i\;j\;k) \in A_4$ und $\sigma(i) = j$. \qed

---

Es sei $U \subseteq S_4$ eine solche Untergruppe. Da $M = \set{1, 2, 3, 4}$ die
disjunkte Vereinigung der Bahnen ist und es wegen der Transitivität nur eine
Bahn gibt, ist $M = U \cdot 1$ eine Bahn. Nach dem Bahnenlemma ist $\abs{M} =
\abs{U \cdot 1} = 4$ ein Teiler von $\abs{U}$. Also ist $\abs{U}$ durch 3 und
durch 4 und daher durch 12 teilbar.

Laut Lemma 1 sind $A_4$ und $S_4$ die einzigen Untergruppen mit durch 12
teilbarer Ordnung. Laut Lemma 2 operiert $A_4$ und als größere Gruppe auch $S_4$
transitiv auf $M$.

\newpage

## Aufgabe 7

(a) Im Folgenden seien $(a, b), (c, d), (e, f) \in G$.

    Assoziativität: Es gilt
    $$((a, b) \cdot (c, d)) \cdot (e, f) = (ac, ad+b) \cdot (e, f) = (ace,
    acf+ad+b) = (a, b) \cdot (ce, cf+d) = (a, b) \cdot ((c, d) \cdot (e, f)).$$

    Neutrales Element: Wähle $e_G \coloneqq (\overline{1}, \overline{0})$ und
    erhalte
    $$e_G \cdot (a, b) = (\overline{1} \cdot a, \overline{1} \cdot b +
    \overline{0}) = (a, b) = (a \cdot \overline{1}, a \cdot \overline{0} + b) =
    (a, b) \cdot e_G.$$

    Inverses Element: Wähle $(a, b)^{-1} \coloneqq (a^{-1}, -a^{-1}b)$ und
    erhalte
    $$(a, b) \cdot (a, b)^{-1} = (aa^{-1}, a(-a^{-1}b) + b) = e_G = (a^{-1}a,
    a^{-1}b - a^{-1}b) = (a, b)^{-1} \cdot (a, b).$$

(b) Es gilt
    $$(\overline{1}, \overline{1}) \cdot (\overline{2}, \overline{2}) =
    (\overline{2}, \overline{3}) \ne (\overline{2}, \overline{4}) =
    (\overline{2}, \overline{2}) \cdot (\overline{1}, \overline{1}).$$

(c) Sei $(\overline{1}, d) \in H$ und $(a, b) \in G$. Dann gilt
    $$(a, b) \cdot (\overline{1}, d) \cdot (a, b)^{-1} = (a, ad+b) \cdot
    (a^{-1}, -a^{-1}b) = (aa^{-1}, -aa^{-1}b+ad+b) = (\overline{1}, ad) \in H.$$
    Nun ist noch zu zeigen, dass
    $$\vfi: G \twoheadrightarrow \F_p^\times, \qquad (a, b) \mapsto a$$
    ein Epimorphismus von Gruppen mit $\Ker(\vfi) = H$ ist.

    Homomorphismus-Eigenschaft: Für $(a, b), (c, d) \in G$ gilt
    $$\vfi((a, b) \cdot (c, d)) = (ac, ad+b) = ac = \vfi((a, b)) \cdot \vfi((c,
    d)).$$

    Surjektivität: Ist $a \in \F_p^\times$, so ist $\vfi((a, \overline{0})) =
    a$.

    Kern: Für $(a, b) \in G$ gilt
    $$(a, b) \in H \iff a = \overline{1} \iff \vfi((a, b)) = a = \overline{1}.
    \iff (a, b) \in \Ker(\vfi).$$

(d) Die Aussage $\abs{H} = \abs{\set{\overline{1}} \times \F_p} =
    \abs{\set{\overline{1}}} \cdot \abs{\F_p} = 1 \cdot p = p$ ist klar.

    Es sei $U \subseteq G$ eine weitere Untergruppe der Ordnung $p$ in $G$.

    Wegen der Primalität von $p$ ist $U$ zyklisch, also $U = \ideal{(a, b)}$ für
    ein $(a, b) \in G$. Dann gilt $(\overline{1}, \overline{0}) = e_G = (a, b)^p
    = (a^p, c)$ für ein $c \in \F_p$, also $\ord_{\F_p^\times}(a) \mid p$.

    Wir haben $\ord_{\F_p^\times}(a) \mid p$ und $\ord_{\F_p^\times}(a) \mid p -
    1 = \abs{\F_p^\times}$, also $\ord_{\F_p^\times}(a) = 1$ und daher $a =
    \overline{1}$. Nun ist $p = \ord_G((a, b)) = \ord_G((\overline{1}, b)) =
    \ord_{\F_p}(b)$ und daher $\ideal{b} = \F_p$.

    Insgesamt folgt
    $$U = \ideal{(\overline{1}, b)} = \set{\overline{1}} \times \F_p = H.$$

\newpage

## Aufgabe 8

Schreibe $G / Z = \ideal{gZ}$ für ein $g \in G$. Nun seien $a, b \in G$. Es gibt
$n, m \in \Z$ mit $aZ = (gZ)^n = g^nZ$ und $bZ = (gZ)^m = g^mZ$. Daher gibt es
$z, w \in Z$ mit $a =zg^n$ und $b = wg^m$ und wir haben
$$ab = zg^nwg^m = zwg^{n+m} = wzg^{m+n} = wg^mzg^n = ba.$$

\newpage

## Aufgabe 9

\paragraph{Lemma 1:} Sei $n \geqslant 2$ und $\sigma \in A_n$ ein $k$-Zykel mit
$k$ ungerade. Dann ist $\sigma$ ein Quadrat in $A_n$.
\vspace{-.5cm}
\paragraph{Beweis.} Wegen $\ggT\left(k, \frac{k+1}{2}\right) = 1$ ist $\tau =
\sigma^{\frac{k+1}{2}}$ wieder ein $k$-Zykel und damit gerade. Wegen
$\ord_{S_n}(\sigma) = k$ gilt
$$\tau^2 = \left( \sigma^{\frac{k+1}{2}} \right)^2 = \sigma^{k+1} = \sigma$$
und $\sigma$ ist ein Quadrat in $A_n$. \qed

\paragraph{Lemma:} Sei $n \geqslant 2$ und seien $\sigma = (i_1\;\cdots\;i_k),
\tau = (j_1\;\cdots\;j_l) \in S_n$ Zykel positiver gerader Länge mit disjunkten
Wirkungsbereichen. Dann ist $\sigma\tau \in Q(A_n)$.
\vspace{-.5cm}
\paragraph{Beweis.} Für die Doppeltransposition $\pi = (i_{k-1}\;i_k)(j_1\;j_2)$
gilt mit Lemma 1:
$$\pi = (i_{k-1}\;i_k)(j_1\;j_2) = (i_{k-1}\;i_k\;j_1)(i_k\;j_1\;j_2) =
(i_{k-1}\;j_1\;i_k)^2(i_k\;j_2\;j_1)^2 \in Q(A_n).$$
Nun folgt mit Lemma 1:
$$\sigma\tau = \underbrace{(i_1\;\cdots\;i_{k-1})}_{\in
Q(A_n)}\underbrace{(i_{k-1}\;i_k)(j_1\;j_2)}_{\in
Q(A_n)}\underbrace{(j_2\;\cdots\;j_l)}_{\in Q(A_n)} \in Q(A_n),$$
wie gewünscht. \qed

---

(a) Ist $\pi = \prod_{i=1}^{n} \sigma_i^2 \in Q(S_4)$ mit $\sigma_i \in S_4$, so
    ist
    $$\sign(\pi) = \sigma\left( \prod_{i=1}^{n} \sigma_i^2 \right) =
    \prod_{i=1}^{n} \sign(\sigma_i)^2 = 1,$$
    also $\pi \in A_4$ und daher $Q(S_4) \subseteq A_4$.

    Jedes Quadrat in $A_4$ ist auch ein Quadrat in $S_4$. Daher gilt $Q(A_4)
    \subseteq Q(S_4)$ und mit (c) $A_4 \subseteq Q(A_4) \subseteq Q(S_4)$.

(b) Sei $\vfi \in \Aut(G)$ und $g = \prod_{i=1}^{n} g_i^2 \in Q(G)$ mit $g_i \in
    G$. Dann gilt
    $$\vfi(g) = \vfi\left( \prod_{i=1}^{n} g_i^2 \right) = \prod_{i=1}^{n}
    \vfi(g_i)^2 \in Q(G),$$
    also $\vfi(Q(G)) \subseteq Q(G)$ und mit der Bijektivität folgt $\vfi(Q(G))
    = Q(G)$.

(c) Die Inklusion $Q(A_n) \subseteq A_n$ ist klar.

    Umgekehrt sei $\pi \in A_n$. Ist $\pi = \sigma_1 \cdots \sigma_k \tau_1
    \cdots \tau_l$ die Zykelzerlegung von $\pi$ mit geraden Zyklen $\sigma_i$
    und ungeraden Zyklen $\tau_j$, so ist $l$ gerade.

    Laut Lemma 1 sind die $\sigma_i$ Quadrate in $A_n$. Laut Lemma 2 liegt das
    Produkt je zweier ungerader Zyklen $\tau_j, \tau_{j+1}$ in $Q(A_n)$. Daher
    ist
    $$\pi = \underbrace{\sigma_1}_{\in Q(A_n)} \cdots \underbrace{\sigma_k}_{\in
    Q(A_n)} \underbrace{\tau_1\tau_2}_{\in Q(A_n)} \cdots
    \underbrace{\tau_{l-1}\tau_l}_{\in Q(A_n)} \in Q(A_n).$$

\newpage

## Aufgabe 10

\paragraph{Notation:} Für $n \in \N_+$ und $\sigma \in S_n$ schreibe $W(\sigma)
\eqqcolon \set{i \in \set{1, \dots, n} \mid \sigma(i) \ne i}$ für den
Wirkungsbereich von $\sigma$.

\paragraph{Lemma:} Sei $n \in \N$ ungerade. Ist $\sigma \in S_n$ mit
$\ord_{S_n}(\sigma) = 2$, so gilt $0 < \abs{W(\sigma)} < n$ und insbesondere
$W(\sigma) \ne \varnothing \ne \set{1, \dots, n} \setminus W(\sigma)$.
\vspace{-.5cm}
\paragraph{Beweis.} Bei der Zyklenzerlegung muss $\sigma$ in Transpositionen
zerfallen. Der Wirkungsbereich von $\sigma$ ist dann die disjunkte Vereinigung
der Wirkungsbereiche dieser Transpositionen und besitzt daher eine gerade
Anzahl an Elementen. Wegen $\sigma \ne \Id$ ist $W(\sigma) \ne 0$. \qed

---

Die Gruppe $A_5 \times (\Z / 2\Z)$ besitzt mit $\set{\Id} \times (\Z / 2\Z)$
einen Normalteiler der Ordnung zwei. Wir zeigen, dass $S_5$ keinen Normalteiler
der Ordnung zwei besitzt.

Sei $U \subseteq S_5$ eine Untergruppe von $S_5$ mit $\abs{U} = 2$. Es gibt ein
$\sigma \in S_5$ mit $U = \ideal{\sigma}$, also $\ord_{S_5}(\sigma) = 2$. Laut
Lemma gibt es ein $i \in W(\sigma)$ und ein $j \in \set{1, \dots, 5} \setminus
W(\sigma)$. Wir haben
$$\tau\sigma\tau^{-1}(i) = \tau\sigma(j) = \tau(j) = i \ne \sigma(i)$$
sowie
$$\tau\sigma\tau^{-1}(j) = \tau\sigma(i) \ne \tau(i) = j = \Id(j),$$
also $\tau\sigma\tau \notin \set{\Id, \sigma} = U$. Daher ist $U$ kein
Normalteiler und $S_5 \not\cong A_5 \times (\Z / 2\Z)$.
