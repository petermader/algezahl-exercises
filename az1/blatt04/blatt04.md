# Algebra und Zahlentheorie -- Übungsblatt 4

## Aufgabe 1

\paragraph{Lemma 1:} Seien $R, S$ Ringe und sei $\vfi \colon R
\twoheadrightarrow S$ ein Epimorphismus von Ringen. Dann existiert ein
Homomorphismus $\alpha \colon S \to \quotient{R}{\Ker(\vfi)}$ mit
$\alpha(\vfi(a)) = a + \Ker(\vfi)$ für alle $a \in R$. \vspace{-0.3cm}
\paragraph{Beweis.} Sei $s \in S$. Da $\vfi$ surjektiv ist, gibt es ein $a \in
\vfi^{-1}(s)$. Wir wollen $\vfi^{-1}(s) = a + \Ker(\vfi)$ zeigen.

\begin{enumerate}
    \item[$\subseteq$] Ist $b \in \vfi^{-1}(s)$, so gilt $\vfi(b - a) =
        \vfi(b) - \vfi(a) = s - s = 0$, also $b-a \in \Ker(\vfi)$ und daher $b =
        a + (b - a) \in a + \Ker(\vfi)$.

    \item[$\supseteq$] Ist $b \in a + \Ker(\vfi)$, so gibt es ein $c \in
        \Ker(\vfi)$ mit $b = a + c$. Nun folgt $\vfi(b) = \vfi(a+c) = \vfi(a) +
        \vfi(c) = s + 0$ und daher $b \in \vfi^{-1}(s)$.
\end{enumerate}

Verwende nun $\alpha(s) = \vfi^{-1}(s)$. Ist dann $a \in R$, so gilt $a \in
\vfi^{-1}(\vfi(a))$, also $\alpha(\vfi(a)) = \vfi^{-1}(\vfi(a)) = a+\Ker(\vfi)$.

Um zu zeigen, dass $\alpha$ ein Homomorphismus ist, seien $s, t \in S$. Es gibt
$a, b \in R$ mit $\vfi(a) = s, \vfi(b) = t$. Dann gilt $\vfi(a+b)=s+t$ und
$\vfi(ab) = st$. Nun folgt
$$\alpha(s+t) = \vfi^{-1}(s+t) = a + b + \Ker(\vfi) = (a + \Ker(\vfi)) + (b +
\Ker(\vfi)) = \vfi^{-1}(s) + \vfi^{-1}(t) = \alpha(s) + \alpha(t)$$
sowie
$$\alpha(st) = \vfi^{-1}(st) = ab + \Ker(\vfi) = (a + \Ker(\vfi))(b +
\Ker(\vfi)) = \vfi^{-1}(s)\vfi^{-1}(t) = \alpha(s)\alpha(t).$$

Sind $R$ und $S$ unitär und gilt $\vfi(1) = 1$, so erhält $\alpha$ offenbar auch
das Einselement, denn $\alpha(1) = \vfi^{-1}(1) = 1 + \Ker(\vfi)$. \qed

\paragraph{Lemma 2:} Sei $R$ ein Ring und seien $I, J \subseteq R$ Ideale mit $I
\subseteq J$. Dann ist die Abbildung
$$\beta \colon \quotient{R}{I} \to \quotient{R}{J}, \qquad a+I \mapsto a+J$$
wohldefiniert und ein Ringhomorphismus.\vspace{-0.3cm}
\paragraph{Beweis.} Für die Wohldefiniertheit seien $a, b \in R$ mit $a+I =
b+I$. Dann ist $b-a \in I \subseteq J$, also
$$\beta(a+I) = a+J = (a+J) + (0+J) = (a+J) + (b-a+J) = b+J = \beta(b+I).$$
Für die Homomorphismus-Eigenschaft seien $a+I, b+I \in \quotient{R}{I}$. Dann
gilt
$$\beta((a+I)+(b+I)) = \beta(a+b+I) = a+b+J = (a+J) + (b+J) = \beta(a+I) +
\beta(b+I)$$
sowie
$$\beta((a+I)(b+I)) = \beta(ab+I) = ab+J = (a+J)(b+J) = \beta(a+I)\beta(b+I).$$
Ist $R$, so erhält $\beta$ offenbar auch das Einselement, denn $\beta(1+I) =
1+J$. \qed

\newpage

\paragraph{Lemma 3:} Seien $R, S$ Ringe und sei $\vfi \colon R \to S$ ein
Ringhomorphismus. Dann ist die Abbildung
$$\gamma \colon \quotient{R}{\Ker(\vfi)} \to S, \qquad a + \Ker(\vfi) \mapsto
\vfi(a)$$
wohldefiniert und ein Monomorphismus von Ringen.\vspace{-0.3cm}
\paragraph{Beweis.} Für die Wohldefiniertheit seien $a, b \in R$ mit $a +
\Ker(\vfi) = b + \Ker(\vfi)$. Dann gilt $\vfi(b-a) = 0$, also
$$\gamma(a + \Ker(\vfi)) = \vfi(a) = \vfi(a) + 0 = \vfi(a) + \vfi(b - a) =
\vfi(a+b-a) = \vfi(b) = \gamma(b + \Ker(\vfi)).$$
Für die Homomorphismus-Eigenschaft seien $a + \Ker(\vfi), b + \Ker(\vfi) \in
\quotient{R}{\Ker(\vfi)}$. Dann gilt
$$\gamma((a+\Ker(\vfi)) + (b + \Ker(\vfi))) = \gamma(a+b+\Ker(\vfi)) = \vfi(a+b)
= \vfi(a) + \vfi(b) = \gamma(a+\Ker(\vfi)) + \gamma(b+\Ker(\vfi))$$
sowie
$$\gamma((a+\Ker(\vfi))(b + \Ker(\vfi))) = \gamma(ab+\Ker(\vfi)) = \vfi(ab) =
\vfi(a)\vfi(b) = \gamma(a+\Ker(\vfi))\gamma(b+\Ker(\vfi)).$$
Für die Injektivität sei $a + \Ker(\vfi) \in \Ker(\gamma)$. Dann gilt $0 =
\gamma(a + \Ker(\vfi)) = \vfi(a)$, also $a \in \Ker(\vfi)$, und damit $a +
\Ker(\vfi) = 0 + \Ker(\vfi)$.
Sind $R$ und $S$ unitär und gilt $\vfi(1) = 1$ , so erhält $\gamma$ offenbar
auch das Einselement, denn $\gamma(1+\Ker(\vfi)) = \vfi(1) = 1$. \qed

\paragraph{Zur Aufgabe:}

(a) Verwende $\alpha : S' \to \quotient{R}{\Ker(\vfi')}$ aus Lemma 1, $\beta :
    \quotient{R}{\Ker(\vfi')} \to \quotient{R}{\Ker(\vfi)}$ aus Lemma 2 und
    $\gamma : \quotient{R}{\Ker(\vfi)} \to S$ und setze $\psi = \gamma \circ
    \beta \circ \alpha$. Es gilt also
    $$\psi : S' \xrightarrow{\;\alpha\;} \quotient{R}{\Ker(\vfi')}
    \xrightarrow{\;\beta\;} \quotient{R}{\Ker(\vfi)} \xrightarrow{\;\gamma\;}
    S.$$
    Als Komposition von Ringhomomorphismen ist $\psi$ ein Ringhomomorphismus.
    Ist $a \in R$, so gilt
    $$\psi(\vfi'(a)) = \gamma(\beta(\alpha(\vfi'(a)))) =
    \gamma(\beta(a+\Ker(\vfi'))) = \gamma(a+\Ker(\vfi)) = \vfi(a),$$
    also $\psi \circ \vfi' = \vfi$. Nun ist noch $\vfi'(\Ker(\vfi)) =
    \Ker(\psi)$ zu zeigen.

    \begin{enumerate}
        \item[$\subseteq$] Sei $s' \in \vfi'(\Ker(\vfi))$. Es gibt also ein $a
            \in \Ker(\vfi)$ mit $\vfi'(a) = s'$. Nun gilt
            $$\psi(s') = \psi(\vfi'(a)) = \vfi(a) = 0,$$
            also $s' \in \Ker(\psi)$.
        \item[$\supseteq$] Ist umgekehrt $s' \in \Ker(\psi)$, so schreibe $s' =
            \vfi'(a)$ für ein $a \in R$. Dann gilt
            $$0 = \psi(s') = \gamma(\beta(\alpha(s'))),$$
            und da $\gamma$ injektiv ist, folgt
            $$0 + \Ker(\vfi) = \beta(\alpha(s')) = \beta(\alpha(\vfi'(a))) =
            \beta(a + \Ker(\vfi')) = a + \Ker(\vfi).$$
            Dies zeigt $a \in \Ker(\vfi)$ und $s' = \vfi'(a) \in
            \vfi'(\Ker(\vfi))$.
    \end{enumerate}

\newpage

## Aufgabe 2

(a) Für $f = 0$ ist die Aussage offenbar erfüllt. Es gelte nun $f \ne 0$, also
    $n \geqslant 0$ und $a_n \ne 0$. Wir beweisen die Aussage per Induktion nach
    $\deg(f) = n$. Schreibe $f = a_nx^n + g$ mit $g = 0$ oder $\deg(g) < n$.

    \paragraph{Induktionsanfang:} Gilt $n = 0$, so ist $f = a_0$ offenbar
    nilpotent genau dann, wenn $a_0$ nilpotent ist.\vspace{-0.3cm}

    \paragraph{Induktionsschritt:} Sei $n > 0$. Die Aussage gilt laut
    Induktionsvoraussetzung für $g$.

    \begin{enumerate}
    \item[$\Rightarrow$] Sei $f$ nilpotent, d.h. $f^m = 0$ für ein $m \in \N$.
        Wir haben
        $$\begin{aligned}
        0 = f^m = \left( a_nx^n + g \right)^m &= \sum_{k=0}^{m} \binom{m}{k}
        \left(a_nx^n\right)^k g^{m-k} \\
        &= (a_nx^n)^m + \underbrace{\sum_{k=0}^{m-1} \binom{m}{k}
        \left(a_nx^n\right)^k g^{m-k}}_{=: h} = \left(a_n\right)^mx^{nm} + h.
        \end{aligned}$$
        Für $k \in \set{0, \dots, m-1}$ ist
        $$\deg\left(\left(a_nx^n\right)^k g^{m-k}\right) \leqslant nk + \deg(g)
        \cdot \underbrace{(m-k)}_{>0} < nk + n(m-k) = nm.$$
        Daher gilt
        $$\deg(h) = \deg\left( \sum_{k=0}^{m-1} \left(a_nx^n\right)^k g^{m-k}\right)
        < nm.$$
        Somit folgt aus $0 = \left(a_n\right)^mx^{nm} + h$, dass $h = 0$ und
        $\left(a_n\right)^m = 0$. Da $g = f - a_nx^n$ als Summe zweier
        nilpotenter Elemente nilpotent ist (Blatt 1, Aufgabe 4(a)), liefert die
        Induktionsvoraussetzung, dass auch $a_0, \dots, a_{n-1}$ nilpotent sind.

    \item[$\Leftarrow$] Sind $a_0, \dots, a_n$ nilpotent in $R \subseteq
        R[x]$, so auch $a_ix^i \in R[x]$ für $i \in \set{0, \dots, n}$. Daher
        ist $f$ als Summe nilpotenter Elemente ebenfalls nilpotent.
    \end{enumerate}

(b) Ist $f$ eine Einheit, so gilt $f \ne 0$ und wir können $a_n \ne 0$
    verlangen.

    Es gibt ein $g = b_0 + b_1x + \cdots + b_mx^m \in
    R[x] \nozero$ mit $m \geqslant 0, b_0, \dots, b_m \in R$ und $b_m \ne 0$,
    sodass
    $$1 = fg = \sum_{k=0}^{n+m} x^k
    \sum_{\substack{i\in\set{0,\dots,n},\\j\in\set{0,\dots,m},\\i+j=k}}
    a_ib_j.$$

    Damit ist schon einmal $a_0b_0 = 1$, also $a_0, b_0 \in R^\times$. Ferner
    gilt für $k \in \set{0, \dots, n+m}$:
    $$0 = \sum_{\substack{i\in\set{0,\dots,n},\\j\in\set{0,\dots,m},\\i+j=k}}
    a_ib_j =
    \sum_{\substack{i\in\set{0,\dots,n},\\j\in\set{0,\dots,m},\\i+(m-j)=k}}
    a_ib_{m-j}.$$

    Nun kann man mit starker endlicher Induktion zeigen, dass $(a_n)^{l+1}
    b_{m-l} = 0$ für alle $l \in \set{0, \dots, m}$. Für $l = m$ folgt dann
    $(a_n)^{m+1}b_0 = 0$, und da $b_0$ eine Einheit ist, ist $(a_n)^{m+1} =
    (a_n)^{m+1}b_0(b_0)^{-1} = 0$.

    Wir brauchen keinen Induktionsanfang. Für den Induktionsschritt sei $l \in
    \set{0, \dots, m}$. Laut Induktionsvoraussetzung gilt $(a_n)^j b_{m-j} = 0$
    für alle $j \in \set{0, \dots, l - 1}$. Nun haben wir

    $$\begin{aligned}
    0 = (a_n)^l \cdot 0
    &= (a_n)^l \cdot \left(
    \sum_{\substack{i\in\set{0,\dots,n},\\j\in\set{0,\dots,m},\\i+(m-j)=m+n-l}}
    a_ib_{m-j}\right) \\
    &= (a_n)^l \cdot \left(
    \sum_{\substack{i\in\set{0,\dots,n},\\j\in\set{0,\dots,l},\\i-j=n-l}}
    a_ib_{m-j}\right) \\
    &= (a_n)^l \cdot a_nb_{m-l} + (a_n)^l \cdot \left(
    \sum_{\substack{i\in\set{0,\dots,n-1},\\j\in\set{0,\dots,l-1},\\i-j=n-l}}
    a_ib_{m-j}\right) \\
    &= (a_n)^l \cdot a_nb_{m-l} + \left(
    \sum_{\substack{i\in\set{0,\dots,n-1},\\j\in\set{0,\dots,l-1},\\i-j=n-l}}
    (a_n)^l a_ib_{m-j}\right) \\
    &= (a_n)^{l+1} \cdot b_{m-l} + \left(
    \sum_{\substack{i\in\set{0,\dots,n-1},\\j\in\set{0,\dots,l-1},\\i-j=n-l}}
    (a_n)^{l-j} a_i \underbrace{(a_n)^j b_{m-j}}_{=0}\right) \\
    &= (a_n)^{l+1}b_{m-l}.
    \end{aligned}$$

    Zum besseren Verständnis dieser beachtlichen Formel noch mal mit weniger
    Quantoren: Es gilt $a_nb_m = 0$ und $a_{n-1}b_m + a_nb_{m-1} = 0$. Wir
    erhalten
    $$0 = a_n \cdot 0 = a_n(a_{n-1}b_m + a_nb_{m-1}) = a_{n-1}a_nb_m +
    (a_n)^2b_{m-1} = (a_n)^2b_{m-1}.$$
    Wegen $a_{n-2}b_m + a_{n-1}b_{m-1} + a_nb_{m-2} = 0$ gilt dann auch
    $$0 = (a_n)^2(a_{n-2}b_m + a_{n-1}b_{m-1} + a_nb_{m-2}) =
    (a_n)^3b_{m-2} + a_{n-1}(a_n)^2b_m + a_n(a_n)^2b_{m-2} = (a_n)^3b_{m-2}.$$
    So fortfahrend kann man sich durch sämtliche Summanden der obigen Summen
    hangeln und $(a_n)^{m+1}b_0 = 0$ erhalten.

(c) Für $f = 0$ ist die Aussage offenbar erfüllt. Es gelte nun $f \ne 0$, also
    $n \geqslant 0$ und $a_n \ne 0$. Wir beweisen die Aussage per Induktion nach
    $\deg(f) = n$. Schreibe $f = a_nx^n + g$ mit $g = 0$ oder $\deg(g) < n$.

    \paragraph{Induktionsanfang:} Gilt $n = 0$, so ist $f = a_0$ offenbar
    genau dann eine Einheit, wenn $a_0$ eine Einheit ist.\vspace{-0.3cm}

    \paragraph{Induktionsschritt:} Sei $n > 0$. Die Aussage gilt laut
    Induktionsvoraussetzung für $g$.

    \begin{enumerate}
        \item[$\Rightarrow$] Ist $f$ eine Einheit, so gilt laut (b), dass $a_n$
            nilpotent ist und dass $a_0 \in R^\times$. Laut Blatt 1, Aufgabe
            4(b) ist die Summe einer Einheit und eines nilpotenten Elements eine
            Einheit. Also ist $g = f - a_nx^n \in \left(R[x]\right)^\times$, und
            die Induktionsvoraussetzung liefert, dass $a_1, \dots, a_{n-1}$
            nilpotent sind.
        \item[$\Leftarrow$]
            Ist $a_0 \in R^\times$ und sind $a_1, \dots, a_n$ nilpotent, so ist
            laut Induktionsvoraussetzung $g$ eine Einheit. Wieder liefert Blatt
            1, Aufgabe 4(b), dass $f = a_nx^n + g \in (R[x])^\times$.
    \end{enumerate}

## Aufgabe 3

Für jeden Automorphismus $\vfi$ von $K[x]$, der $K$ identisch abbildet, gibt es
ein eindeutiges $(a, b) \in K^\times \times K$ mit $\vfi(x) = ax+b$. Umgekehrt
gibt es zu $(a, b) \in K^\times \times K$ einen eindeutigen Automorphismus von
$K[x]$, der $K$ identisch abbildet und $\vfi(x) = ax+b$ erfüllt.

Sei $\vfi: K[x] \to K[x]$ ein Automorphismus, der $K$ identisch abbildet.
Für ein Polynom $f = \sum_{i=0}^{n} a_ix^i \in K[x]$ gilt
$$\vfi(f) = \sum_{i=0}^{n} a_n\vfi(x)^i,$$
also $\deg(\vfi(f)) = n \cdot \deg(\vfi(x))$.

Wäre $\vfi(x) \in K$, so wäre $\vfi$ offenbar nicht injektiv. Wäre
$\deg(\vfi(x)) > 1$, dann gälte $x \notin \Img(\vfi)$, denn für alle
$f \in K[x]$ wäre $\deg(\vfi(f)) = \deg(f) \cdot \deg(\vfi(x)) \ne 1 = \deg(x)$.
Dann wäre $\vfi$ nicht surjektiv. Daher ist $\deg(\vfi(x)) = 1$, und es gibt ein
eindeutiges $(a, b) \in K^\times \times K$ mit $\vfi(x) = ax + b$.

Umgekehrt sei nun $(a, b) \in K^\times \times K$. Laut der universellen
Eigenschaft für den Polynomring kann man den Monomorphismus $K
\xhookrightarrow{} K[x], k \mapsto k$ in eindeutiger Weise zu einem
Ringhomorphismus $\vfi$ mit $\vfi(x) = ax+b$ erweitern. Dieser ist bijektiv, da
durch
$$\vfi^{-1} \colon K[x] \to K[x], \qquad \sum_{i=0}^{n} a_ix^i \mapsto
\sum_{i=0}^{n} a_i\left(a^{-1}(x-b)\right)^i$$
eine Umkehrabbildung gegeben ist.

## Aufgabe 4

(a) Offenbar ist $f \ne 0$. Schreibe $f = \sum_{i=0}^n c_ix^i$ mit $n
    \geqslant 0$, $c_0, \dots, c_n \in \Z$ und $c_n \ne 0$.

    Für $m \in \N$ gilt
    $$a^m - b^m = (a - b) \underbrace{\sum_{i=0}^{m-1} a^ib^{m-i-1}}_{=: d_m}.$$

    Es folgt
    $$q = f(a) - f(b) = \left(\sum_{i=0}^{n} c_ia^i\right) -
    \left(\sum_{i=0}^{n} c_ib^i\right)
    = \sum_{i=0}^{n} c_i \left(a^i - b^i\right)
    = \sum_{i=0}^{n} c_n(a - b)d_i = (a - b) \sum_{i=0}^{n} c_id_i,$$
    also $a - b \mid q$, und daher $a-b \in \set{-q, -1, 1, q}$.

(b) Verwende $u_1 = 0, u_2 = 1, u_3 = 2, u_4 = 3$ und $f = \pm g$ mit $g = x^2 -
    3x + 1$. Es gilt dann $f(u_1) = f(u_4) = \pm 1$ und $f(u_2) = f(u_3) = \mp
    1$.

\newpage

## Aufgabe 5

Zerlegungen über $\Z$ sind
$$\begin{aligned}
f_1 &= x^4 - x^3 - 18x^2 + 52x - 40 = (x-2)^3(x+5), \\
f_2 &= 4x^3 - 3x^2 - 36x + 52 = (x-2)^2(4x+13), \\
f_3 &= 6x^2 - 3x - 18 = (x-2)(6x+9).
\end{aligned}$$

Über $\F_3$ ist $f_1 = (x-2)^3(x+2), f_2 = (x-2)^3$ und $f_3 = 0$. Daher ist
$(x-2)^3$ ist ein größter gemeinsamer Teiler, und weil dieser eindeutig bestimmt
ist, muss $t$ dazu assoziiert sein.

Über $\F_7$ ist $f_1 = (x-2)^4, f_2 = (x-2)^2(4x+6)$ und $f_3 = -(x-2)^2$. Daher
ist $(x-2)^2$ ist ein größter gemeinsamer Teiler, und weil dieser eindeutig
bestimmt ist, muss $t$ dazu assoziiert sein.

Über $\F_p$ mit $p \notin \set{3, 7}$ sind $x - 2$ und $6x+9$ nicht assoziiert,
denn $u \cdot (x-2) = 6x+9$ mit einer Einheit $u \in \F_p \nozero$ würde $u = 6$
und $9 = -2u = -12$ nach sich ziehen, also $0 = 21 = 3 \cdot 7$, und das kann
nur über $\F_3$ und $\F_7$ geschehen. Daher ist hier $x-2$ der größte
gemeinsame Teiler.

## Aufgabe 6

Vom Grad 1: $x, x+1$.

Vom Grad 2: $x^2 + x + 1$, das einzige quadratische Polynom ohne Nullstelle.

Vom Grad 3: $x^3 + x^2 + 1, x^3 + x + 1$, die kubischen Polynome ohne
Nullstelle.

Vom Grad 4: $x^4 + x^3 + x^2 + x + 1, x^4 + x^3 + 1, x^4 + x + 1$, die
quartischen Polynome ohne Nullstelle, die $x^2+x+1$ nicht als Teiler besitzen.

Vom Grad 5: $x^5+x^4+x^3+x^2+1, x^5+x^4+x^3+x+1, x^5+x^4+x^2+x+1,
x^5+x^3+x^2+x+1, x^5+x^3+1, x^5+x^2+1$, die quintischen Polynome ohne
Nullstelle, die $x^2+x+1$ nicht als Teiler besitzen.
