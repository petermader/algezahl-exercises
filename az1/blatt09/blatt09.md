# Algebra und Zahlentheorie -- Übungsblatt 9

## Aufgabe 1

\paragraph{Lemma:} Sei $n \in \N$. Für Permutationen $\pi_1, \pi_2 \in S_n$
seien $\pi_1 = \sigma_1 \cdots \sigma_s$ und $\pi_2 = \tau_1 \cdots \tau_t$ mit
Zyklen der Längen $k_1, \dots, k_s, l_1, \dots, l_t$ die
Zyklenzerlegungen.\footnote{Mit \textit{Zyklenzerlegung} ist hier immer die bis
auf Reihenfolge eindeutige Zerlegung einer Permutation in Zyklen mit disjunkten
Wirkungsbereichen gemeint.} Die Permutationen $\pi_1, \pi_2$ sind konjugiert
genau dann, wenn $\set{k_1, \dots, k_s} = \set{l_1, \dots, l_t}$.
\vspace{-0.5cm}
\paragraph{Beweis.} \begin{enumerate}
    \item[$\Rightarrow$] Sind $\pi_1$ und $\pi_2$ konjugiert, so gibt es ein
        $\vfi \in S_n$ mit $\pi_1 = \vfi\pi_2\vfi^{-1}$.

        Nach Satz 16.18 (a) ist für $j \in \set{1, \dots, t}$ neben $\tau_j$
        auch $\vfi\tau_j\vfi^{-1}$ ein $l_j$-Zyklus. Mit
        $$\sigma_1 \cdots \sigma_s = \pi_1 = \vfi\pi_2\vfi^{-1} =
        (\vfi\tau_1\vfi^{-1}) \cdots (\vfi\tau_t\vfi^{-1})$$
        und der Eindeutigkeit der Zyklenzerlegungen bis auf Reihenfolge erhalten
        wir $\set{k_1, \dots, k_s} = \set{l_1, \dots, l_t}$.

    \item[$\Leftarrow$] Es gelte $\set{k_1, \dots, k_s} = \set{l_1, \dots,
        l_t}$, und insbesondere $s = t$.

        Da die Zyklen $\tau_1, \dots, \tau_t$ kommutieren,
        können wir $k_1 = l_1, \dots, k_s = l_s = l_t$ annehmen. Wir nehmen
        ferner an, dass 1-Zykel verwendet werden, d.h. $\sum_{i=1}^{s} k_i = n$.

        Die Permutation $\vfi$ sei wie folgt definiert:

        Für $i = 1, \dots, s$ schreibe $\sigma_i = (a_1\;\cdots\;a_{k_i})$ und
        $\tau_i = (b_1\;\cdots\;b_{k_i})$. Da die Wirkungsbereiche der
        $\sigma_1, \dots, \sigma_s$ und der $\tau_1, \dots, \tau_s$ disjunkt
        sind, sind $\vfi(b_j)$ und $\vfi^{-1}(a_j)$ noch nicht definiert. Setze
        $\vfi(b_j) = a_j$ für\ $j \in \set{1, \dots, k_i}$.

        Wegen $\sum_{i=1}^{s} k_i = n$ ist die Permutation $\vfi$ nun
        vollständig definiert. Für $i \in \set{1, \dots, s}$ gilt nach
        Satz\ 16.18\ (b):
        $$\sigma_i = (a_1\;\cdots\;a_{k_i}) = (\vfi(b_1)\;\cdots\;\vfi(b_{k_i}))
        = \vfi(b_1\;\cdots\;b_{k_i})\vfi^{-1} = \vfi\tau_i\vfi^{-1}$$
        und daher
        $$\pi_1 = \sigma_1 \cdots \sigma_s =
        (\vfi\tau_1\vfi^{-1})\cdots(\vfi\tau_s\vfi^{-1}) = \vfi\pi_2\vfi^{-1}.$$
        Somit sind $\sigma$ und $\tau$ konjugiert. \qed
\end{enumerate}

---

(a) Es gilt
    $$\tau = \underbrace{(1\;3\;7)}_{\eqqcolon \sigma_1}
    \underbrace{(2\;10)}_{\eqqcolon \sigma_2}
    \underbrace{(4\;8\;6\;5)}_{\eqqcolon \sigma_3},$$
    sowie
    $$\sign(\tau) = \sign(\sigma_1)\sign(\sigma_2)\sign(\sigma_3) = 1 \cdot
    (-1) \cdot (-1) = 1,$$
    und
    $$\ord_{S_{10}}(\tau) = \kgV(\ord_{S_{10}}(\sigma_1),
    \ord_{S_{10}}(\sigma_2), \ord_{S_{10}}(\sigma_3)) = \kgV(3, 2, 4) = 12.$$

(b) Nach dem Lemma sind die Permutationen zu zählen, die dieselbe Zyklenstruktur
    wie $\tau$ besitzen. Es müssen vier Permutationen für den 4-Zykel
    identifiziert werden, drei für den 3-Zykel und zwei für den 2-Zykel. Es gibt
    also $\frac{10!}{4\cdot 3\cdot 2} = 151200$ zu $\tau$ konjugierte Elemente.

(c) Ist $\sigma = \sigma_1 \cdots \sigma_n$ die Zerlegung von $\sigma$ in Zyklen
    mit disjunkten Wirkungsbereichen, so ist
    $$p = \ord_{S_p}(\sigma) = \kgV(\ord_{S_p}(\sigma_1), \dots,
    \ord_{S_p}(\sigma_n)).$$
    Es gibt also ein $i \in \set{1, \dots, n}$, sodass $\ord_{S_p}(\sigma_i) =
    p$. Wegen der Disjunktheit der Wirkungsbereiche ist außerdem
    $$\sum_{i=1}^{n} \ord_{S_p}(\sigma_i) \leqslant p.$$
    Somit ist $\ord_{S_p}(\sigma_j) = 0$ für $j \in \set{1, \dots, n} \setminus
    \set{i}$ und daher ist $\sigma = \sigma_i$ ein $p$-Zyklus.

\newpage

## Aufgabe 2

(a) Es sei $\pi = (1\;2\;\cdots\;n)$.

    Betrachte die Permutation $\sigma = \pi^{i-1}$. Wegen $\sigma^n =
    \pi^{n(i-1)} = \Id^{i-1} = \Id$ gilt $\ord_{S_n}(\sigma) \mid n$, und wegen
    $\sigma \ne \Id$ ist $\ord_{S_n}(\sigma) \ne 1$. Die Primalität von $n$
    liefert $\ord_{S_n}(\sigma) = n$. Laut Aufgabe 1 (c) ist dann $\sigma$ ein
    $n$-Zyklus.

    Mithilfe einer Bijektion $\set{1, \dots, n} \to \set{1, \dots, n}$
    kann man die Elemente so umbenennen, dass $\sigma = (1\;i\;\cdots\;j)$ der
    Permutation $\pi = (1\;2\;\cdots\;n)$ und $\tau = (1\;i)$ der Permutation
    $(1\;2)$ entspricht. Dann gilt
    $$S_n = \ideal{(1\;2), \pi} \cong \ideal{\sigma, \tau}.$$
    Wegen $\sigma = \pi^{i-1} \in \ideal{\pi}$ gilt
    $$S_n = \ideal{\pi, \tau} = \ideal{(1\;2\;\cdots\;n), (1\;i)}.$$

(b) Schreibe $\pi_1 = (1\;3)$ und $\pi_2 = (1\;2\;3\;4)$.

    Für $a, b \in \set{1, 2, 3, 4}$ mit $a+2\Z = b+2\Z$ gilt
    $\pi_1(a) - \pi_1(b) \in 2\Z$ und $\pi_2(a) - \pi_2(b) \in 2\Z$ und daher
    $\pi(a) - \pi(b) \in 2\Z$ für alle $\pi \in \ideal{\pi_1, \pi_2} \subseteq
    S_4$.

    Mit $\tau = (1\;2) \in S_4$ gilt aber $\tau(3) - \tau(1) = 3 - 2 = 1 \notin
    2\Z$, also $\tau \notin \ideal{\pi_1, \pi_2}$.

\newpage

## Aufgabe 3

(a) Ja, den gibt es.

    Betrachte $\pi = (1 \; 2)(3 \; 4 \; 5 \; 6 \; 7) \in S_7$. Dann gilt
    $$\ord_{S_7}(\pi) = \kgV(\ord_{S_7}((1 \; 2)), \ord_{S_7}((3 \; 4 \; 5 \; 6
    \; 7))) = \kgV(2, 5) = 10.$$
    Also ist $\Z / 10\Z \cong C_{10} \cong \ideal{\pi} \subseteq S_7$ und
    $$\iota: \Z / 10\Z \xhookrightarrow{} S_7, \qquad a+10\Z \mapsto \pi^a$$
    ist injektiv.

(b) Nein, den gibt es nicht.

    Angenommen, $\iota \colon \Z / 8\Z \xhookrightarrow{} S_7$ wäre injektiv.
    Dann gälte
    $$C_8 \cong \Z / 8\Z \cong \Img(\iota) = \ideal{\iota(1 + 8\Z)}.$$
    Wir hätten also ein Element $\pi = \iota(1 + 8\Z) \in S_7$ der Ordnung 8.
    Sei $\pi = \sigma_1 \cdots \sigma_n$ die Zykelzerlegung von $\pi$. Für $i
    \in \set{1, \dots, n}$ gälte dann $\ord_{S_7}(\sigma_i) < 8$ und
    $$\ord_{S_7}(\sigma_i) \mid \kgV(\ord_{S_7}(\sigma_1), \dots,
    \ord_{S_7}(\sigma_n)) = \ord_{S_7}(\pi) = 8.$$
    Das lieferte
    $\ord_{S_7}(\sigma_i) \in \set{1, 2, 4}$ für $i \in \set{1, \dots, n}$, also
    $$\ord_{S_7}(\pi) = \kgV(\ord_{S_7}(\sigma_1), \dots, \ord_{S_7}(\sigma_n))
    < 8,$$
    Widerspruch.

(c) Für jeden Gruppenhomomorphismus $\vfi: \Z / 5\Z \to S_4$ ist $\Ker(\vfi)
    \trianglelefteq \Z / 5\Z$, also
    $$\Ker(\vfi) \in \set{\set{0+5\Z}, \; \Z / 5\Z}.$$

    * Gilt $\Ker(\vfi) = \Z / 5\Z$, so ist $\vfi: a+5\Z \mapsto \Id$ der
      triviale Homomorphismus.
    * Gilt $\Ker(\vfi) = \set{0 + 5\Z}$, so ist $\vfi$ injektiv und
      $$C_5 \cong \Z / 5\Z \cong \Img(\vfi) \cong \ideal{\vfi(1+5\Z)} \subseteq
      S_4.$$
      Also besitzt $\pi = \vfi(1+5\Z)$ in $S_4$ die Ordnung 5. Das geht aber
      nicht (siehe (b)), denn die Ordnungen der Zykel in $S_4$ sind echt kleiner
      als 5 und 5 ist nicht kleinstes gemeinsames Vielfaches der Zahlen $\set{1,
      2, 3, 4}$. Dieser Fall ist also unmöglich.

    Somit ist der triviale Homomorphismus der einzige Gruppenhomomorphismus
    $\vfi \colon \Z / 5\Z \to S_4$.

\newpage

## Aufgabe 4

\paragraph{Lemma:} Ist $U \subseteq S_4$ eine Untergruppe mit $\abs{U} = 6$, so
gilt $U \cong S_3$. \vspace{-.5cm}
\paragraph{Beweis.} Bis auf Isomorphie existieren nur zwei Gruppen sechster
Ordnung, nämlich $C_6$ und $S_3$. Da $A_4$ acht 3-Zyklen, drei
Doppeltranspositionen und die Identität und damit kein Element sechster Ordnung
enthält, gilt $U \not\cong C_6$. Es folgt $U \cong S_3$. \qed

---

(a) Angenommen, es gäbe eine solche Untergruppe $U \subseteq A_4$ mit $\abs{U} =
    6$. Laut Lemma gilt $U \cong S_3$. Also enthält $U$ drei Elemente zweiter
    Ordnung, in $A_4$ sind dies genau die drei Doppeltranspositionen $\tau_1 =
    (1\;2)(3\;4), \tau_2 = (1\;3)(2\;4), \tau_3 = (1\;4)(2\;3)$. Dann ist aber
    $V_4 \cong \ideal{\tau_1, \tau_2, \tau_3}$ eine Untergruppe von $U$,
    im Widerspruch zu $\abs{V_4} = 4 \nmid 6 = \abs{U}$.

(b) Siehe Lemma.

(c) Auch $U = G \,\cap\, A_n \subseteq A_n \subseteq S_n$ ist eine Untergruppe.
    Wegen $G \not\subseteq A_n$ ist $G \setminus A_n \ne \varnothing$, es gibt
    also ein\ $\sigma \in G \setminus A_n$. Für die Linksnebenklasse $\sigma U$
    gilt $\abs{\sigma U} = \abs{U}$.

    Wir wollen noch $G = U \;\dot\cup\; \sigma U$ zeigen, denn dann gilt
    $\abs{G} = \abs{U} + \abs{\sigma U} = 2 \abs{U}$.

    Die Elemente von $U$ sind sämtlich gerade, die von $\sigma U$ sämtlich
    ungerade. Die Vereinigung $U \cup \sigma U$ ist also disjunkt.

    \begin{enumerate}
        \item[$\subseteq$] Ist $\pi \in G \setminus U$, so gibt es nach
            Bemerkung 16.15 ein $\alpha \in A_n$ mit $\pi = \sigma\alpha$. Es
            gilt $\alpha = \sigma^{-1}\pi \in G$, also $\alpha \in G \cap A_n =
            U$, und daher $\pi \in \sigma U$.
        \item[$\supseteq$] Klar, weil $G$ multiplikativ abgeschlossen ist.
    \end{enumerate}

\newpage

## Aufgabe 5

Wir fassen $D_4 = \set{\Id, \rho, \rho^2, \rho^3, \sigma_1, \sigma_2, \sigma_3,
\sigma_4} \subseteq S_4$ als Untergruppe von $S_4$ auf, wobei
$$\rho = (1\;2\;3\;4), \rho^2 = (1\;3)(2\;4), \rho^3 = (1\;4\;3\;2), \sigma_1 =
(1\;3), \sigma_2 = (2\;4), \sigma_3 = (1\;4)(2\;3), \sigma_4 = (1\;2)(3\;4).$$

Es gilt dann $D_4 = \ideal{\rho, \sigma} = \set{\Id, \rho, \rho^2, \rho^3,
\sigma, \rho\sigma, \rho^2\sigma, \rho^3\sigma}$ mit der Rotation $\rho =
(1\;2\;3\;4)$ und der Spiegelung $\sigma = \sigma_1 = (1\;3)$.

\paragraph{Untergruppen erster Ordnung:} Hier gibt es nur $\set{\Id} \cong C_1$.

\paragraph{Untergruppen zweiter Ordnung:} Die Elemente $\rho^2, \sigma,
\rho\sigma, \rho^2\sigma, \rho^3\sigma$ sind von der Ordnung zwei, es gilt
$$\ideal{\rho^2} \cong \ideal{\rho\sigma} \cong \ideal{\rho^2\sigma} \cong
\ideal{\rho^3\sigma} \cong C_2.$$

\paragraph{Untergruppen vierter Ordnung:} Die Gruppe $\ideal{\rho} =
\ideal{\rho^3} \cong C_4$ ist zyklisch.

Außerdem gibt es die Untergruppen $\ideal{\rho^2, \sigma} \cong V_4$
und $\ideal{\rho^2, \rho\sigma} \cong V_4$.

\paragraph{Untergruppen achter Ordnung:} Die Gruppe $D_4$ ist eine Untergruppe
von $D_4$ und die einzige der Ordnung acht.

\newpage

## Aufgabe 6

(a) Identifiziert man die Ecken des Würfels mit den Seitenflächen des Oktaeders,
    so sind die Symmetrien des Würfels genau die des Oktaeders.

(b) Wir wollen zeigen, dass die Symmetriegruppe eines regulären Oktaeders
    isomorph zu $S_4 \times C_2$ ist.

    Es gibt die folgenden orientierungserhaltenden Symmetrien:

    * Für jede der drei Diagonalen gegenüberliegender Ecken gibt es je eine
      Rotation um $90^{\circ}$ (Ordnung\ 4), um $180^{\circ}$ (Ordnung 2) und um
      $270^{\circ}$ (Ordnung 4). Diese entsprechen den sechs 4-Zyklen und den
      drei Doppeltranspositionen.
    * Für jede der vier Achsen durch die Mittelpunkte zweier gegenüberliegender
      Seiten gibt es je eine Rotation um $120^{\circ}$ (Ordnung 3) und um
      $240^{\circ}$ (Ordnung 3). Diese entsprechen den acht 3-Zyklen.
    * Für jede der sechs Achsen durch die Mittelpunkte zweier gegenüberliegender
      Kanten gibt es eine Rotation um $180^{\circ}$ (Ordnung 2). Diese
      entsprechenden den sechs Transpositionen.
    * Die Identität (Ordnung 1).

    An den Elementordnungen lässt sich erkennen, dass diese
    orientierungserhaltenden Symmetrien die Gruppe $S_4$ bilden. Jede
    orientierungsumkehrende Symmetrie ergibt sich als Produkt einer
    orientierungserhaltenden Symmetrie und einer festen Spiegelung (Ordnung 2).
    Daher ist die Symmetriegruppe des regulären Oktaeders isomorph zu $S_4
    \times C_2$. Diese besitzt $\abs{S_4 \times C_2} = \abs{S_4} \cdot \abs{C_2}
    = 24 \cdot 2 = 48$ Elemente.

(c) Wir wollen die Elemente von $G = S_4 \times C_2$ der Ordnung 3 zählen. Ist
    $a = (\pi, b+2\Z) \in S_4 \times (\Z / 2\Z)$, so gilt $\ord_G(a) =
    \kgV(\ord_{S_4}(\pi), \ord_{\Z / 2\Z}(b+2\Z))$.

    Also gilt $\ord_G(a) = 3$ genau dann, wenn $\ord_{S_4}(\pi) = 3$ und
    $\ord_{\Z / 2\Z}(b+2\Z) = 1$ (d.h. $b+2\Z = 0+2\Z)$. In $S_4$ existieren
    acht Elemente der Ordnung 3. Daher besitzt $G$ ebenfalls acht Elemente der
    Ordnung 3.
