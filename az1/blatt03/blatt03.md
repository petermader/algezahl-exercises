# Algebra und Zahlentheorie -- Übungsblatt 3

\newcommand\Idem{\mathrm{Idem}}
\newcommand\Nil{\mathrm{Nil}}
\newcommand\ideal[1]{\left<#1\right>}

## Aufgabe 1

(a) Es gilt $(e')^2 = (1 - e)^2 = 1 - e - e + e^2 = 1 - e - e + e = 1 - e = e'$.

(b) $Re$ und $Re'$ sind bekanntermaßen Ideale und damit Unterringe von $R$
    (wobei $Re'$ nicht notwendigerweise unitär ist).

    Betrachte $\vfi: R \to Re \times Re',\; a \mapsto (ae, ae')$.

    Die Abbildung $\vfi$ ist ein Homomorphismus von Ringen: für $a, b \in R$
    gilt
    $$\vfi(a + b) = ((a+b)e, (a+b)e') = (ae + be, ae' + be') = (ae, ae') +
    (be, be') = \vfi(a) + \vfi(b)$$
    sowie
    $$\vfi(ab) = (abe, abe') = (abe^2, ab(e')^2) = ((ae)(be), (ae')(be')) = (ae,
    ae') \cdot (be, be') = \vfi(a)\vfi(b).$$

    Die Abbildung $\vfi$ ist injektiv: Für $a, b \in R$ mit $\vfi(a) = \vfi(b)$
    gilt $(ae, ae') = \vfi(a) = \vfi(b) = (be, be')$, also
    $$a - ae = a(1-e) = ae' = be' = b(1-e) = b - be = b - ae,$$
    und beidseitige Addition von $ae$ liefert $a = b$.

    Die Abbildung $\vfi$ ist surjektiv: Sei $(x, y) \in Re \times Re'$. Wegen $x
    \in Re$ gibt es ein $a \in R$ mit $x = ae$, wegen $y \in Re'$ ein $b \in R$
    mit $y = be'$. Es gilt $ee' = 0$, und deshalb
    $$\vfi(x + y) = ((x+y)e, (x+y)e') = (ae^2 + bee', aee' + b(e')^2) = (ae,
    be') = (x, y).$$

    Damit ist $\vfi \colon R \xrightarrow{\;\cong\;} Re \times Re'$.

## Aufgabe 2

Sei $n \geqslant 2$ und $R = \quotient{\Z}{n\Z}$.

Es gilt für $p \in \Z$:
$$\overline{p} \text{ prim in } R \qquad \iff \qquad d :=
\ggT(p, n) \text{ prim in } \Z.$$

\paragraph{Beweis.} Zunächst bemerke man, dass $\overline{p} \mid \overline{d}$,
denn es gibt $r, s \in \Z$ mit $d = rp + sn$, und $\overline{d} \mid
\overline{p}$ wegen $d \mid p$.

\paragraph{$\Rightarrow$} Per Kontraposition: Es sei $d = \ggT(p, n) \in \Z$
keine Primzahl, und wir zeigen, dass $\overline{p}$ kein Primelement in $R$ ist.
Es treten drei Fälle auf:

* $d = 0$. Dann ist aber $p = 0$ und $\overline{p} = \overline{0}$ ist kein
  Primelement.
* $d = 1$. Dann ist aber $\overline{p}$ eine Einheit in $\quotient{\Z}{n\Z}$ und
  kein Primelement.
* $d > 1$ und $d$ ist zusammengesetzt, also $d = d_1d_2$ mit $1 < d_1, d_2 < d$.

  Zeige zunächst, dass $\overline{d}$ ein Primelement in $R$ ist. Es gilt
  $\overline{d} = \overline{d_1}\overline{d_2} \mid
  \overline{d_1}\overline{d_2}$, aber weder $\overline{d} \mid \overline{d_1}$
  noch $\overline{d} \mid \overline{d_2}$:

  Angenommen, es gälte $\overline{d} \mid \overline{d_1}$, also
  $\overline{d}\overline{x} = \overline{d_1}$ für ein $\overline{x} \in R$. Dann
  folgte $dx = d_1 + kn$ für ein $k \in \Z$, und wegen $d \mid n$ auch $d \mid
  d_1$ in $\Z$, im Widerspruch zu $1 < d_1 < d$. Analog zeigt man $\overline{d}
  \nmid \overline{d_1}$.

  Mit $\overline{d}$ ist nun auch $\overline{p}$ kein Primelement: Es gilt zwar
  $\overline{p} \mid \overline{d} = \overline{d_1} \overline{d_2}$, aber wäre
  $\overline{p} \mid \overline{d_1}$, so wäre wegen $\overline{d} \mid
  \overline{p}$ auch $\overline{d} \mid \overline{d_1}$. Somit folgt
  $\overline{p} \nmid \overline{d_1}$, und analog $\overline{p} \nmid
  \overline{d_2}$.

\paragraph{$\Leftarrow$} Sei $d = \ggT(p, n) \in \Z$ eine Primzahl. Dann gibt es
$u, v \in \Z$ mit $p = ud, n = vd$.

Nun seien $\overline{a}, \overline{b} \in R \nozero$ mit $\overline{p}
\mid \overline{a}\overline{b}$. Es gibt also $q, x \in \Z$ mit $pq = ab + xn$.
Wir erhalten
$$d(uq) = pq = ab + xn = ab + xvd.$$
Es folgt $d \mid ab$, also $d \mid a$ oder $d \mid b$, also $\overline{d} \mid
\overline{a}$ oder $\overline{d} \mid \overline{b}$.
Wegen $\overline{p} \mid \overline{d}$ folgt $\overline{p} \mid \overline{a}$
oder $\overline{p} \mid \overline{b}$.

\paragraph{Eine kleine Anmerkung.} Aufgabenstellungen mit offenem Charakter, das
heißt Aufgaben, die das zu zeigende Ergebnis noch nicht verraten, sind im
Allgemeinen nicht verwerflich. Bei Aufgaben wie der vorliegenden wird jedoch
eine Charakterisierung verlangt, die den Bearbeitenden im Zweifel lässt, was
genau hier zu tun sei.

Der Aufgabensteller hat bei solchen Aufgaben meist \textit{eine}
Charakterisierung im Sinn, die ihm als die einzig sinnvolle und klare und
einfache Charakterisierung erscheint.

Ein weiteres Beispiel wäre etwa die Aufgabe, alle Untergruppen von $(\Z, +)$ zu
bestimmen. Hier mag es für manchen noch einleuchtend erscheinen, "die
Mengen der Form $k\Z$ mit $k \in \Z$" als einzige Antwort gelten zu
lassen. Ebenso richtig wäre aber "die Mengen $U \subseteq \Z$, die unter
$+$ und $-$ abgeschlossen sind".

Natürlich obliegt es auch dem Bearbeitenden, unklare Aufgabenstellungen mit
Wohlwollen richtig zu interpretieren. Bei der vorliegenden Aufgabe ist --
zumindest mir -- unklar, welches diese richtige Interpretation ist.

Nachdem ich aber nun zu viel Zeit in diesen belanglosen Aufsatz gesteckt habe,
darf ich mit dem Lob abschließen, dass die Aufgaben in _Algebra und
Zahlentheorie_ im Allgemeinen lehrreich und gut mit der Vorlesung abgestimmt
sind und dass ihre Bearbeitung durchaus Spaß macht.

## Aufgabe 3

\paragraph{Notation:} Ist $R$ ein kommutativer Ring, so schreibe $\Idem(R) :=
\set{a \in R \mid a^2 = a}$.

\paragraph{Lemma:} Ist $p \in \N$ eine Primzahl und $k \in \N_+$, so gilt
$\Idem\left(\quotient{\Z}{p^k\Z}\right) = \set{\overline{0}, \overline{1}}$.
\vspace{-0.5cm}
\paragraph{Beweis.} Sei $a \in \Z$ mit $a^2 \equiv a \pmod{p^k}$, also $p^k \mid
a^2 - a = a(a - 1)$. Nur einer der beiden Faktoren kann durch $p$ teilbar sein.
Dieser Faktor ist dann durch $p^k$ teilbar, also $p^k \mid a$ oder $p^k \mid a -
1$. Wir erhalten
$$a \equiv 0 \pmod{p^k} \qquad \text{oder} \qquad a \equiv 1 \pmod{p^k}.$$

Zur Aufgabe:

(a) Die Anzahl der Einheiten ist $\vfi(420)$. Mit Korollar 6.10 (b) folgt
    $$\vfi(420) = \vfi(4 \cdot 3 \cdot 5 \cdot 7) = \vfi(4) \cdot \vfi(3) \cdot
    \vfi(5) \cdot \vfi(7) = 2 \cdot 2 \cdot 4 \cdot 6 = 96.$$

(b) Laut Satz 5.4 sind die Nullteiler genau die Nichteinheiten von
    $\quotient{\Z}{420\Z}$. Es gibt deren $420 - 96 = 324$.

(c) Machen wir's doch gleich allgemeiner: Sei $R = \quotient{\Z}{n\Z}$ für ein
    $n \in \N_+$ und $n = \prod_{i=1}^s p_i^{\alpha_i}$ die Primfaktorzerlegung
    von $n$ mit $s \geqslant 0$ und paarweise verschiedenen Primzahlen $p_1,
    \dots, p_s$.

    Wir wollen $\Nil(R) = \ideal{p_1 \cdot \dots \cdot p_s} =: I$ zeigen.

    Sei $\overline{a} \in \Nil(R)$. Dann gibt es ein $k \in \N$ mit $a^k \equiv
    0 \pmod{n}$, also $n \mid a^k$. Somit teilt jeder Primteiler von $n$ auch
    $a^k$, und damit $a$. Dies zeigt $\overline{a} \in I$.

    Nun sei $\overline{b} \in I$ mit $b = p_1 \cdot \dots \cdot p_s \cdot c$ und
    ein $c \in \Z$. Setze $\alpha = \max\set{\alpha_1, \dots, \alpha_s}$.
    Dann gilt
    $$n = p_1^{\alpha_1} \cdot \dots \cdot p_s^{\alpha_s} \mid p_1^\alpha \cdot
    \dots \cdot p_s^\alpha \mid b^\alpha.$$
    Dies zeigt $b^\alpha \equiv n \equiv 0 \pmod{n}$, also $\overline{b} \in
    \Nil(R)$.

    Für $420 = 2^2 \cdot 3 \cdot 5 \cdot 7$ ist dann
    $$\Nil(R) = \ideal{2\cdot 3\cdot 5\cdot 7} = \ideal{210} =
    \set{\overline{0}, \overline{210}}.$$

(d) Machen wir's doch gleich allgemeiner: Sei $R = \quotient{\Z}{n\Z}$ für ein
    $n \in \N_+$ und $n = \prod_{i=1}^s p_i^{\alpha_i}$ die Primfaktorzerlegung
    von $n$ mit $s \geqslant 0$ und paarweise verschiedenen Primzahlen $p_1,
    \dots, p_s$.

    Für $i \in \set{1, \dots, s}$ setze $R_i = \quotient{\Z}{p_i^{\alpha_i}\Z}$.
    Mit dem chinesischen Restsatz kann man $R$ zerlegen:
    $$R = \quotient{\Z}{n\Z} \cong \bigotimes_{i=1}^s R_i =: \tilde{R}.$$

    Zeigt man nun noch $\Idem(\tilde{R}) = \bigotimes_{i=1}^s \Idem(R_i)$, so
    folgt mit dem Lemma, dass
    $$\abs{\Idem(R)} = \abs{\Idem(\tilde{R})}
    = \abs{\bigotimes_{i=1}^s \Idem(R_i)} = \prod_{i=1}^s \abs{\Idem(R_i)} =
    \prod_{i=1}^s \abs{\set{0+p_i^{\alpha_i}\Z, 1 + p_i^{\alpha_i}\Z}} = 2^s.$$

    Sind $e_1 \in \Idem(R_1), \dots, e_s \in \Idem(R_s)$, so ist $a = (e_1,
    \dots, e_s) \in \Idem(\tilde{R})$: $a^2 = (e_1^2, \dots, e_s^2) = (e_1, \dots, e_s) = a$.

    Ist dagegen $e = (e_1, \dots, e_s) \in \Idem(\tilde{R})$, so gilt
    $(e_1^2, \dots, e_s^2) = e^2 = e = (e_1, \dots, e_s)$,
    und damit $e_i \in \Idem(R_i)$ für alle $i \in \set{1, \dots, s}$.

    Zusammenfassend gibt es also $2^s$ idempotente Elemente, wobei $s$ die
    Anzahl der verschiedenen Primteiler von $n$ ist.

    Für $420 = 2^2 \cdot 3 \cdot 5 \cdot 7$ ist $s = 4$, es gibt also $2^4 =
    16$ idempotente Elemente.

(e) Laut Blatt 2, Aufgabe 3, ist hier nach der Anzahl der Teiler von $420 = 2^2
    \cdot 3 \cdot 5 \cdot 7$ gefragt. Diese ist durch $3 \cdot 2 \cdot 2 \cdot
    2 = 24$ (drei mögliche Exponenten für den Primteiler 2, zwei mögliche
    Exponenten für die anderen Primteiler) gegeben.

Wegen $\overline{191} \cdot \overline{11} = \overline{1}$ ist $a =
\overline{191}$ eine Einheit in $\quotient{\Z}{420\Z}$ und $a^{-1} =
\overline{11}$.

## Aufgabe 4

(a) Sei $e$ der Exponent von $(\quotient{\Z}{n\Z})^\times$. Mit Satz 5.15 folgt
    $e \leqslant \vfi(n)$.

    Angenommen, es gälte $e \nmid \vfi(n)$, also $\vfi(n) = qe+r$ mit $0<r<e$.
    Dann hätten wir für alle $a \in (\quotient{\Z}{n\Z})^\times$:
    $$\overline{1} = a^{\vfi(n)} = a^{qe+r} = (a^e)^q \cdot a^r = \overline{1}^q
    \cdot a^r = a^r,$$
    im Widerspruch zur Minimalität von $e$.

(b) Für $n = 8$ ist $\vfi(n) = 4$ und $(\quotient{\Z}{8\Z})^\times =
    \set{\overline{1}, \overline{3}, \overline{5}, \overline{7}}$. Für alle $a
    \in (\quotient{\Z}{8\Z})^\times$ ist $a^2 = \overline{1}$.  Somit ist $2 < 4
    = \vfi(n)$ der Exponent von $(\quotient{\Z}{8\Z})^\times$.

\newpage

## Aufgabe 5

Da nicht anders angegeben wurde angenommen, dass die gegebene Aussage zu
beweisen ist.

\paragraph{$\Rightarrow$} Es gebe $x, y \in \Z$ mit $ax+by \equiv c \pmod{n}$.
Dann existiert ein $k \in \Z$ mit $ax+by-c = kn$, also $c = ax + by - kn$. Da
$\Z$ ein Hauptidealbereich ist, folgt
$$c = ax + by - kn \in a\Z + b\Z + n\Z = \ggT(a, b, n)\Z.$$

\paragraph{$\Leftarrow$} Es gelte $c = k \cdot \ggT(a, b, n)$ für ein $k \in
\Z$. Dann gibt es mit dem Lemma von Bézout $m_1, m_2, m_3 \in \Z$ mit
$\ggT(a, b, n) = m_1a + m_2b + m_3n$ und es folgt $c = k \cdot \ggT(a, b, n) =
a(m_1k) + b(m_2k) + n(m_3k)$, also
$$a(m_1k) + b(m_2k) \equiv a(m_1k) + b(m_2k) + n(m_3k) = c \pmod{n}.$$

## Aufgabe 6

(a) Sei $R$ euklidisch mit euklidischer Funktion $\vfi$.

    Für $a, b \in R$ setze $r_0 := a, r_1 := b$ und für $i = 1, 2, \dots$
    schreibe $r_{i-1} = q_{i+1}r_i + r_{i+1}$ mit $q_{i+1}, r_{i+1} \in R$ und
    $\vfi(r_{i+1}) < \vfi(r_i)$, solange bis $r_{i+1} = 0$. Dieser Fall tritt
    irgendwann für ein $i =: n$ irgendwann ein, da die Werte $\vfi(r_{i+1})$
    nicht beliebig klein werden können.

    Nun gilt es zu zeigen, dass $r_n$ ein größter gemeinsamer Teiler von $a$ und
    $b$ ist.

    Es gilt $r_n \mid r_{n-1}$, und wegen $r_{n-2} = q_nr_{n-1} + r_n$ auch $r_n
    \mid r_{n-2}$. So fährt man fort bis $r_n \mid r_1 = b$ und $r_n \mid r_0 =
    a$.

    Sei $d \in R$ ein weiterer gemeinsamer Teiler von $a$ und $b$.
    Wegen $d \mid a = q_2b + r_2$ und $d \mid b$ folgt dann $d \mid r_2$. Aus $d
    \mid r_1 = b = q_3r_2 + r_3$ und $d \mid r_2$ folgt $d \mid r_2$. So fährt
    man fort bis $d \mid r_n$.

(b) Zunächst ein paar kleine Lemmata:\vspace{-0.3cm}

    \paragraph{Lemma 1:} Ein euklidischer Integritätsring $R$ mit euklidischer
    Funktion $\vfi$ ist ein Hauptidealbereich. \vspace{-0.5cm}
    \paragraph{Beweis.} Ist $I \ne 0 \cdot R = \set{0}$ ein Ideal, so kann man
    ein $a \in I$ mit minimalem $\vfi(a)$ wählen. Für jedes $b \in I$ kann man
    $b = qa + r$ mit $r = 0$ oder $\vfi(r) < \vfi(a)$ schreiben. Es gilt $r \in
    I$, und wegen der Minimalität von $\vfi(a)$ muss $r = 0$ sein. Dies zeigt $b
    \in aR$, also $I = \ideal{a}$. \qed

    \paragraph{Lemma 2:} In einem kommutativen Hauptidealbereich $R$ gilt der
    Teilerkettensatz für Elemente. \vspace{-0.5cm}
    \paragraph{Beweis.} Sei $\set{r_n}_{n\in\N}$ eine Teilerkette. Betrachte die
    Kette
    $$\ideal{r_0} \subseteq \ideal{r_1} \subseteq \ideal{r_2} \cdots.$$
    Dann ist $I = \bigcup_{n\in\N} \ideal{r_n}$ ein Ideal, denn für $a, b \in I$
    und $r \in R$ gibt es ein $i \in \N$ mit $a, b \in \ideal{r_i}$ und daher
    $a+b, a-b, ra \in I$.

    Da $R$ ein Hauptidealbereich ist, gibt es ein $d \in I$ mit $\ideal{d} = I$.
    Wieder gibt es ein $i \in \N$ mit $d \in \ideal{r_i}$. Für alle $n \geqslant
    i$ gilt dann
    $$\ideal{d} \subseteq \ideal{r_i} \subseteq \ideal{r_n} \subseteq
    \ideal{r_{n+1}} \subseteq I = \ideal{d}$$
    und somit sind alle Inklusionen Gleichheiten. Aus $\ideal{r_n} =
    \ideal{r_{n+1}}$ folgt $r_{n+1} \mid r_n$ und $r_n \mid r_{n+1}$. \qed

    \newpage

    \paragraph{Lemma 3:} In einem Hauptidealbereich $R$ sind die Primelemente
    von $R$ genau die irreduziblen Elemente.\vspace{-0.5cm}
    \paragraph{Beweis.} Laut Satz 4.12 (a) sind Primelemente von
    Integritätsringen irreduzibel.

    Sei $r \in R$ irreduzibel und seien $a, b \in R \nozero$ mit $r \mid ab$.
    Da $R$ ein Hauptidealbereich ist, gibt es ein $d \in R$ mit $\ideal{r, a} =
    \ideal{d}$. Wegen $r \ne 0$ ist $\ideal{0} \ne \ideal{r, a} = \ideal{d}$,
    also $d \ne 0$.

    Es gilt $d \mid a$ und $d \mid r$, also $r = ds$ für ein $s \in R$. Da $r$
    irreduzibel ist, gilt $d \in R^\times$ oder $s \in R^\times$.

    * Gilt $d \in R^\times$, so ist $\ideal{r, a} = \ideal{d} = R = \ideal{1}$.
      Es gibt $x, y \in R$ mit $rx + ay = 1$, also $b = b(rx + ay) = rbx +
      aby$. Aus $r \mid rbx$ und $r \mid ab$ (so waren $a, b$ gewählt) folgt $r
      \mid b$.
    * Gilt $s \in R^\times$, so folgt $r \mid d$, also $r \mid a$.

    Somit ist $r$ ein Primelement von $R$. \qed

    \paragraph{Lemma 4:} Sei $R$ ein Integritätsbereich, sei $r \in R$ und seien
    $a, b \in R \setminus (R^\times \cup \set{0})$. Dann gilt $r \nmid a$.
    \vspace{-0.5cm}
    \paragraph{Beweis.} Angenommen, $r \mid a$, also $a = kr$ für ein $k \in R$.
    Dann hätten wir $a = kr = kab$, also $a(1 - kb) = 0$. Dann folgte wegen der
    Nullteilerfreiheit von $R$ und $a \ne 0$, dass $1 - kb = 0$, also $b \in
    R^\times$. \qed

    ---

    Sei nun $R$ ein euklidischer Integritätsring mit euklidischer Funktion
    $\vfi$. Die Voraussetzung, dass in $R$ der Teilerkettensatz gelte, ist laut
    Lemma 2 überflüssig.

    \paragraph{Existenz der Primfaktorzerlegung:} Laut Lemma 1 ist $R$ ein
    Hauptidealbereich, daher sind laut Lemma 3 die Primelemente von $R$ genau
    die irreduziblen Elemente.

    Sei $r \in R \nozero$. Ist $r$ eine Einheit oder irreduzibel, so ist $r$
    bereits in Primfaktorzerlegung gegeben.

    Sei $r \in R \nozero$ nun keine Einheit und nicht irreduzibel. Angenommen,
    es gäbe keine Primfaktorzerlegung für $r$. Dann gäbe es Faktoren $a, b \in R
    \setminus (R^\times \cup \set{0})$ mit $r_0 := r = ab$. Nicht beide Faktoren
    könnten dann eine Primfaktorzerlegung besitzen, sonst hätte auch $r$ eine.

    Sei $r_1$ dieser Faktor. Dann gilt $r_1 \mid r_0$, aber $r_0 \nmid r_1$ laut
    Lemma 4. So fortfahrend kann man eine unendliche echt absteigende
    Teilerkette konstruieren, im Widerspruch zum Teilerkettensatz, der laut
    Voraussetzung oder Lemma 2 für $R$ gilt.

    \newpage

    \paragraph{Eindeutigkeit der Primfaktorzerlegung:} Behauptung: Gilt
    $p_1 \cdots p_k = q_1 \cdots q_l$
    mit $k, l \in \N$ und Primelementen $p_1, \dots, p_k, q_1, \dots, q_l \in
    R \setminus (R^\times \cup \set{0})$, so gilt $k \leqslant l$ und es gibt
    eine Injektion $\sigma \colon \set{1, \dots, k} \to \set{1, \dots, l}$ mit
    $p_i \mid q_{\sigma(i)}$ für alle $i \in \set{1, \dots, k}$.

    Zeigt man diese Aussage per Induktion nach $k$, so folgt aus
    Symmetriegründen die Eindeutigkeit der Primfaktorzerlegung bis auf
    Multiplikation mit Einheiten.

    \paragraph{Induktionsanfang:} $k = 0$. Es gilt $1 = p_1 \cdots p_k = q_1
    \cdots q_l$, und da die $q_i$ Nichteinheiten sind, folgt auch $l = 0
    \geqslant k$ und $\sigma: \varnothing \to \varnothing$ besitzt
    trivialerweise die gewünschte Eigenschaft.

    \paragraph{Induktionsschritt:} $k > 0$. Wegen $p_k \mid q_1 \cdots q_l$ und
    der Primalität von $p_k$ haben wir $p_k \mid q_i$ für ein $i \in \set{1,
    \dots, l}$. Ohne Einschränkung sei dies $q_l$. Wegen der Irreduzibilität von
    $q_l$ folgt aus $p_k \mid q_i$, dass $q_l = up_k$ für eine Einheit $u \in
    R^\times$.

    Dies zeigt $p_1 \cdots (u^{-1}p_{k-1}) = q_1 \cdots q_{l-1}$, und die
    Induktionsvoraussetzung liefert $k - 1 \leqslant l - 1$, also $k \leqslant
    l$, sowie eine Injektion $\sigma' \colon \set{1, \dots, k-1} \to \set{1,
    \dots, l-1}$ mit $p_i \mid q_{\sigma'(i)}$ für $i \in \set{1, \dots, l-1}$.
    Diese kann man zu einer Injektion
    $$\sigma: \set{1, \dots, k} \to \set{1, \dots, l}, \quad i \mapsto
    \begin{cases}\sigma'(i) & i < k, \\l & i = k\end{cases}$$
    erweitern und ist fertig.
