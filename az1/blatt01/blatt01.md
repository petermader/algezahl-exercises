# Algebra und Zahlentheorie -- Übungsblatt 1

\newcommand{\Nil}{\mathrm{Nil}}

## Aufgabe 1

Sei $b \in R$. Es ist $0 = ac - a$, also $0 = b(ac - a) = bac - ba = a(bc - b)$.
Mit $a \ne 0$ liefert die Nullteilerfreiheit von $R$, dass $bc - b = 0$, also $b
= bc = cb$. Somit ist $c$ ein Einselement.

## Aufgabe 2

(1) Die Relation $\sim$ auf $\N^2$ sei definiert durch
    $$(a_1, a_2) \sim (b_1, b_2) \quad \iff \quad a_1 + b_2 = a_2 + b_1, \qquad
    (a_1, a_2), (b_1, b_2) \in \N^2.$$

(2) Diese Relation ist eine Äquivalenzrelation:

    * **Reflexivität**: für $a = (a_1, a_2) \in \N^2$ gilt $a_1 + a_2 = a_2 +
      a_1$, also $a \sim a$.
    * **Symmetrie**: für $a = (a_1, a_2), b = (b_1, b_2) \in \N^2$ mit $a \sim
      b$ haben wir $a_1 + b_2 = a_2 + b_1$, also $b_1 + a_2 = b_2 + a_1$, und
      damit $b \sim a$.
    * **Transitivität**: für $a = (a_1, a_2), b = (b_1, b_2), c = (c_1, c_2) \in
      \N^2$ mit $a \sim b$ und $b \sim c$ haben wir $a_1 + b_2 = a_2 + b_1$ und
      $b_1 + c_2 = b_2 + c_1$, also
      $$(a_1 + c_2) + (b_1 + b_2) = (a_1 + b_2) + (b_1 + c_2) = (a_2 + b_1) +
      (b_2 + c_1) = (a_2 + c_1) + (b_1 + b_2).$$
      Abhängig von der Konstruktion von $\N$ lässt sich zeigen, dass $(\N, +)$
      ein rechtskürzbares Monoid ist, dass also aus dieser Gleichung schon $a_1
      + c_2 = a_2 + c_1$ und damit $a \sim c$ folgt.

(3) Setze $\Z = \quotient{\N^2}{\sim}$. Die Verknüpfung
    $$+ \colon \Z \times \Z \to \Z, \;
    ([(a_1, a_2)]_{\sim}, [(b_1, b_2)]_{\sim}) \mapsto [(a_1 + b_1, a_2 +
    b_2)]_{\sim}$$
    ist wohldefiniert: Seien $a = (a_1, a_2), b = (b_1, b_2), a' = (a_1', a_2'),
    b' = (b_1', b_2') \in \N^2$ mit $[a]_\sim = [a']_\sim, [b]_\sim =
    [b']_\sim$. Es gilt
    $$(a_1 + b_1) + (a_2' + b_2') = (a_1 + a_2') + (b_1 + b_2') = (a_1' + a_2) +
    (b_1' + b_2) = (a_2 + b_2) + (a_1' + b_1'),$$
    also $[a]_{\sim} + [b]_{\sim} = [(a_1 + b_1, a_2 + b_2)]_{\sim} = [(a_1' +
    b_1', a_2' + b_2')]_{\sim} = [a']_{\sim} + [b']_{\sim}$.

(4) Mit der Verknüpfung $+$ aus (3) wird $\Z$ zur einer abelschen Gruppe, da
    sich die Eigenschaften von $\N$ auf $\Z$ vererben:

    * **Assoziativität**: Sind $a = [(a_1, a_2)]_\sim, b = [(b_1, b_2)]_\sim, c
      = [(c_1, c_2)]_\sim \in \Z$, so ist
      $$\begin{aligned}
      (a + b) + c &= [(a_1 + b_1, a_2 + b_2)]_\sim + c = [((a_1 + b_1) + c_1,
      (a_2 + b_2) + c_2)]_\sim \\
      &= [(a_1 + (b_1 + c_1), a_2 + (b_2 + c_2))]_\sim = a + [(b_1 + c_1, b_2 +
      c_2)]_\sim = a + (b + c).
      \end{aligned}$$
    * **Kommutativität**: Sind $a = [(a_1, a_2)]_\sim, b = [(b_1, b_2)]_\sim \in
      \Z$, so ist
      $$a + b = [(a_1 + b_1, a_2 + b_2)]_\sim = [(b_1 + a_1, b_2 + a_2)]_\sim =
      b + a.$$
    * **Neutrales Element**: Setze $e = [(0, 0)]_\sim$. Dann gilt für $a =
      [(a_1, a_2)]_\sim \in \Z$:
      $$e + a = a + e = [(a_1 + 0, a_2 + 0)]_\sim = a.$$
    * **Inverse Elemente**: Ist $a = [(a_1, a_2)]_\sim \in \Z$, so wähle $-a =
      [(a_2, a_1)]_\sim$. Für alle $x \in \N$ gilt $(x, x) \sim (0, 0)$, also
      folgt
      $$(-a) + a = a + (-a) = [(a_1 + a_2, a_2 + a_1)]_\sim = e.$$

(5) Sei $a = [(a_1, a_2)]_\sim \in \Z$.

    Ist $a_1 \geqslant a_2$, so existiert ein $b \in \N$ mit $a_1 + 0 = a_1 = b
    + a_2$, also $(b, 0) \sim (a_1, a_2)$, und damit $a = [(b, 0)]_\sim$.

    Ist dagegen $a_1 < a_2$, so existiert ein $b \in \N$ mit $a_2 + 0 = a_2 = b
    + a_1$, also $(0, b) \sim (a_1, a_2)$, und damit $a = [(0, b)]_\sim$.

(6) Betrachte $\vfi \colon \N \to \Z, a \mapsto [(a, 0)]_\sim$. Für $a, b \in
    \N$ mit $a \ne b$ gilt $0 + a = a \ne b = 0 + b$. Damit ist $(a, 0) \not\sim
    (b, 0)$, also $\vfi(a) = [(a, 0)]_\sim \ne [(b, 0)]_\sim = \vfi(b)$, und
    $\vfi$ ist eine Injektion.

    Für beliebige $a, b \in \N$ gilt außerdem
    $$\vfi(a + b) = [(a + b, 0 + 0)]_\sim = [(a, 0)]_\sim + [(b, 0)]_\sim =
    \vfi(a) + \vfi(b)$$
    sowie $\vfi(0) = [(0, 0)]_\sim = e$. Somit ist das Monoid $\N$ samt seiner
    additiven Struktur via $\N \xhookrightarrow{\;\vfi\;} \Z$ in $\Z$
    enthalten.


## Aufgabe 3

Der Binomialkoeffizient erfüllt die Rekursionsgleichung
$$\binom{n-1}{k-1} + \binom{n-1}{k} = \binom{n}{k}$$
für $n \geqslant 2, k \in \set{1, \dots, n-1}$. Nun folgt die Behauptung per
Induktion nach $n$:

\paragraph{Induktionsanfang} Mit $n = 0$ haben wir
$$(a + b)^n = 1_R = 1 \cdot 1_R \cdot 1_R = \binom{0}{0} a^0 b^0 =
\sum_{k=0}^{n} \binom{n}{0} a^kb^{n-k}.$$

\paragraph{Induktionsschritt} Sei $n > 0$. Wir haben
$$\begin{aligned}
(a + b)^n &= (a + b)(a + b)^{n-1}
\stackrel{\text{IV}}{=} (a + b)\left(\sum_{k=0}^{n-1}
    \binom{n-1}{k}a^kb^{n-1-k}\right) \\
&= \left(\sum_{k=0}^{n-1} \binom{n-1}{k}a^{k+1}b^{n-1-k}\right)
    + \left(\sum_{k=0}^{n-1} \binom{n-1}{k} a^kb^{n-k}\right) \\
&= \left(\sum_{k=1}^{n} \binom{n-1}{k-1}a^kb^{n-k}\right)
    + \left(\sum_{k=0}^{n-1} \binom{n-1}{k} a^kb^{n-k}\right) \\
&= \binom{n-1}{n-1}a^nb^0 + \left(\sum_{k=1}^{n-1}
    + \binom{n-1}{k-1}a^kb^{n-k}\right)
    + \left(\sum_{k=1}^{n-1} \binom{n-1}{k} a^kb^{n-k}\right)
    + \binom{n-1}{0}a^0b^n \\
&= \binom{n}{n}a^nb^0 + \left(\sum_{k=1}^{n-1} \binom{n}{k}a^kb^{n-k}\right)
    + \binom{n}{0}a^0b^n \\
&= \sum_{k=0}^{n} \binom{n}{k}a^kb^{n-k}
\end{aligned}$$

## Aufgabe 4

(a) Seien $a, b \in \Nil(R)$. Dann gibt es $n, m \in \N_+$ mit $a^n = b^m = 0$.
    Für alle $k \in \set{0, \dots, n + m}$ ist $k \geqslant n$ oder $n + m - k
    \geqslant m$. Daher gilt mit Aufgabe 3:
    $$(a + b)^{n+m} = \sum_{k=0}^{n+m} \binom{n+m}{k}
    \underbrace{a^kb^{n+m-k}}_{=0} = 0,$$
    also $a + b \in \Nil(R)$. Außerdem ist $(-a)^n = (-1)^na^n = 0$, also $-a
    \in \Nil(R)$.

(b) Laut Voraussetzung existiert $x^{-1} \in R$ sowie ein $n \in \N_+$ mit $y^n
    = 0$. Es ist
    $$\begin{aligned}
    (x + y)\left(x^{-1} \sum_{k=0}^{n-1} (-yx^{-1})^k\right) &=
    (1 + yx^{-1}) \sum_{k=0}^{n-1} (-yx^{-1})^k \\
    &= \sum_{k=0}^{n-1} (-yx^{-1})^k - (-yx^{-1})^{k+1} \\
    &\stackrel{(*)}{=} 1 - (-yx^{-1})^{n} = 1 - \underbrace{y^{n}}_{=0}(-x)^{-n}
    = 1,
    \end{aligned}$$
    wobei bei $(*)$ eine Teleskopsumme Verwendung findet. Somit ist $x + y$ in
    $R$ invertierbar.

\newpage

## Aufgabe 5

(a) Sei $R = \left\{ \pmat{a&b\\0&a} \;\Bigg| \;a, b \in K \right\} \subseteq
    \Mat_2(K)$ und seien $A_1 = \pmat{a_1&b_1\\0&a_1}\!\!,\, A_2 =
    \pmat{a_2&b_2\\0&a_2} \in R$.

    Dann gilt $I_2 = \pmat{1&0\\0&1} \in R$ und
    $$\begin{aligned}
    A_1 + A_2 &= \pmat{a_1 + a_2 & b_1 + b_2 \\ 0 & a_1 + a_2} \in R, \\
    A_1 A_2 &= \pmat{a_1a_2 & a_1b_2 + b_1a_2 \\ 0 & a_1a_2 } \in R, \\
    - A_1 &= \pmat{-a_1&-b_1\\0&-a_1} \in R, \\
    A_1 A_2 &= \pmat{a_1a_2 & a_1b_2 + b_1a_2 \\ 0 & a_1a_2 } = A_2 A_1.
    \end{aligned}$$
    Somit ist $R$ ein (unitärer) kommutativer Unterring von $\Mat_2(K)$.

(b) Der Körper $K$ ist ein Integritätsbereich: sind $x, y \in K$ mit $xy = 0$
    und $x \ne 0$, so ist $y = x^{-1}xy = 0$. Sei nun $A_1 =
    \pmat{a_1&b_1\\0&a_1} \in R$.

    * Ist $a_1 = 0$, so kann man $A_2 = \pmat{0&1\\0&0} \in R$ wählen und erhält
      $A_1 A_2 = 0$. In diesem Fall ist $A_1$ also sicherlich ein Nullteiler.

    * Sei $a_1 \ne 0$ und $A_2 = \pmat{a_2&b_2\\0&a_2} \in R$ mit
      $$\pmat{0&0\\0&0} = A_1 A_2 = \pmat{a_1a_2 & a_1b_2 + b_1a_2 \\ 0 &
      a_1a_2 }.$$
      Aus $a_1a_2 = 0$ und $a_1 \ne 0$ folgt $a_2 = 0$. Aus $0 = a_1b_2 +
      b_1a_2 = a_1b_2$ und $a_1 \ne 0$ folgt $b_2 = 0$. Somit ist $A_2 = 0$ und
      $A_1$ kein Nullteiler von $R$.

    Die Nullteiler von $R$ sind also genau die Matrizen der Form
    $\pmat{0&b\\0&0} \in R$ mit $b \in K$.

(c) Für $A = \pmat{a&b\\0&a} \in R$ gilt
    $$\begin{aligned}
    A \in R^{\times} &\iff \text{es gibt ein $A' \in R$ mit $A A' = I_2$} \\
    &\iff A \in \GL_2(K) \iff \det(A) \ne 0 \iff a^2 \ne 0 \iff a \ne 0.
    \end{aligned}$$
