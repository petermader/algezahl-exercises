# Algebra und Zahlentheorie -- Übungsblatt 10

## Aufgabe 1

\paragraph{Lemma:} Sei $G$ eine Gruppe und $U \subseteq G$ eine Untergruppe.
Genau dann ist $U$ ein Normalteiler von $G$, wenn $U$ die disjunkte Vereinigung
von Konjugationsklassen von $G$ ist. \vspace{-.5cm}
\paragraph{Beweis.} Ist $U$ ein Normalteiler von $G$, so gilt
$$[a] \coloneqq \set{gag^{-1} \mid g \in G} \subseteq U$$
für alle $a \in U$. Daher ist
$$U = \bigcup_{a \in U} [a]$$
und diese Vereinigung ist disjunkt, weil jedes Element nur in einer
Konjugationsklasse liegen kann.

Ist umgekehrt $U$ die Vereinigung von Konjugationsklassen, so liegt jedes $a \in
U$ in einer Konjugationsklasse $[a]$. Es gilt $[a] \subseteq U$, also $gag^{-1}
\in [a] \subseteq U$. \qed

---

(a) Wir schreiben wieder $D_4 = \ideal{\rho, \sigma} = \set{\Id, \rho, \rho^2,
    \rho^3, \sigma, \rho\sigma, \rho^2\sigma, \rho^3\sigma} \subseteq S_4$ mit
    der Rotation $\rho = (1\;2\;3\;4)$ und der Spiegelung $\sigma = (1\;3)$.

    * Untergruppen erster Ordnung: Hier gibt es nur $\set{\Id} \cong
      C_1$. Die triviale Untergruppe ist immer ein Normalteiler.

    * Untergruppen zweiter Ordnung: Die Elemente $\rho^2, \sigma,
      \rho\sigma, \rho^2\sigma, \rho^3\sigma$ sind von der Ordnung zwei, es gilt
      $$\ideal{\rho^2} \cong \ideal{\sigma} \cong \ideal{\rho\sigma} \cong
      \ideal{\rho^2\sigma} \cong \ideal{\rho^3\sigma} \cong C_2.$$

      Das Element $\rho^2$ kommutiert mit allen Elementen von $D_4$. Daher gilt
      $$\pi\rho^2\pi^{-1} = \rho^2\pi\pi^{-1} = \rho^2 \in \ideal{\rho^2}$$
      für alle $\pi \in D_4$ und die Untergruppe $\ideal{\rho^2}$ ist ein
      Normalteiler von $D_4$. Wegen
      $$\begin{aligned}
      &\rho\sigma\rho^{-1} = \rho^2\sigma \notin \ideal{\sigma}
      &\rho(\rho^2\sigma)\rho^{-1} = \sigma \notin \ideal{\rho^2\sigma}, \\
      &\rho(\rho\sigma)\rho^{-1} = \rho^3\sigma \notin \ideal{\rho\sigma},
      &\rho(\rho^3\sigma)\rho^{-1} = \rho\sigma \notin \ideal{\rho^3\sigma}
      \end{aligned}$$
      sind $\ideal{\sigma}, \ideal{\rho\sigma}, \ideal{\rho^2\sigma}$ und
      $\ideal{\rho^3\sigma}$ keine Normalteiler.

    * Untergruppen vierter Ordnung: Die Gruppe $\ideal{\rho} =
      \ideal{\rho^3} \cong C_4$ ist zyklisch. Außerdem gibt es die Untergruppen
      $\ideal{\rho^2, \sigma} \cong V_4$ und $\ideal{\rho^2, \rho\sigma} \cong
      V_4$.

      Diese Untergruppen besitzen sämtlich den Index 2 in $D_4$ und sind daher
      Normalteiler in $D_4$.

    * Untergruppen achter Ordnung: Die Gruppe $D_4$ ist eine
      Untergruppe von $D_4$ und die einzige der Ordnung acht. Die ganze Gruppe
      ist immer ein Normalteiler.

    Somit sind $\ideal{\Id}, \ideal{\rho^2}, \ideal{\rho}, \ideal{\rho^2,
    \sigma}, \ideal{\rho^2, \rho\sigma}$ und $D_4$ die Normalteiler von $D_4$.

(b) Die Gruppe $S_4$ besitzt die folgenden Konjugationsklassen, die sich aus den
    Elementen mit derselben Zyklenstruktur zusammensetzen:

    * Die Klasse der Identität $\set{\Id}$.
    * Die Klasse der Transpositionen (6 Elemente).
    * Die Klasse der Doppeltranspositionen (3 Elemente).
    * Die Klasse der 3-Zyklen (8 Elemente).
    * Die Klasse der 4-Zyklen (6 Elemente).

    Um die Normalteiler von $S_4$ zu erhalten, müssen laut Lemma diese
    Konjugationsklassen nun so kombiniert werden, dass die Vereinigung eine
    Untergruppe bildet. Notwendig dafür ist, dass die Elementezahl
    insgesamt ein Teiler von $\abs{S_4} = 24$ ist.

    Wir betrachten die Teiler von 24:

    * Ordnung 1: Die Konjugationsklasse $\set{\Id}$ ist eine Untergruppe und
      laut Lemma Normalteiler.
    * Ordnung 2: Kann nicht aus den Konjugationsklassen kombiniert werden.
    * Ordnung 3: Kann nicht aus den Konjugationsklassen kombiniert werden.
    * Ordnung 4: Hier gibt es als einzig mögliche Kombination die Vereinigung
      von $\set{\Id}$ und den Doppeltranspositionen. Das ist die Kleinsche
      Vierergruppe, die damit ein Normalteiler ist.
    * Ordnung 6: Kann nicht aus den Konjugationsklassen kombiniert werden.
    * Ordnung 8: Kann nicht aus den Konjugationsklassen kombiniert werden.
    * Ordnung 12: Die Gruppe $A_4$ besitzt Index 2 in $G$ und ist damit
      Normalteiler. Sie ergibt sich aus den Konjugationsklassen der
      Identität, der 3-Zyklen und der Doppeltranspositionen.
    * Ordnung 24: Die Gruppe $S_4$ ist die Vereinigung aller ihrer
      Konjugationsklassen.

\newpage

## Aufgabe 2

(a) Für $x \in \F_p^\times$ gilt $x \in \Ker(\vfi) \iff x^2 = 1 \iff x \in
    \set{-1, 1}$, also $\abs{\Ker(\vfi)} = 2$. Wegen $\F_p^\times / \Ker(\vfi)
    \cong \Img(\vfi)$ ist
    $$\abs{\Img(\vfi)} = \abs{\F_p^\times / \Ker(\vfi)} =
    \frac{\abs{\F_p^\times}}{\abs{\Ker(\vfi)}} = \frac{p-1}{2}.$$

(b) Die Quadrate in $\F_p$ sind genau die Elemente von $\Img(\vfi)$ und $0$.
    Daher gibt es $\abs{\Img(\vfi)} + 1 = \frac{p+1}{2}$ Quadrate.

\newpage

## Aufgabe 3

\paragraph{Lemma:} Für $a, b \in \Z \nozero$ ist die Abbildung
$$\vfi: a\Z / ab\Z \to \Z / b\Z, \qquad ak + ab\Z \mapsto
k+b\Z$$
ein wohldefinierter Isomorphismus von Gruppen.\footnote{Leider im Allgemeinen
nicht von Ringen: Setze $a=2, b=3, k=l=1$ und erhalte
$$\vfi((ak+ab\Z)(al+ab\Z)) = \vfi(4+6\Z) = 2+3\Z \ne 1+3\Z = (1+3\Z)(1+3\Z) =
\vfi(ak+ab\Z)\vfi(al+ab\Z).$$} \vspace{-.5cm}
\paragraph{Beweis.} Für die Wohldefiniertheit seien $k, l \in \Z$ mit $ak+ab\Z =
al+ab\Z$, also $a(k-l) = ak - al \in ab\Z$. Das zeigt $k-l \in b\Z$ und damit
$$\vfi(ak+ab\Z) = k+b\Z = l+b\Z = \vfi(al+ab\Z).$$

Für die Injektivität seien $ak+ab\Z, al+ab\Z \in a\Z / ab\Z$ mit $\vfi(ak+ab\Z)
= \vfi(al+ab\Z)$. Es gilt dann $k+b\Z = \vfi(ak+ab\Z) = \vfi(al+ab\Z) = l+b\Z$
und damit $ak+ab\Z = a(k+b\Z) = a(l+b\Z) = al+ab\Z$.

Für die Surjektivität sei $k+b\Z \in \Z / b\Z$. Dann gilt $\vfi(ak+ab\Z) =
k+b\Z$.

Für die Homomorphismus-Eigenschaft seien $ak+ab\Z, al+ab\Z \in a\Z / ab\Z$. Es
gilt
$$\begin{aligned}
\vfi((ak+ab\Z) + (al+ab\Z)) &= \vfi(a(k+l)+ab\Z) = k+l+b\Z \\
&= (k+b\Z)+(l+b\Z) = \vfi(ak+ab\Z) + \vfi(al+ab\Z).
\end{aligned}$$
Insgesamt erhalten wir $a\Z / ab\Z \cong \Z / b\Z$. \qed

\paragraph{Kleiner Scherz:} Es gibt unendlich viele zusammengesetzte Zahlen.
\vspace{-0.5cm}
\paragraph{Beweis.} Nimm an, es gäbe nur endlich viele, also $4, 6, \dots, a_k$,
und betrachte ihr Produkt. \qed

---

Es gilt $nm = vd$, also $\frac{n}{d} = \frac{v}{m}$, und mit dem Lemma
$$m\Z / v\Z = m\Z / \left(m \cdot \frac{v}{m}\right)\Z \cong \Z /
\left(\frac{v}{m}\right)\Z = \Z / \left(\frac{n}{d}\right)\Z \cong d\Z / \left(d
\cdot \frac{n}{d}\Z\right) = d\Z / n\Z.$$

\newpage

## Aufgabe 4

(a) Für $a, b, g \in G$ gilt
    $$\begin{aligned}
    g[a, b]g^{-1} &= gaba^{-1}b^{-1}g^{-1} =
    gag^{-1}gbg^{-1}ga^{-1}g^{-1}gb^{-1}g^{-1} \\
    &= (gag^{-1})(gbg^{-1})(gag^{-1})^{-1}(gbg^{-1})^{-1}
    = [gag^{-1}, gbg^{-1}].
    \end{aligned}$$
    Ist $h \in [G, G]$, so schreibe $h = \prod_{i=1}^{n} [a_i, b_i]$ mit $a_1,
    \dots, a_n, b_1, \dots, b_n \in G$. Nun gilt für $g \in G$:
    $$ghg^{-1} = g\left( \prod_{i=1}^{n} [a_i, b_i] \right) g^{-1} =
    \prod_{i=1}^{n} g[a_i, b_i]g^{-1} = \prod_{i=1}^{n} [ga_ig^{-1}, gb_ig^{-1}]
    \in [G, G],$$
    also $g[G, G]g^{-1} \subseteq [G, G]$. Daher ist $[G, G]$ ein Normalteiler
    von $G$. Sind $a, b \in G$, so gilt
    $(ba)^{-1}(ab) = a^{-1}b^{-1}ab = [a^{-1}, b^{-1}] \in [G, G]$.
    Dies zeigt $ba[G, G] = ab[G, G]$ und wir erhalten die Kommutativität von $G
    / [G, G]$.

(b) Wir zeigen $[G, G] \subseteq \Ker(\vfi)$. Die universelle Eigenschaft (Satz
    18.16) liefert dann den Rest.\footnote{Kleiner Scherz,
    Folge 2: Wer liest Papers in der Kategorientheorie? Ein Koautor!
    \scalebox{.3}{Weil der Koautor der einzige ist, den's interessiert.}}

    Für $a, b \in G$ gilt $\vfi([a, b]) = \vfi(aba^{-1}b^{-1}) =
    \vfi(a)\vfi(b)\vfi(a)^{-1}\vfi(b)^{-1} =
    \vfi(a)\vfi(a)^{-1}\vfi(b)\vfi(b)^{-1} = 1$.
    Ist $h \in [G, G]$, so schreibe $h = \prod_{i=1}^{n} [a_i, b_i]$ mit $a_1,
    \dots, a_n, b_1, \dots, b_n \in G$. Nun gilt
    $$\vfi(h) = \prod_{i=1}^{n} \vfi([a_i, b_i]) = \prod_{i=1}^{n} 1 = 1,$$
    also $h \in \Ker(\vfi)$.

\newpage

## Aufgabe 5

(a) Betrachte die Abbildung
    $$\vfi: U \times V \to UV, \qquad (u, v) \mapsto uv.$$
    Ist $a \in UV$, so gibt es ein $u \in U$ und ein $v \in V$ mit $a = uv$. Wir
    wollen zunächst
    $$\vfi^{-1}(a) = \set{(uw, w^{-1}v) \mid w \in U \cap V} \eqqcolon
    F_a$$
    zeigen.

    \begin{enumerate}
        \item[$\subseteq$] Sei $(\tilde{u}, \tilde{v}) \in \vfi^{-1}(a)$. Dann
        gilt $\tilde{u}\tilde{v} = \vfi(\tilde{u}, \tilde{v}) = a = uv$.
        Wähle $w = u^{-1}\tilde{u} = v\tilde{v}^{-1} \in U \cap V$ und erhalte
        $(\tilde{u}, \tilde{v}) = (uw, w^{-1}v) \in F_a$.
        \item[$\supseteq$] Sei $w \in U \cap V$. Dann gilt
        $\vfi(uw, w^{-1}v) = uww^{-1}v = uv = a$, also $(uw, w^{-1}v) =
        \vfi^{-1}(a)$.
    \end{enumerate}

    Für $w, w' \in U \cap V$ mit $w \ne w'$ gilt auch $(uw, w^{-1}v) \ne (uw',
    w'^{-1}v)$, also $\abs{F_a} = \abs{U \cap V}$.

    Für $a, b \in UV$ mit $a \ne b$ sind $F_a = \vfi^{-1}(a)$ und $F_b =
    \vfi^{-1}(b)$ disjunkt. Daher gilt
    $$\abs{U} \cdot \abs{V} = \abs{U \times V} = \abs{\bigdotcup_{a \in UV}
    \vfi^{-1}(a)} = \abs{\bigdotcup_{a \in UV} F_a} = \sum_{a \in UV} \abs{F_a}
    = \abs{UV} \cdot \abs{U \cap V},$$
    wie zu zeigen war.

(b) Wegen $e = ee \in UV$ ist $UV \ne \varnothing$. Seien $a = u_1v_1, b =
    u_2v_2 \in UV$ mid $u_1, u_2 \in U$ und $v_1, v_2 \in V$. Es gilt
    $$ab^{-1} = (u_1v_1)(u_2v_2)^{-1} = u_1v_1v_2^{-1}u_2^{-1} =
    \underbrace{u_1u_2^{-1}}_{\in U}\underbrace{u_2(v_1v_2^{-1})u_2^{-1}}_{\in
    V} \in UV$$
    und das Untergruppenkriterium liefert den Rest.

(c) Mit Teilaufgabe (a) gilt
    $$\ind{UV}{V} = \frac{\abs{UV}}{\abs{V}} = \frac{\abs{U} \cdot
    \abs{V}}{\abs{V} \cdot \abs{U \cap V}} = \frac{\abs{U}}{\abs{U \cap V}},$$
    also $\abs{U} = \ind{UV}{V} \cdot \abs{U \cap V}$.

    Da $UV$ laut (b) eine Untergruppe von $G$ ist, gibt es ein $k \in \N$ mit $k
    \cdot \abs{UV} = \abs{G}$. Also ist
    $$\ind{G}{V} = \frac{\abs{G}}{\abs{V}} = k \cdot
    \frac{\abs{UV}}{\abs{V}} = k \cdot \ind{UV}{V}.$$

(d) Laut (c) ist $\ind{UV}{V}$ gemeinsamer Teiler von $\abs{U}$ und
    $\ind{G}{V}$. Daher gilt
    $$0 < \ind{UV}{V} \mid \ggT(\abs{U}, \ind{G}{V}) = 1$$
    und daher $1 = \ind{UV}{V} =
    \frac{\abs{UV}}{\abs{V}}$, also $\abs{UV} = \abs{V}$.

    Es gilt $V \subseteq UV$, also $\abs{V} \leqslant \abs{UV}$. Gäbe es ein $a
    \in U \setminus V$, so wäre $a = ae \in UV \setminus V$ und wir hätten
    $\abs{UV} > \abs{V}$. Es gibt also kein solches $a$ und wir schließen $U
    \subseteq V$.

    \newpage

(e) Wir zeigen zunächst $V = \set{a^{\ind{G}{V}} \mid a \in G}$.

    \begin{enumerate}
        \item[$\subseteq$] Sei $b \in V$. Schreibe $1 = s\abs{V} + t\ind{G}{V}$
        mit $s, t \in \Z$. Dann gilt $b^{\abs{V}} = e$, also
        $$b = b^{s\abs{V} + t\ind{G}{V}} = \left(b^{\abs{V}}\right)^s \cdot
        \left(b^t\right)^{\ind{G}{V}} \in \set{a^{\ind{G}{V}} \mid a \in G}.$$
        \item[$\supseteq$] Sei $a \in G$. Dann gilt
        $$a^{\ind{G}{V}}V = (aV)^{\abs{G/V}} = eV,$$
        also $a^{\ind{G}{V}} \in V$.
    \end{enumerate}

    Nun sei $v \in V$ und $h \in H$. Es gibt also ein $a \in G$ mit $v =
    a^{\ind{G}{V}}$. Wegen $G \trianglelefteq H$ gilt $b = hah^{-1} \in
    G$. Es folgt
    $$hvh^{-1} = ha^{\ind{G}{V}}h^{-1} = (hah^{-1})^{\ind{G}{V}} =
    b^{\ind{G}{V}} \in V.$$

\newpage

## Aufgabe 6

Die Abbildung $\vfi \colon G \to G, x \mapsto x^3$ sei ein Automorphismus.

Es gilt $ab^3a^{-1} = (aba^{-1})^3 = \vfi(aba^{-1}) = a^3b^3a^{-3}$, also
$b^3a^2 = a^2b^3$, für alle $a, b \in G$.

Wir wollen nun zeigen, dass die Abbildung $\psi \colon G \to G, x \mapsto x^2$
ein Endomorphismus ist. Es seien wieder $a, b \in G$. Da $\vfi$ surjektiv
ist,\footnote{Kleiner Scherz, Folge 3: Gilt $\mathrm{CoCoA} \cong \mathrm{A}$?}
gibt es ein $c \in G$ mit $b = \vfi(c) = c^3$. Nun gilt mit der obigen
Eigenschaft
$$a^2b = a^2c^3 = c^3a^2 = ba^2,$$
also
$$(ab)(ab)^2 = (ab)^3 =  \vfi(ab) = a^3b^3 = a(a^2b)b^2 = a(ba^2)b^2 =
(ab)a^2b^2.$$
Kürzen liefert $\psi(ab) = (ab)^2 = a^2b^2 = \psi(a)\psi(b)$. Die Abbildung
$\psi$ ist damit tatsächlich ein Endomorphismus.

Ein letztes Mal seien $a, b \in G$. Es gilt
$$a(ba)b = \psi(ab) = \psi(a)\psi(b) = a(ab)b.$$
Durch beidseitiges Kürzen erhalten wird $ba = ab$.
