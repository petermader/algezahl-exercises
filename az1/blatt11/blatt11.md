# Algebra und Zahlentheorie -- Übungsblatt 11

## Aufgabe 1

(a) Für $m \in M$ ist $\abs{G \circ m} = \ind{G}{\Sta_G(m)}$ ein
    Teiler von $\abs{G} = 55$. Außerdem ist $M$ die disjunkte Vereinigung aller
    Bahnen.\footnote{Worunter man sich wohl
    \href{https://en.wikipedia.org/wiki/International_Union_of_Railways}{\color{red}so
    etwas} vorzustellen hat} Also ist $\abs{M} = 39$ die Summe der
    Kardinalitäten der Bahnen.

    Ein Elemente $m \in M$ ist ein Fixpunkt der Operation genau dann, wenn
    $\abs{G \circ m} = \frac{\abs{G}}{\abs{\Sta_G(m)}} = 1$.

    Hätte die Operation keinen Fixpunkt, so wäre $\abs{M} = 39$ als Summe der
    echten Teiler von $\abs{G} = 55$ darstellbar, d.h. $39 = 5s + 11t$ mit $s, t
    \in \N$. Letzteres ist ein Ding der Unmöglichkeit.

(b) Es gibt keine Darstellungen $18 = 5s + 11t$ oder $18 = 5s + 11t + 1$ mit $s,
    t \in \N$. Analoge Überlegungen wie in (a) führen dazu, dass es mindestens
    zwei Fixpunkte geben muss.

\newpage

## Aufgabe 2

\paragraph{Lemma 1:} Sei $G$ eine Gruppe und $a \in G$. Gilt $ga \ne ag$ für ein
$g \in G$, so gibt es einen nicht-trivialen Gruppenhomomorphismus $\vfi:
\ideal{g} \to \Aut(\ideal{a})$. \vspace{-.5cm}
\paragraph{Beweis.} Für $k \in \Z$ verwende $\vfi(g^k) = \vfi_k$ mit
dem inneren Automorphismus $\vfi_k : \ideal{a} \to \ideal{a}, a \mapsto
g^kag^{-k}$. Für $k, l, m \in \Z$ ist
$$\vfi_{k}(\vfi_l(a^m)) = g^kg^la^mg^{-l}g^{-m} = g^{k+l}a^mg^{-k-l} =
\vfi_{k+l}(a^m),$$ also $\vfi_k \circ \vfi_l = \vfi_{k+l}$.

Dann ist $\vfi$ ein Gruppenhomomorphismus, denn
für $g^k, g^l \in \ideal{g}$ mit $k, l \in \Z$ gilt
$$\vfi(g^kg^l) = \vfi(g^{k+l}) = \vfi_{k+l} = \vfi_k \circ \vfi_l = \vfi(g^k)
\circ \vfi(g^l).$$
Außerdem ist $\vfi$ nicht-trivial, denn $\vfi(g) = (a \mapsto gag^{-1}) \ne
\Id_{\ideal{a}}$. \qed

\paragraph{Lemma 2:} Für eine Primzahl $p$ gilt $\Aut(C_p) \cong C_{p-1}$.
\vspace{-.5cm}
\paragraph{Beweis.} Betrachte die Abbildung
$$\psi: (\Z / p\Z)^\times \to \Aut(\Z / p\Z), \qquad a+p\Z \mapsto
\lambda_{a+p\Z},$$
wobei
$$\lambda_{a+p\Z} : \Z / p\Z \to \Z / p\Z, b+p\Z \mapsto ab+p\Z$$
als Linksmultiplikation mit einer Einheit ein Automorphismus von $\Z / p\Z$ ist.

Die Abbildung $\psi$ ist wohldefiniert: Seien $a_1+p\Z, a_2+p\Z \in (\Z /
p\Z)^\times$ mit $a_1+p\Z = a_2+p\Z$. Dann gilt für alle $b+p\Z \in \Z / p\Z$:
$$\begin{aligned}
\psi(a_1+p\Z)(b+p\Z) &= a_1b+p\Z = (a_1+p\Z)(b+p\Z) = (a_2+p\Z)(b+p\Z) \\
&=a_2b+p\Z = \psi(a_2+p\Z)(b+p\Z),
\end{aligned}$$
also $\psi(a_1+p\Z) = \psi(a_2+p\Z)$.

Ein Automorphismus $\alpha \in \Aut(\Z / p\Z)$ ist durch das Bild von $1+p\Z$
unter $\alpha$ eindeutig bestimmt. Es gilt $\alpha(1+p\Z) \in (\Z /
p\Z)^\times$, also $\psi(\alpha(1+p\Z)) = \lambda_{\alpha(1+p\Z)} = \alpha$.
Daher ist $\psi$ surjektiv. Wegen
$$\abs{(\Z / p\Z)^\times} = p-1 = \abs{\set{\lambda_{a+p\Z} \mid a+p\Z \in (\Z /
p\Z)^\times}} = \abs{\Aut(\Z / p\Z)}$$
ist $\psi$ außerdem injektiv.

Zuletzt ist noch zu zeigen, dass $\psi$ ein Gruppenhomomorphismus ist. Seien
$a_1+p\Z, a_2+p\Z \in (\Z / p\Z)^\times$. Dann gilt für alle $b+p\Z \in \Z /
p\Z$:
$$\begin{aligned}
\psi((a_1+p\Z)(a_2+p\Z))(b+p\Z) &= \psi(a_1a_2+p\Z)(b+p\Z) = a_1a_2b+p\Z \\
&= \psi(a_1+p\Z)(a_2b+p\Z) = \psi(a_1+p\Z)(\psi(a_2+p\Z)(b)).
\end{aligned}$$
Somit ist $\psi((a_1+p\Z)(a_2+p\Z)) = \psi(a_1+p\Z) \circ \psi(a_2+p\Z)$.

Also ist $(\Z / p\Z)^\times \cong \Aut(\Z / p\Z)$. Da $(\Z / p\Z)^\times$ als
Einheitengruppe eines Körpers zyklisch ist (Satz 15.15), haben wir
$$\Aut(C_p) \cong \Aut(\Z / p\Z) \cong (\Z / p\Z)^\times \cong C_{p-1},$$
wie gewünscht. \qed

\newpage

Sei $G$ eine endliche Gruppe und sei $p$ der kleinste Primteiler von $\abs{G}$.
Sei $N \trianglelefteq G$ mit $\abs{N} = p$.

Da $N$ von Primzahlordnung ist, ist $N$ zyklisch und es gilt $N = \ideal{a}$ für
ein $a \in N \setminus \set{e}$. Wir wollen $a \in Z(G)$ zeigen.

Angenommen, es gäbe ein $g \in G$ mit $ga \ne ag$. Dann gäbe es laut Lemma\ 1
und Lemma\ 2 einen nicht-trivialen Gruppenhomomorphismus
$$\vfi: \ideal{g} \to \Aut(\ideal{a}) \cong \Aut(C_p) \cong C_{p-1}.$$
Wir hätten $1 < \abs{\Img(\vfi)} < p$ und $\abs{\Img(\vfi)} =
\frac{\abs{\ideal{g}}}{\abs{\Ker(\vfi)}}$ wäre ein Teiler von $\abs{G}$. Damit
wäre aber $p$ doch nicht der kleinste Primteiler von $\abs{G}$.

Insgesamt erhalten wir $\set{e} \subsetneq \set{e, a} \subseteq Z(G)$.

\newpage

## Aufgabe 3

(a) Es wird davon ausgegangen, dass $n \in \N$ gelten soll.

    Nach Satz 21.15 sind drei Dinge nachzuweisen:
    $$T_n \trianglelefteq E_n(\R), \qquad E_n(\R) = T_n \cdot O_n(\R) \qquad
    \text{und} \qquad T_n \cap O_n(\R) = \set{\Id_{\R^n}}.$$

    Um zu zeigen, dass $T_n$ ein Normalteiler in $E_n(\R)$ ist, sei $\tau \in
    T_n$ eine Translation und $\alpha \in E_n(\R)$ eine Isometrie. Dann gibt es
    $a, b \in \R^n$ und $o \in O_n(\R)$, sodass $\tau(v) = v + b$ und $\alpha(v)
    = o(v) + a$ und $\alpha^{-1}(v) = o^{-1}(v - a)$ für alle $v \in \R^n$.
    Für alle $v \in \R^n$ ist nun
    $$\alpha\tau\alpha^{-1}(v) = \alpha\tau(o^{-1}(v - a)) = \alpha(o^{-1}(v) -
    o^{-1}(a) + b) = o(o^{-1}(v) - o^{-1}(a) + b) + a = v - a + o(b) + a = v +
    o(b),$$
    also $\alpha\tau\alpha^{-1} \in T_n$ und daher $T_n \trianglelefteq
    E_n(\R)$.

    Offenbar ist $E_n(\R) \subseteq T_n \cdot O_n(\R)$. Ist umgekehrt $\alpha
    \in E_n(\R)$, so gibt es $o \in O_n(\R)$ und $a \in \R^n$ mit
    $\alpha(v) = o(v) + a$ für $v \in \R^n$. Mit $\tau: \R^n \to \R^n, \quad v
    \mapsto v + a$ ist $\alpha = \tau\vfi \in T_n \cdot O_n(\R)$. Es folgt
    $E_n(\R) = T_n \cdot O_n(\R)$.

    Schließlich sei $\alpha \in T_n \cap O_n(\R)$. Wegen $\alpha \in T_n$ gibt
    es ein $a \in \R^n$ mit $\alpha(v) = v + a$ für alle $v \in \R^n$.
    Wegen $\alpha \in O_n(\R)$ ist $0 = \alpha(0) = 0 + a = a$. Also ist
    $\alpha(v) = v$ für alle $v \in \R^n$ und es gilt $T_n \cap O_n(\R) =
    \set{\Id_{\R^n}}$.

(b) Es wird davon ausgegangen, dass $n \in \N$ mit $n \geqslant 3$ gelten soll
    (siehe dazu Aufgabe 4 (b)).

    Schreibe $\rho \coloneqq (1\;2\;\cdots\;n)$ und
    $$\tau \coloneqq \begin{cases}
        (1\;n)(2\;n-1) \cdots \left( \frac{n-1}{2} \; \frac{n+1}{2} \right) &
        \text{für $n$ ungerade}, \\
        (1\;n)(2\;n-1) \cdots \left( \frac{k}{2} - 1\; \frac{k}{2} + 1 \right) &
        \text{für $n$ gerade.}
    \end{cases}$$

    In Aufgabe 4 (a) wird $D_n = \ideal{\rho} \rtimes \ideal{\tau} :\cong
    \ideal{\rho} \rtimes_\Psi \ideal{\tau}$ gezeigt, wobei $\Psi$ durch den für
    das innere semidirekte Produkt verwendeten Gruppenhomomorphismus
    $$\Psi : \ideal{\tau} \to \Aut(\ideal{\rho}), \qquad \pi
    \mapsto (\rho^k \mapsto \pi\rho^k\pi^{-1})$$
    gegeben ist.

    Wir fassen auch $\Phi$ als Gruppenhomomorphismus $\Phi: \ideal{\tau} \to
    \Aut(\ideal{\rho})$ auf. Es gibt nur zwei Gruppenhomomorphismen $C_2 \to
    \Aut(C_n)$ und sowohl $\Phi$ als auch $\Psi$ sind nicht-trivial. Also ist
    $\Phi = \Psi$ und
    $$D_n \cong \ideal{\rho} \rtimes_\Psi \ideal{\tau} =
    \ideal{\rho} \rtimes_\Phi \ideal{\tau} \cong C_n \rtimes_\Phi C_2.$$

\newpage

## Aufgabe 4

\paragraph{Notation:} Für $n \in \N$ mit $n \geqslant 2$ (sic!) schreibe $\rho
\coloneqq \rho_n \coloneqq (1\;2\;\cdots\;n)$ und
$$\tau \coloneqq \tau_n \coloneqq \begin{cases}
    (1\;n)(2\;n-1) \cdots \left( \frac{n-1}{2} \; \frac{n+1}{2} \right) &
    \text{für $n$ ungerade}, \\
    (1\;n)(2\;n-1) \cdots \left( \frac{k}{2} - 1\; \frac{k}{2} + 1 \right) &
    \text{für $n$ gerade.}
\end{cases}$$
Ferner sei $a \text{ mod } n \coloneqq \min{(a+n\Z) \cap \N}$ für $a \in \Z$.

\paragraph{Lemma:} Sei $n \in \N$ mit $n \geqslant 3$. Dann gilt $D_n =
\ideal{\rho} \cdot \ideal{\tau}$. \vspace{-.5cm}
\paragraph{Beweis.} Nach Bemerkung 17.9 (a) ist $D_n = \ideal{\rho, \sigma}$ mit
$\sigma = (1\;n) \tau$. Durch Vertauschen von 1 und $n$ erhalten wir $D_n =
\ideal{\rho, \tau}$. Nun zeigen wir noch $ab \in \ideal{\rho} \cdot
\ideal{\tau}$ für $a, b \in \set{\rho, \tau}$, denn dann ist liegt jedes Produkt
mit Faktoren in $\set{\rho, \tau}$ in $\ideal{\rho} \cdot \ideal{\tau}$ und
wir haben
$$D_n = \ideal{\rho, \tau} \subseteq \ideal{\rho} \cdot \ideal{\tau} \subseteq
\ideal{\rho, \tau} = D_n.$$
Also, frisch an's Werk:

* $a = \rho, b = \tau$: Klar.
* $a = \rho, b = \rho$: Klar.
* $a = \tau, b = \tau$: Klar.
* $a = \tau, b = \rho$: Für $i \in \set{1, \dots, n}$ gilt
  $$\begin{aligned}
  ab(i) &= \tau\rho(i) = \tau(i+1 \text{ mod } n) = -(i+1) \text{ mod } n =
  -i+n-1 \text{ mod } n \\
  &= \rho^{n-1}(-i \text{ mod } n) = \rho^{n-1}\tau(i),
  \end{aligned}$$
  also $ab = \rho^{n-1}\tau \in \ideal{\rho} \cdot \ideal{\tau}$. \qed

---

(a) Es wird davon ausgegangen, dass $n \in \N$ mit $n \geqslant 3$ gelten soll
    (siehe dazu Aufgabe 4 (b)).

    Die Untergruppe $\ideal{\rho}$ besitzt Index 2 in $D_n$ und ist daher ein
    Normalteiler. Laut Lemma gilt $D_n = \ideal{\rho} \cdot \ideal{\tau}$.
    Offenbar ist $\ideal{\rho} \cap \ideal{\tau} = \set{\Id}$. Satz\ 21.15
    liefert $D_n = \ideal{\rho} \rtimes \ideal{\tau}$.

(b) Satz 17.8 definiert $D_n$ nur für $n \geqslant 3$. Bemerkung 17.9 (a) stellt
    keine Bedingung an $n$, aber Einsetzen in 17.9 (a) liefert
    $D_2 = \ideal{\rho_2, \tau_2} = \ideal{(1\;2), (1\;2)} \cong C_2 \not\cong
    C_2 \times C_2$.
    Im Streben nach Allgemeinheit wäre es wohl am Besten, $D_n :\cong C_n
    \rtimes_\Phi C_2$ mit $\Phi$ wie in Aufgabe 3 (b) zu definieren. Dann wäre
    bei dieser Aufgabe aber nichts zu tun, denn für $n = 2$ ist $\Phi$ trivial
    und $C_2 \times C_2 = C_2 \rtimes_\Phi C_2$.

    Alternativ könnte man das ausgeartete reguläre 1- bzw. 2-Eck erlauben und
    wieder $D_1$ bzw. $D_2$ als dessen Symmetriegruppe definieren. Das 2-Eck
    besitzt als Symmetrien die Achsenspiegelungen an $x$- und $y$-Achse und die
    Punktspiegelung im Ursprung. Somit ist tatsächlich $D_2 = V_4 = C_2 \times
    C_2$.

(c) Die Aussage $D_3 = C_3 \rtimes C_2$ gilt nach (a). Der bei diesem
    semidirekten Produkt verwendete Gruppenhomomorphismus $\Phi: C_2 \to
    \Aut(C_3)$ ist nicht-trivial, daher ist $C_3 \rtimes C_2 \not \cong C_3
    \times C_2 \cong C_6$. Weil es bis auf Isomorphie nur zwei Gruppen
    sechster Ordnung gibt, ist daher $C_3 \rtimes C_2 \cong S_3$.


\newpage

## Aufgabe 5

Es gilt $\abs{C_3 \rtimes_\Phi C_4} = \abs{C_3 \times C_4} = 12$. Die Gruppe ist
nicht kommutativ, denn
$$(a, \tilde{e}) *_\Phi (e, b) = (a \Phi(\tilde{e})(e), \tilde{e} b) = (a,
b) \ne (a^2, b) = (e \Phi(b)(a), b\tilde{e}) = (e, b) *_\Phi (a,
\tilde{e}).$$
Es gilt $(e, b) \ne (e, \tilde{e})$ und $(e, b)^2 = (e, b^2) \ne (e, \tilde{e})$
und $(e, b)^3 = (e, b^3) \ne (e, \tilde{e})$, also $\ord_{C_3 \rtimes C_4}((e,
b)) > 3$. Somit ist $C_3 \rtimes C_4 \not\cong A_4$, denn in $A_4$ existieren
nur Elemente der Ordnungen 1, 2 und 3.

\newpage

## Aufgabe 6

Es folgt ein Spezialfall des Satzes von Cauchy, der in seiner allgemeinen
Fassung allerdings noch nicht bewiesen wurde.

\paragraph{Lemma 1:} Ist $G$ eine endliche Gruppe gerader Ordnung, so enthält
$G$ ein Element zweiter Ordnung. \vspace{-.5cm}
\paragraph{Beweis.} Für $a \in G$ gilt
$$\abs{\set{a, a^{-1}}} = 2 \iff a \ne a^{-1} \iff a^2 \ne e \iff \ord_G(a) >
2.$$
Setzt man $M = \set{a \in G \mid \ord_G(a) > 2}$, so ist $M$ die disjunkte
Vereinigung von Mengen der Form $\set{a, a^{-1}}$ mit $a \in G$ und $\ord_G(a) >
2$. Also ist $\abs{M}$ gerade.

Gäbe es in $G$ kein Element zweiter Ordnung, so wäre $\abs{G} = \abs{\set{e}
\cupdot M} = 1 + \abs{M}$ ungerade. \qed

Genau genommen hier wurde sogar bewiesen, dass es in Gruppen gerader Ordnung
ungerade viele Elemente zweiter Ordnung gibt.

\paragraph{Lemma 2:} Sei $k \in \N_+$ ungerade und $\tau \in S_{2k}$ mit $\tau^2
= \Id$. Besitzt $\tau$ keinen Fixpunkt, so ist $\tau$ ungerade. \vspace{-.5cm}
\paragraph{Beweis.} Gilt $\tau(i) = j$ für $i, j \in \set{1, \dots, 2k}$, so
gilt $\tau(j) = \tau^2(i) = i$. Damit ist $\tau$ Produkt von Transpositionen mit
disjunkten Wirkungsbereichen. Da $\tau$ keinen Fixpunkt besitzt, sind dies $k$
Transpositionen und wir erhalten $\sign(\tau) = (-1)^k = -1$. \qed

\paragraph{Lemma 3:} Sei $n \in \N$ mit $n \geqslant 3$ und $U \subsetneq S_n$
eine Untergruppe, die nicht in $A_n$ enthalten ist. Dann besitzt $U$ einen
nicht-trivialen Normalteiler, der echt in $G$ enthalten ist. \vspace{-.5cm}
\paragraph{Beweis.} Laut Blatt 9, Aufgabe 4(c), enthält $U \cap A_n$ genau die
Hälfte der Elemente von $U$. Also gilt $\set{\Id} \subsetneq U \cap A_n
\subsetneq U$. Weil $U \cap A_n$ Index 2 in $U$ hat, ist außerdem $U \cap A_n
\trianglelefteq U$. \qed

---

Sei $m = \abs{G} = 2(2n+1) \geqslant 3$. Für $a \in G$ sei $\lambda_a: G \to G,
g \mapsto ag$ die Linksmultiplikation mit $a$.

Wir betrachten die Abbildung
$$\iota : G \xhookrightarrow{\vfi} \Bij(G, G) \xrightarrow{\psi} S_m$$
mit dem Monomorphismus $\vfi: a \mapsto \lambda_a$ und dem Isomorphismus $\psi$
wie in Satz 20.15. Wir können statt $G$ auch $H = \iota(G) \cong G$ betrachten.

Laut Lemma\ 1 existiert ein Element $g \in G$ mit $\ord_G(g) = 2$. Wegen $g \ne
e$ besitzen $\lambda_g$ und damit $\tau = \psi(\lambda_g)$ keinen Fixpunkt.
Wegen $H \cong G$ ist $\ord_{H}(\tau) = \ord_{H}(\iota(g)) = \ord_G(g) = 2$.

Laut Lemma\ 2 ist daher $\tau \in H$ ungerade. Somit ist $H$ nicht $A_n$
enthalten und besitzt laut Lemma\ 3 einen nicht-trivialen Normalteiler, der echt
in $H$ enthalten ist. Wegen $G \cong H$ besitzt auch $G$ einen solchen
Normalteiler.

<!--
## Test

\paragraph{Lemma:} Genau dann ist $a \in \Z$ gerade, wenn $a \in 2\Z$.
\vspace{-.5cm}
\paragraph{Beweis.}
\begin{enumerate}
    \item[{\includegraphics[width=7pt,height=7pt]{galgen.png}}]
    Ist $a$ gerade, so gibt es ein $b \in \Z$ mit $a = 2b \in \set{2b \mid b \in
    \Z} = 2\Z$.
    \item[$\Leftarrow$] Sei $a \in 2\Z$. Dann gibt es ein $b \in \Z$ mit $a =
    2b$ und $a$ ist gerade. \qed
\end{enumerate}
-->
