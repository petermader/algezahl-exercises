# Algebra und Zahlentheorie -- Übungsblatt 2

\newcommand\ideal[1]{\left<#1\right>}

## Aufgabe 1

(a) Setze $\Ker(d) = \set{x \in R \mid d(x) = 0}$.

    Es gilt $0 = 0 d(0) + 0 d(0) = d(0 \cdot 0) = d(0)$, also $0 \in \Ker(d)$.

    Seien $x, y \in \Ker(d)$. Dann gilt
    $$\begin{aligned}
    d(x + y) &= d(x) + d(y) = 0 + 0 = 0, \\
    d(-x) &= d(x) + d(-x) = d(x - x) = d(0) \qquad \text{und}\\
    d(xy) &= x d(y) + y d(x) = x \cdot 0 + y \cdot 0 = 0,
    \end{aligned}$$
    also $x + y, -x, xy \in \Ker(d)$. Somit ist $\Ker(d)$ ein Unterring von $R$.

    Es gilt $d(1) = d(1 \cdot 1) = 1 d(1) + 1 d(1) = d(1) + d(1)$. Beidseitige
    Subtraktion von $d(1)$ liefert $0 = d(1)$, also $1 \in \Ker(d)$.

(b) Per Induktion nach $n$:

    \paragraph{Induktionsanfang:} $n = 1$. Ist $x \in R$, so gilt
    $$d(x^n) = d(x^1) = d(x) = 1 \cdot x^{1-1} \cdot d(x) = nx^{n-1}d(x).$$

    \paragraph{Induktionsschritt:} $n > 1$. Ist $x \in R$, so gilt
    $$\begin{aligned}
    d(x^n) &= d(x \cdot x^{n-1}) = x d(x^{n-1}) + x^{n-1} d(x) \\
    &\stackrel{\text{IV}}{=} (n-1) x x^{n-2} d(x) + x^{n-1} d(x) =
    (n-1)x^{n-1}d(x) + x^{n-1}d(x) = nx^{n-1}d(x).
    \end{aligned}$$

(c) Sei $R \in \set{\Q, \Z, \quotient{\Z}{n\Z}}$ und $d$ eine Derivation von
    $R$.

    Für $k \in \Z$ oder $k \in \quotient{\Z}{n\Z}$ gilt
    $$d(k) = d\left( \sum_{i=1}^{k} 1\right) = \sum_{i=1}^{k} d(1) = 0.$$
    sowie im Fall $k \in \Z \nozero$:
    $$0 = d(1) = d\left( \frac{k}{k} \right) = k d\left( \frac{1}{k} \right) +
    \frac{1}{k} d(k) = k d\left( \frac{1}{k} \right).$$
    Da $\Z$ ein Integritätsring ist, folgt $d\left( \frac{1}{k} \right) = 0$.

    Ist $k \in \Q$, so schreibe $k = \frac{p}{q}$ mit $p \in \Z$ und $q \in
    \N_+$. Dann gilt
    $$d(k) = d\left( \frac{p}{q} \right) = p d\left( \frac{1}{q} \right) + q
    d(q) = 0 + 0 = 0.$$

## Aufgabe 2

(a) Es gilt $384 = 6 \cdot 63 + 6, 63 = 10 \cdot 6 + 3, 6 = 2 \cdot 3 + 0$, also
    $$\begin{aligned}
    \ggT(63, 384) &= 3 = 63 - 10 \cdot 6 = 63 - 10 \cdot (384 - 6 \cdot 63) =
    63 - 10 \cdot 384 + 10 \cdot 6 \cdot 63 \\ &= -10 \cdot 384 + 61 \cdot 63.
    \end{aligned}$$

(b) Es gilt $245 = 20 \cdot 12 + 5, 12 = 2 \cdot 5 + 2, 5 = 2 \cdot 2 + 1, 2 = 2
    \cdot 1 + 0$, also
    $$\begin{aligned}
    \ggT(245, 12) &= 1 = 5 - 2 \cdot 2 = 5 - 2 \cdot (12 - 2
    \cdot 5) = -2 \cdot 12 + (1 + 4) \cdot 5 \\ &= -2 \cdot 12 + 5 \cdot (245 -
    20 \cdot 12) = 5 \cdot 245 - 102 \cdot 12.
    \end{aligned}$$
    Somit gilt $\overline{12} \cdot \overline{-102} = \overline{5 \cdot 245} +
    \overline{-102 \cdot 12} = \overline{5 \cdot 245 - 102 \cdot 12} =
    \overline{1}$,
    und $\overline{12}^{-1} = \overline{-102} = \overline{143}$.

## Aufgabe 3

Sei $R = \quotient{\Z}{n\Z}$. Zeigt man für $I \subseteq R$
$$I \text{ Ideal} \qquad \iff \qquad \text{es gibt ein $d \in \Z$ mit $d \mid n$
und $I = \overline{d}R$},$$
so folgt direkt, dass $\abs{T_n}$, die Anzahl der Teiler von $n$,
gleich der Anzahl der Ideale von $R$ ist.

\begin{enumerate}
    \item[$\Leftarrow$] Für $d \in \Z$ ist $\overline{d}R = \ideal{d}$ ein
    Ideal.

    \item[$\Rightarrow$] Sei $I = \set{\overline{x_1}, \dots, \overline{x_k}}
    \subseteq R$ ein Ideal in $R$. Dabei gelte $x_1 = n$ und $x_i \ne 0$ für $i
    = 2, \dots, k$.

    Sei $d = \ggT(x_1, \dots, x_k)$ der größte gemeinsame Teiler der $x_i$ in
    $\Z$. Es gilt dann auch $\overline{d} \mid
    \overline{x_i}$ für alle $i = 1, \dots, k$. Jedes Element $m \in I$ ist von
    der Form $m = \sum_{i=1}^{k} a_i\overline{x_i}$ mit $a_1, \dots, a_k \in R$
    und damit ein Vielfaches von $\overline{d}$. Dies zeigt $I \subseteq
    \ideal{\overline{d}}$.

    Nach induktiver Anwendung des Lemmas von Bézout gibt es $a_1, \dots, a_k$
    mit $d = \sum_{i=1}^{k} a_ix_i$. Dies zeigt $\overline{d} = \sum_{i=1}^{k}
    \overline{b_i} \cdot \overline{x_i} \in I$, also $\ideal{\overline{d}}
    \subseteq I$.

    Insgesamt erhalten wir $I = \ideal{\overline{d}} = \overline{d}R$. Nach
    Konstruktion von $d$ gilt ferner $d \mid n$.
\end{enumerate}

## Aufgabe 4

(a) Die Ringeigenschaften von $R$ und $S$ vererben sich auf $P = R \times S$.

    Seien $a = (a_1, a_2), b = (b_1, b_2), c = (c_1, c_2) \in P$.

    \paragraph{Assoziativität der Addition:} Es gilt
    $$\begin{aligned}
    (a + b) + c &= (a_1 + b_1, a_2 + b_2) + (c_1, c_2) = ((a_1 + b_1) + c_1,
    (a_2 + b_2) + c_2) \\ &= (a_1 + (b_1 + c_1), a_2 + (b_2 + c_2)) = (a_1, a_2)
    + (b_1 + c_1, b_2 + c_2) = a + (b + c).
    \end{aligned}$$

    \paragraph{Assoziativität der Multiplikation:} Es gilt
    $$\begin{aligned}
    (a \cdot b) \cdot c &= (a_1 \cdot b_1, a_2 \cdot b_2) \cdot (c_1, c_2) =
    ((a_1 \cdot b_1) \cdot c_1, (a_2 \cdot b_2) \cdot c_2) \\
    &= (a_1 \cdot (b_1 \cdot c_1), a_2 \cdot (b_2 \cdot c_2)) = (a_1, a_2) \cdot
    (b_1 \cdot c_1, b_2 \cdot c_2) = a \cdot (b \cdot c).
    \end{aligned}$$

    \paragraph{Kommutativität der Addition:} Es gilt
    $$a + b = (a_1 + b_1, a_2 + b_2) = (b_1 + a_1, b_2 + a_1) = b + a.$$
    Analog zeigt man die Kommutativität der Multiplikation, sofern sie für $R$
    und $S$ erfüllt ist.

    \paragraph{Additiv neutrales Element:} Verwende $0_P = (0_R, 0_S)$. Dann
    gilt
    $$a + 0_P = (a_1 + 0_R, a_2 + 0_S) = a = (0_R + a_1, 0_S + a_2) = 0_P + a.$$
    Analog zeigt man die Existenz eines multiplikativ neutralen Elements, indem
    man $1_P = (1_R, 1_S)$ verwendet.

    \paragraph{Additiv inverse Elemente:} Verwende $-a = (-a_1, -a_2)$. Dann
    gilt
    $$a + (-a) = (a_1 - a_1, a_2 - a_2) = (0_R, 0_S) = 0_P = (0_R, 0_S) = (-a_1
    + a_1, -a_2 + a_2) = -a + a.$$

    \paragraph{Linksdistributivität:} Es gilt
    $$a \cdot (b + c) = (a_1 \cdot (b_1 + c_1), a_2 \cdot (b_2 + c_2)) = (a_1b_1
    + a_1c_1, a_2b_2 + a_2c_2) = ab + ac.$$

    \paragraph{Rechtsdistributivität:} Es gilt
    $$(b + c) \cdot a = ((b_1 + c_1) \cdot a_1, (b_2 + c_2) \cdot a_2) = (b_1a_1
    + c_1a_1, b_2a_2 + c_2a_2) = ba + ca.$$

(b) Es soll per Induktion nach $s$ gezeigt werden, dass für Körper $K_1, \dots,
    K_s$ der Ring $R = K_1 \times \dots \times K_s$ genau $2^{s}$ Ideale
    besitzt. Der Korrektor bereite sich auf einen unübersichtlichen Beweis vor.

    \paragraph{Induktionsanfang:} Mit $s = 0$ ist $R = \set{()}$, und es gibt
    $2^0 = 1$ Ideal, nämlich $R$.

    \paragraph{Induktionsschritt:} Sei $s > 0$.

    * Zunächst soll gezeigt werden, dass es höchstens $2^s$ Ideale in $R$ gibt.

      Sei $I \subseteq R$ ein Ideal. Wie man leicht überprüft, ist dann auch
      $$I' = \set{(a_1, \dots, a_{s-1}) \mid (a_1, \dots, a_s) \in I}$$
      ein Ideal in $K_1 \times \dots \times K_{s-1}$.

      \paragraph{Lokales Lemma: } Es gilt $I' \times \set{0} \subseteq I \subseteq
      I' \times K_s$. \vspace{-0.4cm}
      \paragraph{Beweis.} Nach Konstruktion von $I'$ gilt $I \subseteq I' \times
      K_s$. Ist ferner $(b_1, \dots, b_{s-1}) \in I'$, so gibt es ein $b_s \in
      K_s$ mit $(b_1, \dots, b_s) \in I$. Nun gilt
      $$(b_1, \dots, b_{s-1}, 0) = \underbrace{(b_1, \dots, b_s)}_{\in I} \cdot
      \underbrace{(1, \dots, 1, 0)}_{\in R} \in I,$$
      also $I' \times \set{0} \subseteq I$. \hfill \qed

      Nun wird noch gezeigt, dass eine der beiden Inklusionen aus dem Lemma eine
      Gleichheit ist.

      Gilt $I \ne I' \times \set{0}$, so gibt es ein $a = (a_1, \dots, a_s) \in
      I$ mit $a_s \ne 0$. Dann ist auch $e_s = (0, \dots, 0, 1) = a \cdot (0,
      \dots, 0, a_s^{-1}) \in I$.

      Für alle $(b_1, \dots, b_s) \in I' \times K_s$ gilt dann
      $$(b_1, \dots, b_s) = \underbrace{(b_1, \dots, b_{s-1}, 0)}_{\in I' \times
      \set{0} \subseteq I} - \underbrace{e_s}_{\in I} \cdot \underbrace{(0, \dots,
      0, b_s)}_{\in R} \in I.$$
      Dies zeigt $I' \times K_s \subseteq I$.

      Insgesamt gilt entweder $I = I' \times \set{0}$ oder $I = I' \times K_s$.
      Für $I'$ gibt es laut Induktionsvoraussetzung $2^{s-1}$ Möglichkeiten.
      Somit gibt es für $I$ höchstens $2 \cdot 2^{s-1} = 2^s$ Möglichkeiten.

    * Zu zeigen ist noch, dass die $2^s$ Möglichkeiten auch tatsächlich alle
      auftreten können. Dazu ist zu zeigen, dass für ein Ideal $I'$ in $K_1
      \times \dots \times K_{s-1}$ die Mengen $I' \times \set{0}$ und $I' \times
      K_s$ Ideale in $R$ sind. Es sei dem Korrektor erspart, den Nachweis dieser
      Tatsache zu überprüfen. Er möge die dadurch gewonnene Freizeit für einen
      Spaziergang an der frischen Luft oder für einige Minuten Klavierspiel
      nutzen.

## Aufgabe 5

<!-- TODO: Man braucht das Einselement doch gar nicht? -->

(a) Seien $a, b \in R$ und sei $I = \left<a, b\right> = \set{ra + sb \mid r, s
    \in R}$.

    Sind $x, y \in I$, so gibt es $r_x, r_y, s_x, r_y \in R$ mit
    $x = r_xa + s_xb, y = r_ya + s_yb$. Dann gilt $x + y = (r_x + r_y)a + (s_x +
    s_y)b \in I$ sowie $-x = (-r_x)a + (-s_x)b \in I$.

    Ist außerdem $k \in R$, so gilt $kx = (kr_x)a + (ks_x)b \in I$.

(b) Sei $R$ ein Hauptidealbereich und $a, b \in R$. Dann besitzt $I = \left<a,
    b\right>$ einen Erzeuger $t \in R$, es gilt also $I = t \cdot R$, und man
    kann $t = ra + sb$ mit $r, s \in R$ schreiben.

    Außerdem kann man wegen $a, b \in I$ auch $a = tm_a$ und $b = tm_b$ mit
    $m_a, m_b \in R$ schreiben, $t$ ist also ein gemeinsamer Teiler von $a$ und
    $b$.

    Ist $u \in R$ ein weiterer gemeinsamer Teiler von $a$ und $b$, so gibt es
    $n_a, n_b \in R$ mit $a = un_a, b = un_b$. Nun gilt
    $$t = ra + sb = run_a + sun_b = u(rn_a + sn_b),$$
    also $u \mid t$, und $t$ ist ein größter gemeinsamer Teiler von $a$ und $b$.
