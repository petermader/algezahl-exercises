# Algebra und Zahlentheorie -- Übungsblatt 8

## Aufgabe 1

(a) Sei $G$ eine Gruppe und seien $U_1 \subsetneq G$ und $U_2 \subsetneq G$ zwei
    echte Untergruppen von $G$. Wir unterscheiden drei Fälle:

    * $U_1 \subseteq U_2$. Dann ist $U_1 \cup U_2 = U_2 \subsetneq G$.
    * $U_2 \subseteq U_1$. Dann ist $U_1 \cup U_2 = U_1 \subsetneq G$.
    * Andernfalls ist $U_1 \setminus U_2 \ne \varnothing \ne U_2 \setminus U_1$.
      Sei $a \in U_1 \setminus U_2$ und $b \in U_2 \setminus U_1$. Dann gilt
      $a^{-1} \in U_1 \setminus U_2$ und $b^{-1} \in U_2 \setminus U_1$.

      Es gilt $ab \notin U_1$, sonst hätten wir
      $$b = \underbrace{a^{-1}}_{\in U_1} \underbrace{(ab)}_{\in U_1} \in U_1.$$
      Es gilt $ab \notin U_2$, sonst hätten wir
      $$a = \underbrace{(ab)}_{\in U_2} \underbrace{b^{-1}}_{\in U_2} \in U_2.$$

      Also ist $ab \in G \setminus (U_1 \cup U_2)$ und $G \ne U_1 \cup U_2$.

(b) Zu zeigen ist, dass es in $S$ Inverse gibt. Sei $a \in S$. Dann ist $T =
    \set{a^i \mid i \in \N_+} \subseteq S$, da $S$ unter der Multiplikation
    abgeschlossen ist. Die Menge $T$ ist also endlich und es gibt $n, m \in
    \N_+$ mit $n < m$ und $a^n = a^m$.

    In $G$ erhält man durch Kürzen $1 = a^{m-n}$ und $a^{-1} = a^{m - n - 1}$.

    * Ist $a = 1$, so gilt wegen $m - n \in \N_+$ auch $a^{-1} = 1 \in S$.
    * Ist $a \ne 1$, so ist $m - n > 1$ und $m - n - 1 \in \N_+$, also
      $a^{-1} = a^{m-n-1} \in T \subseteq S$.

\newpage

## Aufgabe 2

Es wird additive Notation für Gruppen verwendet.

\paragraph{Lemma:} Seien $n, m \in \N_+$. Genau dann ist $G = C_n \times C_m$
zyklisch, wenn $\ggT(n, m) = 1$. \vspace{-0.5cm}
\paragraph{Beweis.} Zunächst gelte $\ggT(n, m) = 1$. Die Gruppe $G$ ist als
Produkt abelscher Gruppen abelsch. Schreibe $C_n = \ideal{a}$ und $C_m =
\ideal{b}$. Es gilt
$$\ord_G((a, 0)) = \ord_{C_n}(a) = n \qquad \text{und} \qquad \ord_G((0, b)) =
\ord_{C_m}(b) = m.$$
Nun liefert Proposition 15.12 (b), dass $\ord_G((a, b)) = nm = \abs{G}$ und
damit $G \cong C_{nm}$.

Umgekehrt gelte $\ggT(n, m) > 1$. Ist $(a, b) \in G$, so gilt mit $k =
\kgV(\ord_{C_n}(a), \ord_{C_m}(b))$, dass $(a, b)^k = (a^k, b^k) = 0$,
also
$$\ord_G((a, b)) \leqslant k = \kgV(\ord_{C_n}(a), \ord_{C_m}(b)) < nm =
\abs{G}.$$
Somit ist $G$ nicht zyklisch. \qed

Ferner bemerken wir, dass
$$\begin{aligned}
&(\Z / 2\Z)^\times = \ideal{1+2\Z} \cong C_1,
&(\Z / 3\Z)^\times = \ideal{2+3\Z} \cong C_2, \\
&(\Z / 5\Z)^\times = \ideal{2+5\Z} \cong C_4,
&(\Z / 9\Z)^\times = \ideal{2+9\Z} \cong C_6.
\end{aligned}$$

---

(a) Es ist $\overline{7}^1 = \overline{7}, \overline{7}^2 = \overline{19},
    \overline{7}^3 = \overline{13}$ und $\overline{7}^4 = \overline{1}$ und
    daher $\ord(\overline{7}) = 4$.

(b) Nach dem Chinesischen Restsatz (Korollar 6.10) ist
    $$(\Z / 30\Z)^\times \cong (\Z / 2\Z)^\times \times (\Z / 3\Z)^\times \times
    (\Z / 5)^\times \cong C_1 \times C_2 \times C_4 \cong C_2 \times C_4.$$
    Laut dem Lemma ist $(\Z / 30\Z)^\times$ also nicht zyklisch.

(c) Nach dem Chinesischen Restsatz (Korollar 6.10) ist
    $$(\Z / 45\Z)^\times \cong (\Z / 5\Z)^\times \times (\Z /
    9\Z)^\times \cong C_4 \times C_6.$$
    Laut dem Lemma ist $(\Z / 45\Z)^\times$ also nicht zyklisch.

\newpage

## Aufgabe 3

Angenommen, es gäbe einen Isomorphismus $\vfi: K^\times \to K$. Wir haben
$\vfi(1) = 0$ und wegen der Injektivität von $\vfi$ auch $\vfi(-1) \ne 0$. Das
liefert
$$1 + 1 = \frac{\vfi(-1)}{\vfi(-1)} + \frac{\vfi(-1)}{\vfi(-1)} =
\frac{1}{\vfi(-1)}(\vfi(-1) + \vfi(-1)) = \frac{1}{\vfi(-1)} \cdot \vfi((-1)
\cdot (-1)) = \frac{1}{\vfi(-1)} \vfi(1) = 0$$
und daher $-1 = 1$.

Sei $a \in K^\times$. Dann gilt
$$\vfi(a^2) = \vfi(a) + \vfi(a) = (1+1)\vfi(a) = 0,$$
also $a^2 \in \Ker(\vfi) = \set{1}$, also $a \in \set{-1, 1} = \set{1}$. Dann
ist aber $K = \set{0, 1}$ und $\vfi: \set{1} \to \set{0, 1}$ war doch nicht
bijektiv.

\newpage

## Aufgabe 4

Es wird additive Notation für Gruppen verwendet.

\begin{enumerate}[leftmargin=2cm]
    \item[(a) $\Rightarrow$ (c)] Es sei $G$ zyklisch mit $\abs{G} = p^n$ für
    eine Primzahl $p$ und ein $n \in \N_+$. Aus Isomorphiegründen nehmen wir
    $G = \Z / p^n\Z$ an.

    Sei $U \subseteq G$ eine Untergruppe. Dann ist $U$ zyklisch von
    Primzahlpotenzordnung, d.h. es gibt ein $k \in \set{0, \dots, n}$ mit
    $\abs{U} = p^k$ sowie ein $\overline{a} \in U$ mit $\ideal{\overline{a}} =
    U$, also $\ord(\overline{a}) = p^k$. Wir haben $\overline{a} \cdot
    \overline{p}^k = \overline{p}^n$, daher $ap^k = bp^n$ für ein $b \in \Z$
    und schließlich $a = bp^{n-k}$.

    Einerseits ist damit $\overline{a} = \overline{b} \cdot \overline{p}^{n-k}
    \in \ideal{\overline{p}^{n-k}}$. Andererseits muss $\ggT(b, p) = 1$ sein,
    sonst wäre $\ord(\overline{a}) = \ord(b\overline{p}^{n-k}) < p^k$. Also
    existiert $\overline{b}^{-1}$ und wir haben
    $$\overline{p}^{n-k} = \overline{a}\overline{b}^{-1} \in
    \ideal{\overline{a}}.$$
    Das zeigt $U = \ideal{\overline{a}} = \ideal{\overline{p}^{n-k}}$.

    Somit sind die Untergruppen von $G$ sämtlich von der Form
    $\ideal{\overline{p}^{n-k}}$ für ein $k \in \set{0, \dots, n}$. Wegen
    $$\set{\overline{0}} = \ideal{\overline{p}^n} \subsetneq
    \ideal{\overline{p}^{n-1}} \subsetneq \cdots \subsetneq \ideal{\overline{p}}
    \subsetneq \ideal{\overline{1}} = G$$
    sind die Untergruppen von $G$ total geordnet.

    Hiermit ist auch gleich (a) $\Rightarrow$ (b) gezeigt, ein starkes Indiz
    dafür, dass es auch eleganter gegangen wäre.

\item[(c) $\Rightarrow$ (b)] Wegen $\abs{G}$ gibt es eine echte Untergruppe. Da
    $G$ endlich ist, gibt es nur endlich viele echte Untergruppen von $G$. Da
    diese endlich vielen echten Untergruppen total geordnet sind, gibt es ein
    eindeutiges maximales Element.

\item[(b) $\Rightarrow$ (a)] Sei $M \subsetneq G$ die eindeutige echte
    inklusionsmaximale Untergruppe von $G$. Sei $a \in G \setminus M$.

    Angenommen, $\ideal{a} \subsetneq G$. Da $G$ nur endlich viele Untergruppen
    besitzt, gäbe es eine echte inklusionsmaximale Untergruppe $U$ von $G$ mit
    $\ideal{a} \subseteq U$. Wegen $a \in U$ wäre $U \ne M$, entgegen der
    Eindeutigkeit von $M$ als echte inklusionsmaximale Untergruppe.

    Also ist $\ideal{a} = G$ zyklisch. Sei $\abs{G} = p_1^{\alpha_1} \cdots
    p_s^{\alpha_s}$ mit $s \in \N_+$ und $\alpha_1, \dots, \alpha_s \in \N_+$
    sowie paarweise verschiedenen Primzahlen $p_1, \dots, p_s$ die
    Primfaktorzerlegung von $\abs{G}$.

    Für jedes $i \in \set{1, \dots, s}$ ist dann $k_i = \frac{1}{p_i} \abs{G}$
    ein maximaler Teiler von $\abs{G}$ und $\ideal{a^{k_i}}$ eine echte
    inklusionsmaximale Untergruppe von $G$. Das zeigt $s = 1$ und $G$ ist von
    Primzahlpotenzordnung.
\end{enumerate}

\newpage

## Aufgabe 5

\paragraph{Lemma:} Seien $a, b \in G$. Dann gibt es in $G$ ein Element der
Ordnung $\kgV(\ord(a), \ord(b))$. \vspace{-0.5cm}
\paragraph{Beweis.} Schreibe $\ord(a) = \prod_{i=1}^s p_i^{\alpha_i}$ und
$\ord(b) = \prod_{i=1}^s p_i^{\beta_i}$ mit $s \in \N$ und $\alpha_1, \dots,
\alpha_s, \beta_1, \dots, \beta_s \in \N$ und paarweise verschiedenen Primzahlen
$p_1, \dots, p_s$. Dann ist
$$\kgV(\ord(a), \ord(b)) = \prod_{i=1}^s p_i^{\max\set{\alpha_i, \beta_i}}.$$
Setze
$$n = \prod_{i\in I} p_i^{\alpha_i}, \qquad m = \prod_{j \in J}
p_j^{\beta_j},$$
wobei $I = \set{i \in \set{1, \dots, s} \mid \alpha_i > \beta_i}$ und $J =
\set{1, \dots, s} \setminus I$. Dann gilt $n \mid \ord(a)$, $m \mid \ord(b)$
sowie $\ggT(n, m) = 1$.

Sei $\tilde{a} = a^{\frac{\ord(a)}{n}}$ und $\tilde{b} = b^{\frac{\ord(b)}{m}}$.
Proposition 15.12 (a) liefert $\ord\left( \tilde{a} \right) = n$ und
$\ord\left( \tilde{b} \right) = m$.  Wegen der
Kommutativität\footnote{Abelianität?} von $G$ kommutieren $a$ und $b$.
Proposition 15.12 (b) liefert dann $\ord\left( \tilde{a}\tilde{b} \right) = nm =
\kgV(\ord(a), \ord(b))$. \qed

Da das kleinste gemeinsame Vielfache zweier Zahlen kommutativ und assoziativ
ist, kann man die gezeigte Aussage auch für mehr als zwei Gruppenelemente
verwenden.

---

Ist $k \in \N_+$ mit der Eigenschaft, dass $kg = 0$ für alle $g \in G$, so gilt
$\ord(g) \mid k$ für alle $g \in G$. Die Zahl $k$ ist also ein gemeinsames
Vielfaches der Elemente von  $\set{\ord(g) \mid g \in G}$.

Damit gilt $m = \kgV\set{\ord(g) \mid g \in G}$ und das Lemma liefert den Rest.

\newpage

## Aufgabe 6

(a) Sei $a \in \Z / n\Z = \ideal{\overline{1}}$. Schreibe $a = \overline{m}$ für
    ein $m \in \set{0, \dots, n-1}$. Es gilt
    $$\Z / n\Z = \ideal{a} \iff \Z / n\Z = \ideal{m \cdot \overline{1}}
    \stackrel{\text{Satz 15.11}}{\iff} \ggT(n, m) = 1 \iff a = \overline{m} \in
    (\Z / n\Z)^\times.$$
    Daher besitzt $C_n \cong \Z / n\Z$ genau $\abs{(\Z / n\Z)^\times} = \vfi(n)$
    Erzeuger.

(b) Wir gehen mal von $n > 0$ aus.

    Für $d \in \N_+$ mit $d \mid n$ sei $A_d$ die Menge der Erzeuger von $C_d$.
    Wir betrachten $C_d$ als Untergruppe von $C_n$. Da ein Element offenbar nur
    eine Gruppe erzeugen kann, sind die $A_d$ paarweise disjunkt.

    Mit $A \coloneqq \dot{\bigcup}_{d \mid n} A_d$ gilt
    $$\begin{aligned}
    a \in C_n &\iff \ideal{a} \subseteq C_n \iff \ideal{a} = C_d \text{ für ein
    $n \in \N_+$ mit $d \mid n$} \\
    &\iff a \in A_d \text{ für ein $n \in \N_+$ mit $d \mid n$} \iff a \in A.
    \end{aligned}$$

    Mit (a) folgt nun
    $$n = \abs{C_n} = \abs{A} = \sum_{d\mid n} \vfi(d).$$
