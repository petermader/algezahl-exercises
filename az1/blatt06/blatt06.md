# Algebra und Zahlentheorie -- Übungsblatt 6

\newcommand\Nil{\mathrm{Nil}}

## Aufgabe 1

(a) Laut Blatt 1, Aufgabe 4 (a), ist $I = \Nil(R)$ bereits eine additive
    Untergruppe. Ist $r \in R$ und $a \in \Nil(R)$, so gibt es ein $k \in \N$
    mit $a^k = 0$. Da $R$ kommutativ ist, gilt dann $(ra)^k = r^ka^k = 0$, also
    $ra \in \Nil(R)$. Damit ist $I = \Nil(R)$ ein Ideal.

    Ist $a + I \in \quotient{R}{I}$ nilpotent, also $0 + I = (a+I)^k$ für ein $k
    \in \N$, so gilt
    $$I = 0 + I = (a+I)^k = a^k + I,$$
    also $a^k \in I$, und damit $a^k + I = I = 0 + I$.

(b) Wegen $0^1 = 0 \in J$ ist $\sqrt{J} \ne \varnothing$.

    Seien $a, b \in \sqrt{J}$ und $r \in R$. Dann gibt es $n, m \in \N$ mit $a^n
    \in J, b^m \in J$.
    Für $k \in \set{0, \dots, n}$ ist $m + n - k \geqslant m$, und es gilt
    $$a^kb^{m+n-k} = a^kb^{n-k}\underbrace{b^m}_{\in J} \in J.$$
    Für $k \in \set{n, \dots, n+m}$ ist $k \geqslant n$, und es gilt
    $$a^kb^{m+n-k} = \underbrace{a^n}_{\in J}a^{k-n}b^{m+n-k} \in J.$$
    Somit folgt
    $$(a + b)^{m+n} = \sum_{k=0}^{m+n} \underbrace{a^kb^{m+n-k}}_{\in J} \in
    J,$$
    also $a + b \in \sqrt{J}$. Außerdem gilt
    $$(-b)^m = (-1)^m\underbrace{b^m}_{\in J} \in J,$$
    also $-b \in \sqrt{J}$, und
    $$(ra)^n = r^n\underbrace{a^n}_{\in J} \in J,$$
    also $ra \in \sqrt{J}$.

(c) Erst mal ein

    \paragraph{Lemma:} Sei $n \in \N_+$ und sei $n = p_1^{\alpha_1} \cdots
    p_s^{\alpha_s}$ die Primfaktorzerlegung von $n$ mit $s, \alpha_1, \dots,
    \alpha_s \in \N$ und paarweise verschiedenen Primzahlen $p_1, \dots, p_s$.
    Dann gilt $\sqrt{\ideal{n}} = \ideal{p_1 \cdots p_s}$. \vspace{-0.2cm}
    \paragraph{Beweis.} Es sind zwei Inklusionen zu zeigen.

    \begin{enumerate}
        \item[$\subseteq$] Sei $a \in \sqrt{\ideal{n}}$. Dann gibt es ein $k \in
            \N$ mit $a^k \in \ideal{n}$, also $n \mid a^k$. Wegen $p_1 \cdots
            p_s \mid n$ folgt $p_1 \cdots p_s \mid a^k$. Da die $p_i$ paarweise
            verschieden sind, gilt $p_i \mid a^k$ und damit $p_i \mid a$ für
            alle $i \in \set{1, \dots, s}$. Es folgt $p_1 \cdots p_s \mid a$,
            also $a \in \ideal{p_1 \cdots p_s}$.
        \item[$\supseteq$] Sei $a \in \ideal{p_1 \cdots p_s}$. Setze $k =
            \max\set{\alpha_1, \dots, \alpha_s}$. Dann gilt $n \mid p_1^k \cdots
            p_s^k$. Wegen $p_1 \cdots p_s \mid a$ haben wir außerdem $p_1^k
            \cdots p_s^k \mid a^k$ und insgesamt $n \mid a^k$, also $a \in
            \sqrt{\ideal{n}}$. \qed
    \end{enumerate}

    Die Ideale von $\Z$ sind genau die Mengen $\ideal{n}$ mit $n \in \N$.
    Aus $\sqrt{\ideal{0}} = \ideal{0}$ und dem Lemma folgt, dass die Ideale $J$
    von $\Z$ mit $\sqrt{J} = J$ genau die Ideale $\ideal{0}$ und $\ideal{n}$ mit
    den $n \in \N_+$, die jeden Primfaktor nur einmal besitzen.

## Aufgabe 2

(a) Wir haben zwei Implikationen zu beweisen.

    \begin{enumerate}
        \item[$\Rightarrow$] Sei $R$ lokal und $\mathfrak{m} \subsetneq R$ das
            eindeutige maximale Ideal. Laut Korollar 11.12 ist $R \setminus
            R^\times$ die Vereinigung aller maximalen Ideale von $R$. Also gilt
            $R \setminus R^\times = \mathfrak{m}$.
        \item[$\Leftarrow$] Nun sei $I = R \setminus R^\times$ ein Ideal. Ist $J
            \subseteq R$ ein Ideal mit $R \setminus R^\times = I \subsetneq J$,
            so folgt $J \cap R^\times \ne \varnothing$, also $J = R$, und $I$
            ist maximal.

            Ist $\mathfrak{m} \subsetneq R$ ein weiteres maximales Ideal, so
            muss $\mathfrak{m} \cap R^\times = \varnothing$ gelten, also
            $\mathfrak{m} \subseteq R \setminus R^\times = I$. Die Maximalität
            liefert $\mathfrak{m} = I$. Somit ist $I$ das einzige maximale
            Ideal von $R$, und $R$ ist lokal.
    \end{enumerate}

(b) Sei $\eps: R \twoheadrightarrow R/I, a \mapsto a + I$ der kanonische
    Epimorphismus.

    Wegen $I \subsetneq R$ gibt es laut Satz 11.11 ein maximales Ideal
    $\mm \supseteq I$ von $R$. Weil $\eps$ und $\eps^{-1}$ Inklusionen von
    Idealen erhalten (d.h. monoton bezüglich $\subseteq$ sind), liefert der
    Korrespondenzsatz für Ideale (Satz 12.8), dass $\eps(\mm)$ ein maximales
    Ideal von $R/I$ und auch das einzige solche ist.

(c) Ist jede Nichteinheit nilpotent, so gilt $R \setminus R^\times \subseteq
    \Nil(R)$. Außerdem kann ein nilpotentes Element keine Einheit sein. Wir
    haben $R^\times = \Nil(R)$. Da $\Nil(R)$ laut Aufgabe 1 (a) ein Ideal
    ist, folgt mit Aufgabe 2 (a), dass $R$ lokal ist.

(d) Sei $R$ lokal und $\mathfrak{m} \subsetneq R$ das eindeutige maximale Ideal.
    Dann enthält $\mathfrak{m}$ laut (a) alle Nichteinheiten von $R$, im
    Speziellen alle Nullteiler.

    Sei $e \in R$ idempotent. Wäre $e \notin \set{0, 1}$, so wären wegen
    $$e(e-1) = e^2 - e = e - e = 0$$
    die Elemente $e$ und $e - 1$ Nullteiler, also $e, e - 1 \in \mathfrak{m}$.
    Dies lieferte $1 = e - (e - 1) \in \mathfrak{m}$, im Widerspruch zu
    $\mathfrak{m} \subsetneq R$.

## Aufgabe 3

(a) Es bezeichne $\kappa: \Z[x] \to \F_p[x], f \mapsto \overline{f}$ die
    kanonische Abbildung.

    Für $f = a_0 + a_1x + \cdots + a_nx^n \in \Z[x]$ mit $n \geqslant 0$ und
    $a_0, \dots, a_n \in \Z$ gilt
    $$\begin{aligned}
    f \in \Ker(\kappa) &\iff \overline{a_0} + \overline{a_1}x + \cdots +
    \overline{a_n}x^n = \kappa(f) = \overline{0} \in \F_p[x] \\
    &\iff p \mid a_i \text{ für alle } i \in \set{0, \dots, n} \\
    &\iff f \in p\Z[x],
    \end{aligned}$$
    also $\Ker(\kappa) = p\Z[x]$.

    Ist ferner $\overline{f} = \overline{a_0} + \overline{a_1}x + \cdots +
    \overline{a_n}x^n \in \F_p[x]$, so gilt $\kappa(a_0 + a_1x + \cdots +
    a_nx^n) = \overline{f}$, also $\overline{f} \in \Img(\kappa)$. Somit gilt
    $\F_p[x] \subseteq \Img(\kappa) \subseteq \F_p[x]$, also $\Img(\kappa) =
    \F_p[x]$.

    Nach dem ersten Isomorphiesatz ist dann die von $\kappa$ induzierte
    Abbildung
    $$\overline{\kappa} \colon \Z[x]/p\Z[x] = \Z[x]/\Ker(\kappa)
    \xrightarrow{\;\;\cong\;\;} \Img(\kappa) = \F_p[x]$$
    ein Ringisomorphismus.

(b) Man bemerke zunächst, dass für $a, b \in \Z[x]$ aus $\overline{a} =
    \overline{b} \in \F_p[x]$ folgt, dass $\overline{a} - \overline{b} =
    \overline{0}$, also $a - b \in \Ker(\kappa) = p\Z[x] = \ideal{p}$.
    Umgekehrt folgt aus $a - b \in \ideal{p}$, dass $\overline{a} =
    \overline{b}$.

    Sei $g = a_0 + a_1x + \cdots + a_nx^n \in \Z[x]$ mit $n \geqslant 0$ und
    $a_0, \dots, a_n \in \Z$. Es gilt
    $$\vfi(g) = \vfi(a_0) + \vfi(a_1)\vfi(x) + \cdots + \vfi(a_n)\vfi(x)^n =
    \overline{a_0} + \overline{a_1}x + \cdots + \overline{a_n}x^n +
    \ideal{\overline{f}} = \overline{g} + \ideal{\overline{f}}.$$

    Nun folgt
    $$\begin{aligned}
        g \in \Ker(\vfi) &\iff \overline{g} + \ideal{\overline{f}} = \vfi(g) =
        \overline{0} + \ideal{\overline{f}} = \ideal{\overline{f}} \\
        &\iff \overline{g} \in \ideal{\overline{f}} \\
        &\iff \text{es gibt ein $h \in \Z[x]$ mit } \overline{g} =
        \overline{h}\overline{f} \\
        &\iff \text{es gibt ein $h \in \Z[x]$ mit } g - hf \in \ideal{p} \\
        &\iff \text{es gibt ein $h \in \Z[x]$ und ein $k \in \Z[x]$ mit } g - hf
        = kp \\
        &\iff g \in \ideal{p, f}.
    \end{aligned}$$

(c) Wieder einmal sei $\eps: \Z[x] \twoheadrightarrow \Z[x]/p\Z[x], g \mapsto g
    + p\Z[x]$ der kanonische Epimorphismus. Dann gilt
    $$\begin{aligned}
    \eps(\ideal{p, f}) &= \eps(\ideal{p} + \ideal{f}) = \eps(\ideal{p}) +
    \eps(\ideal{f}) = \eps(p\Z[x]) + \eps(f\Z[x]) \\
    &= \set{0 + p\Z[x]} + \eps(f\Z[x]) = \eps(f) \cdot \eps(\Z[x]/p\Z[x]) \\
    &= (f + p\Z[x]) \cdot (\Z[x]/p\Z[x]) =
    \ideal{f + p\Z[x]} \subseteq \Z[x]/p\Z[x]
    \end{aligned}$$
    sowie
    $$\begin{aligned}
    \overline{\kappa}(\ideal{f + p\Z[x]}) &= \overline{\kappa}((f +
    p\Z[x]) \cdot (\Z[x]/p\Z[x])) =
    \overline{\kappa}(f + p\Z[x]) \cdot \overline{\kappa}(\Z[x]/p\Z[x]) \\
    &= \overline{f} \cdot \F_p[x] = \ideal{\overline{f}} \subseteq \F_p[x].
    \end{aligned}$$

    Laut dem Korrespondenzsatz für Ideale korrespondieren die Ideale $I$ von
    $\Z[x]$ mit $p\Z[x] \subseteq I$ und die Ideale $\eps(I)$ von
    $\Z[x]/p\Z[x]$. Wegen $p\Z[x] = \ideal{p} \subseteq \ideal{p, f}$ gilt also
    $$\ideal{p, f} \text{ maximal in } \Z[x] \qquad \iff \qquad
    \eps(\ideal{p, f}) = \ideal{f + p\Z[x]} \text{ maximal in } \Z[x]/p\Z[x].$$
    Laut Teil (a) ist $\overline{\kappa}$ ein Isomorphismus zwischen
    $\Z[x]/p\Z[x]$ und $\F_p[x]$. Also gilt
    $$\ideal{f + p\Z[x]} \text{ maximal in } \Z[x]/p\Z[x] \qquad \iff \qquad
    \overline{\kappa}(\ideal{f + p\Z[x]}) = \ideal{\overline{f}} \text{ maximal
    in } \F_p[x].$$
    Laut Beispiel 11.13 (b) gilt
    $$\ideal{\overline{f}} \text{ maximal in } \F_p[x] \qquad \iff \qquad
    \overline{f} \text{ irreduzibel in } \F_p[x].$$

(d) Laut Satz 10.16 ist $\Z[x]$ noethersch. Es gibt daher $f_1, \dots, f_s \in
    \Z[x]$ mit $M = \ideal{p, f_1, \dots, f_s}$. Den Berechnungen in (c)
    folgend haben wir
    $$\eps(M) = \ideal{f_1 + p\Z[x], \dots, f_s + p\Z[x]} \subseteq
    \Z[x]/p\Z[x]$$
    und
    $$\overline{\kappa}(\eps(M)) = \ideal{\overline{f_1}, \dots, \overline{f_s}}
    \subseteq \F_p[x].$$
    Da $\F_p[x]$ ein Hauptidealbereich ist, gibt es ein $f \in \Z[x]$ mit
    $$\overline{\kappa}(\eps(M)) = \ideal{\overline{f_1}, \dots, \overline{f_s}}
    = \ideal{\overline{f}}.$$
    Wegen der Bijektivität von $\overline{\kappa}$ gilt
    $$\eps(M) = \overline{\kappa}^{-1}(\overline{\kappa}(\eps(M))) =
    \overline{\kappa}^{-1}\left(\ideal{\overline{f}}\right) = \ideal{f + p\Z[x]}
    \subseteq \Z[x]/p\Z[x]$$
    und der Korrespondenzsatz liefert
    $$M = \eps^{-1}(\eps(M)) = \eps^{-1}(\ideal{f + p\Z[x]}) = \ideal{p, f}
    \subseteq \Z[x].$$

(e) In dem faktoriellen Ring $\F_p[x]$ sind die Primelemente und die
    irreduziblen Elemente genau dieselben.

    Seien $p_1, \dots, p_s \in \F_p[x]$ irreduzible Polynome (solche gibt es,
    zum Beispiel $x$). Sei $p$ ein Primfaktor von $f = p_1 \cdots p_s + 1$
    (einen solchen gibt es, denn $f \notin (\F_p[x])^\times = \F_p^\times$).
    Dann ist $p \notin \set{p_1, \dots, p_s}$, denn sonst gälte $p \mid 1$,
    also $p \in \F_p^\times$, im Widerspruch zur Primalität von $p$.

## Aufgabe 4

Setze $I = \ideal{xy - z^2} \subseteq K[x, y, z]$.

(a) Es gilt
    $$\begin{aligned}
    \ideal{\overline{x}, \overline{z}} &= \set{(a+I)(x+I) + (b+I)(z+I) \mid a,
    b \in K[x, y, z]} \\
    &= \set{ax+bz+I \mid a, b \in K[x, y, z]} = \set{a+I \mid a \in \ideal{x,
    z}} = \ideal{x, z}/I.
    \end{aligned}$$
    Da
    $$R/\ideal{\overline{x}, \overline{z}} = (K[x, y, z]/I) \; / \; (\ideal{x,
    z}/I) \cong K[x, y, z]/\ideal{x, z} \cong K[y]$$
    ein Integritätsbereich ist, ist $\ideal{\overline{x}, \overline{z}}$ laut
    Satz 12.18 ein Primideal.

(b) In einem faktoriellen Ring existiert zu je zwei Ringelementen ein
    eindeutiger größter gemeinsamer Teiler. Das ist hier nicht der Fall:

    Betrachte $a = \overline{x}\overline{y} = \overline{z}^2$ und $b =
    \overline{x}\overline{z}$. Offenbar gilt $\overline{x} \mid a$ und
    $\overline{x} \mid b$ sowie $\overline{z} \mid a$ und $\overline{z} \mid b$.
    Jedoch sind $\overline{x}$ und $\overline{z}$ nicht assoziiert, da $x - z
    \notin I$.

    Die Elemente $a$ und $b$ sind zwei verschiedene kleinste gemeinsame
    Vielfache von $\overline{x}$ und $\overline{z}$, somit kann es keinen
    größten gemeinsamen Teiler von $a$ und $b$ geben.

## Aufgabe 5

Aufgrund der Surjektivität von $\vfi$ gilt für $m \in \N$:
$$\Img(\vfi^m) = \vfi^m(R) = \vfi^{m-1}(\vfi(R)) = \vfi^{m-1}(R) = \dots =
\vfi(R) = R.$$

Wir haben die Kette von Idealen
$$\Ker(\vfi) \subseteq \Ker(\vfi^2) \subseteq \Ker(\vfi^3) \subseteq \cdots,$$
für die es, da $R$ noethersch ist, ein $m \in \N_+$ gibt mit
$$\Ker(\vfi^m) = \Ker(\vfi^{m+1}) = \cdots.$$
Nun zeigen wir noch $\Ker(\vfi^m) \subseteq \set{0}$. Dann gilt $\set{0}
\subseteq \Ker(\vfi) \subseteq \Ker(\vfi^m) \subseteq \set{0}$, also $\Ker(\vfi)
= \set{0}$, und $\vfi$ ist injektiv.

Sei $a \in \Ker(\vfi^m) \subseteq R$. Wegen $a \in R = \Img(\vfi^m)$ gibt es ein
$b \in R$ mit $\vfi^m(b) = a$. Es gilt
$$\vfi^{2m}(b) = \vfi^m(\vfi^m(b)) = \vfi^m(a) = 0,$$
also $b \in \Ker(\vfi^{2m}) = \Ker(\vfi^m)$, und daher
$$a = \vfi^m(b) = 0.$$

\newpage

## Aufgabe 6

\paragraph{Lemma 1:} Ist $t = x_0^{\alpha_0} \cdots x_n^{\alpha_n}$ ein Term in
$P$, so gibt es ein $\alpha \in \N$ mit $t + I = \overline{x_n}^\alpha$.
\paragraph{Beweis.} Per Induktion nach $n$.

Induktionsanfang: $n = 0$. Verwende $\alpha = \alpha_0$ und erhalte
$\overline{t} = \overline{x_0}^{\alpha_0} = \overline{x_n}^\alpha$.

Induktionsschritt: $n > 0$. Laut Induktionsvoraussetzung gibt es ein $\alpha'
\in \N$ mit
$$\overline{t'} = \overline{x_0^{\alpha_0} \cdots
x_{n-1}^{\alpha_{n-1}}} = \overline{x_{n-1}}^{\alpha'}.$$
Nun gilt
$$\overline{t} = \overline{t'} \cdot \overline{x_n}^{\alpha_n} =
\overline{x_{n-1}}^{\alpha'} \cdot \overline{x_n}^{\alpha_n}
= \overline{x_n^2}^{\alpha'} \cdot \overline{x_n}^{\alpha_n}
= \overline{x_n}^{2\alpha' + \alpha_n}$$
und wir haben das gewünschte $\alpha = 2\alpha' + \alpha_n$ gefunden. \qed

\paragraph{Lemma 2:} Seien $n, m \in \N$ mit $m \geqslant n$. Dann gilt
$$\overline{x_n} = \overline{x_m}^{2^{m-n}}.$$
\paragraph{Beweis.} Per Induktion nach $m - n$.

Induktionsanfang: $m - n = 0$. Es gilt $m = n$, also
$$\overline{x_n} = \overline{x_m} = \overline{x_m}^{2^{m-n}}.$$
Induktionsschritt: $m - n > 0$. Laut Induktionsvoraussetzung gilt
$$\overline{x_n} = \overline{x_{m-1}}^{2^{m-n-1}}.$$
Wegen $\overline{x_{m-1}} = \overline{x_m^2}$ gilt
$$\overline{x_n} = \overline{x_{m-1}^2}^{2^{m-n-1}} = \overline{x_m}^{2 \cdot
2^{m-n-1}} = \overline{x_m}^{2^{m-n}}.$$ \qed

\paragraph{Korollar:} Für jedes $r \in \mm$ gibt es ein $a \in K$ und $k, n \in
\N$ mit $r = a \cdot \overline{x_n}^k$, denn die Terme von $r$ lassen sich
laut Lemma 1 auf eine Unbestimmte vereinfachen, und diese Unbestimmten können
laut Lemma 2 auf eine Unbestimmte zurückgeführt werden.

\paragraph{Lemma 3:} $K + \mm = R$.
\paragraph{Beweis.} Die Inklusion $K + \mm \subseteq R$ ist klar. Nun sei $f + I
\in R$ für ein $f \in P$. Dann gibt es ein $n \in \N$, sodass
$$f = \sum_{\alpha = (\alpha_0, \dots, \alpha_n) \in \N^{n+1}} c_\alpha
x_0^{\alpha_0} \cdots x_n^{\alpha_n}
= c_{(0, \dots, 0)} + \sum_{\substack{\alpha = (\alpha_0, \dots, \alpha_n) \\
\in \N^{n+1} \setminus \set{(0, \dots, 0)}}} c_\alpha x_0^{\alpha_0} \cdots
x_n^{\alpha_n},$$
mit $c_\alpha \in K$ für alle $\alpha \in \N^{n+1}$. Wegen $c_\alpha
x_0^{\alpha_0} \cdots x_n^{\alpha_n} + I \in \mm$ für alle $\alpha = (\alpha_0,
\dots, \alpha_n) \in \N^{n+1} \setminus \set{(0, \dots, 0)}$ ist
$$f + I = \underbrace{c_{(0, \dots, 0)}}_{\in K} +
\underbrace{\sum_{\substack{\alpha = (\alpha_0, \dots, \alpha_n) \\ \in
\N^{n+1} \setminus \set{(0, \dots, 0)}}} \underbrace{(c_\alpha x_0^{\alpha_0} \cdots
x_n^{\alpha_n} + I)}_{\in \mm}}_{\in \mm} \in K + \mm.$$
Dies zeigt $R \subseteq K + \mm$. \qed

\newpage

(a) Laut dem Korollar genügt es zu zeigen, dass für $n \in \N$ das Element
    $\overline{x_n} \in \mm$ nilpotent ist.

    Per Induktion nach $n$:

    \paragraph{Induktionsanfang:} $n = 0$. Es ist $x_0^2 \in I$, also
    $(\overline{x_0})^2 = \overline{x_0^2} = x_0^2 + I = 0 + I = \overline{0}$.

    \paragraph{Induktionsschritt:} $n > 0$. Es gilt $x_n^2 - x_{n-1} \in I$,
    also $(\overline{x_n})^2 = \overline{x_{n-1}}$. Da $\overline{x_{n-1}}$ nach
    Induktionsvoraussetzung nilpotent ist, gibt es ein $m \in \N$ mit
    $(\overline{x_{n-1}})^m = \overline{0}$. Also folgt $(\overline{x_n})^{2m} =
    (\overline{x_{n-1}})^m = \overline{0}$.

(b) Betrachte $\vfi: K \to R/\mm = (P/I)/\mm, \; a \mapsto (a + I) + \mm$, die
    Komposition der Homomorphismen
    $$K \xhookrightarrow{\;\iota\;} P \xrightarrowdbl{\eps_1} P/I = R
    \xrightarrowdbl{\eps_2} R/\mm$$
    mit der Inklusionsabbildung $\iota$ und den kanonischen Epimorphismen
    $\eps_1, \eps_2$.

    Für die Injektivität seien $a, b \in K$ mit
    $$(a + I) + \mm = \vfi(a) = \vfi(b) = (b + I) + \mm.$$
    Dann gilt $m \coloneqq (a - b + I) = (a + I) - (b + I) \in \mm$.
    Schreibe
    $$m = \sum_{i=0}^{n} (r_i + I)(x_i + I) = \left(\sum_{i=0}^{n} r_ix_i\right)
    + I$$
    mit $n \in \N$ und $(r_0 + I), \dots, (r_n + I) \in R$. Nun ist
    $$a - b + I = m = \left(\sum_{i=0}^{n} r_ix_i\right) + I,$$
    also $a - b - \left(\sum_{i=0}^{n} r_ix_i\right) \in I$. Also gibt es ein $f
    \in I$ mit
    $$a - b - \left(\sum_{i=0}^{n} r_ix_i\right) = f,$$
    oder anders ausgedrückt:
    $$a - b = f + \left(\sum_{i=0}^{n} r_ix_i\right).$$
    Jetzt kann man Koeffizienten vergleichen: Da der konstante Koeffizient der
    rechten Seite dieser Gleichung verschwindet, gilt $a - b = 0$, wie
    gewünscht.

    Für die Surjektivität sei $f \in P$. Schreibe
    $$f = \sum_{\alpha = (\alpha_1, \dots, \alpha_n) \in \N^n}
    c_{\alpha}x_1^{\alpha_1} \cdots x_n^{\alpha_n}$$
    mit $n \in \N$ und $c_\alpha \in K$ für alle $\alpha \in \N^n$. Dann gilt
    mit $\alpha_0 \coloneqq (0, \dots, 0) \in \N^n$:
    $$\begin{aligned}
    (f + I) + \mm &= \left(\left(\sum_{\alpha = (\alpha_1, \dots, \alpha_n) \in
    \N^n} c_\alpha x_1^{\alpha_1} \cdots x_n^{\alpha_n}\right) + I\right) + \mm
    \\
    &= \left(\sum_{\alpha = (\alpha_1, \dots, \alpha_n) \in \N^n}
    (c_\alpha + I) (x_1 + I)^{\alpha_1} \cdots (x_n + I)^{\alpha_n}\right)
    + \mm \\
    &= ((c_{\alpha_0} + I) + \mm) +
    \left(\underbrace{\left(\sum_{\substack{\alpha =
    (\alpha_1, \dots, \alpha_n) \\ \in \N^n \setminus \set{\alpha_0}}}
    (c_\alpha + I) \underbrace{(\overline{x_0})^{\alpha_1} \cdots
    (\overline{x_n})^{\alpha_n}}_{\in \mm}\right)}_{\in \mm} + \mm\right) \\
    &= ((c_{\alpha_0} + I) + \mm) + ((0 + I) + \mm) \\
    &= (c_{\alpha_0} + I) + \mm.
    \end{aligned}$$
    Also gilt $\vfi(c_{\alpha_0}) = (c_{\alpha_0} + I) + \mm = (f + I) + \mm.$
    Damit ist gezeigt, dass $\vfi$ ein Isomorphismus ist.

    Laut Satz 12.18 (b) ist damit $\mm$ maximal, also ein Primideal. Sei $\pp
    \subsetneq R$ ein Primideal von $R$. Ist $r \in \mm \nozero$, so ist gibt es
    laut (a) ein $k \in \N_+$ mit
    $$r \cdot r^{k-1} = r^k = 0 + I \in \pp,$$
    also $r \in \pp$. Dies zeigt $\mm \subseteq \pp$ und die Maximalität von
    $\mm$ liefert $\mm = \pp$. Somit ist $\mm$ das einzige Primideal von $R$.

(c) Laut Lemma 3 lässt sich jedes Element von $R$ darstellen als $a + x$
    mit $a \in K$ und $x \in \mm$. Ist $a = 0$, so ist $a + x = x$ nilpotent und
    damit keine Einheit. Ist $a \ne 0$, so ist $a + I$ wegen $(a + I)(a^{-1} +
    I) = 1 + I$ eine Einheit, und damit auch $a + x$ als Summe einer Einheit und
    eines nilpotenten Elements.

(d) Laut Lemma 3 lässt sich $r$ schreiben als $r = a + x$ mit $a \in K$ und $x
    \in \mm$. Ist $a = 0$, so ist $r \in \mm$ und daher nach dem obigen Korollar
    darstellbar als $r = \overline{1} \cdot \overline{x_n}^k$ mit $n, k \in \N$.

    Ist $a \ne 0$, so ist $r$ bereits eine Einheit (laut (c)) und wir sind
    fertig.

(e) Schreibe $r = u \cdot \overline{x_n}^k$ und $s = v \cdot \overline{x_m}^l$
    mit $u, v \in R^\times$ und $k, l, n, m \in \N$. Ohne Einschränkung gelte
    $m \geqslant n$. Dann ist
    $$\overline{x_n}^k = \overline{x_m^{2^{m-n}}}^k = \overline{x_m}^{k \cdot
    2^{m-n}}.$$
    Gilt $l \geqslant k \cdot 2^{m-n}$, so ist
    $$r = u \cdot \overline{x_n}^k = u \cdot \overline{x_m}^{k \cdot 2^{m-n}}
    = uv^{-1} \overline{x_m}^{l - k \cdot 2^{m-n}} \cdot \underbrace{v
    \cdot \overline{x_m}^l}_{= s} \in \ideal{s},$$
    also $\ideal{r} \subseteq \ideal{s}$.
    Gilt $l < k \cdot 2^{m-n}$, so ist
    $$s = v \cdot \overline{x_n}^l
    = vu^{-1} \overline{x_m}^{k \cdot 2^{m-n} - l} \cdot \underbrace{u
    \cdot \overline{x_m}^{k \cdot 2^{m-n}}}_{= r} \in \ideal{r},$$
    also $\ideal{s} \subseteq \ideal{r}$.
