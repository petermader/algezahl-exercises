# Algebra und Zahlentheorie II -- Übungsblatt 7

## Aufgabe 4

(a) Es ist
    $$[K : \Q] = [K : \Q(\sqrt{2}, \sqrt{3})] \cdot [\Q(\sqrt{2}, \sqrt{3}) :
    \Q] = 2 \cdot [\Q(\sqrt{2}, \sqrt{3}) : \Q(\sqrt{2})] \cdot [\Q(\sqrt{2}) :
    \Q] = 8.$$

(b) Die $\Q$-Automorphismen $\tau_{\sqrt{2}}, \tau_{\sqrt{3}}, \tau_{\sqrt{5}} :
    K \to K$ mit $\tau_a(a) = -a$ und $\tau_a(b) = b$ für $a \in \set{2, 3, 5}$
    und $b \in \set{2, 3, 5} \setminus \set{a}$ erzeugen eine Untergruppe $U
    \cong C_2 \oplus C_2 \oplus C_2$ von $\Gal(K / \Q)$. Mit (a) und Satz 32.2
    erhalten wir, dass $K / \Q$ galoissch ist.

(c) Mit Theorem 32.16 und (b) entsprechen die Zwischenkörper den Untergruppen
    von $G = C_2 \oplus C_2 \oplus C_2$. Das sind $\set{0}, \ideal{e_1},
    \ideal{e_2}, \ideal{e_3}, \ideal{e_1, e_2}, \ideal{e_1, e_3}, \ideal{e_2,
    e_3}$ und $G$. Das sind acht Stück. Wegen der Abelianität von $G$ sind alle
    Untergruppen normal und nach Satz 32.18 auch alle Zwischenkörper.

## Aufgabe 5

Der Zerfällungskörper von $f = x^7-1$ wird von einer der sechs primitiven
siebten Einheitswurzeln erzeugt. Diese werde mit $\zeta$ bezeichnet.
Bekanntermaßen erzeugt dann der $\Q$-Automorphismus $\vfi$ mit $\vfi(\zeta) \to
\zeta^2$ die abelsche Galois-Gruppe $\ideal{\vfi} = \Gal(f) \cong C_6$.

Der Zerfällungskörper von $g = x^4(x^3+3)$ ist gleich dem Körper $L$ aus
Beispiel 32.3. Daher gilt $\Gal(L / \Q) = \Gal(f) \cong S_3$.

## Aufgabe 6

(a) Es ist $a^2 = 2+\sqrt{2}$ und $a^4=4+4\sqrt{2}+2=6+4+\sqrt{2}$. Daher gilt
    $a^4-4a^2+2=0$. Das Eisensteinkriterium mit $p=2$ liefert die
    Irreduzibilität des primitiven Polynoms $f = x^4-4x^2+2$, das damit das
    Minimalpolynom von $a$ über $\Q$ ist.

(b) Es gilt $f = (x-a)(x+a)(x-b)(x+b)$ mit $b = a^3-3a$. Daher ist $\Q(a)$ der
    Zerfällungskörper von $f$ und $\Q(a) / \Q$ nach Korollar 31.10 endlich und
    normal. Wegen $\mathrm{char}(\Q) = 0$ ist $\Q(a) / \Q$ auch separabel
    algebraisch und somit galoissch.

    Wegen $\deg(\mu_a) = \deg(f) = 4$ ist $\abs{\Gal(\Q(a) / \Q)} = [\Q(a) : \Q]
    = 4$. Betrachte den $\Q$-Automorphismus $\vfi$ von $\Q(a)$ mit $\vfi(a) =
    b$. Dann gilt $\vfi^2(a) = -a$, $\vfi^3(a) = -b$ und $\vfi^4(a) = a$. Damit
    hat $\vfi$ Ordnung 4 und $\Gal(\Q(a) / \Q) = \ideal{\vfi} \cong C_4$.

## Aufgabe 9

(a) Über $\F_2$ besitzt $f$ keine Nullstelle. Für das einzige quadratische über
    $\F_2$ irreduzible Polynom $g=x^2+x+1$ gilt $g^2 = x^4+x^2+1 \ne f$. Das
    Reduktionskriterium mit $p=2$ liefert die Irreduzibilität von $f$ über $\Q$.

(b) Es gilt $(-a^3+a+1)(1+a^2) = -a^3+a+1-a^5+a^3+a^2 = -a^5+a^2+a+1 =
    -a(a^4-a-1)+1 = 1$ und daher $-a^3+a+1 = (1+a^2)^{-1} = b$.

(c) Betrachte $h = x^4-x^3+4*x^2-4x+1$. Es gilt $h(b) = 0$. Analog zu (a) sieht
    die Irreduzibilität von $h$ über $\Q$. Daher gilt $\mu_b = h$.

## Aufgabe 10

Es sei $C_{2n+1}$ respektive $C_{4n+2}$ die multiplikative Gruppe der
$(2n+1)$-ten respektive $(4n+2)$-ten Einheitswurzeln.

Offenbar ist $\beta$ eine $(4n+2)$-te Einheitswurzel:
$$\beta^{4n+2} = (-\alpha^2)^{4n+2} = (-1)^{2(2n+1)}\alpha^{4(2n+1)}
= (\alpha^{2n+1})^4 = 1.$$
Sei $k \in \set{1, \dots, 4n+2}$ mit $\beta^k = 1$. Wir wollen $k=4n+2$ zeigen.
Wir haben
$$1=\beta^k = (-\alpha^2)^k = (-1)^k \alpha^{2k}.$$
Wir unterscheiden zwei Fälle:

* Ist $k$ gerade, so folgt $1 = \alpha^{2k}$ und daher $2n+1 =
  \ord_{C_{2n+1}}(\alpha) \mid 2k$. Wegen $\ggT(2k, 2n+1)$ gilt $2n+1 \mid k$,
  also $k \in \set{2n+1, 4n+2}$. Weil $k$ gerade ist, haben wir $k = 4n+2$.
* Ist $k$ ungerade, so folgt $\alpha^{2k} = -1$ und $\alpha^{4k} = 1$. Analog
  folgt $2n+1 \mid 4k$ und $2n+1 \mid k$, im Widerspruch zu $k \leqslant 4n+2 <
  4 \cdot (2n+1)$. Dieser Fall ist nicht möglich.

Offenbar gilt $\Q(\beta) \subseteq \Q(\alpha)$. Wegen $\alpha \in C_{2n+1}
\subseteq C_{4n+2} = \ideal{\beta}$ gilt $\alpha = \beta^m$ für ein $m \in \N$
und $\alpha \in \Q(\beta)$. Dies zeigt $\Q(\alpha) \subseteq \Q(\beta)$.
