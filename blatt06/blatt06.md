# Algebra und Zahlentheorie II -- Übungsblatt 6

## Aufgabe 1

(a) Schreibe $a = \sqrt[3]{2}$, $b = \sqrt[4]{5}$, $\mu_a =
    (x-a)(x-a\zeta_3)(x-a\zeta_3^2)$ und $\mu_b =
    (x-b)(x-b\zeta_4)(x-b\zeta_4^2)(x-b\zeta_4^3)$, wobei $\zeta_3 = \exp\left(
    \frac{2\pi\ii}{3} \right)$ und $\zeta_4 = \ii$ die dritte respektive vierte
    Einheitswurzel bezeichne.

    Man rechnet nach, dass $\frac{a-a\zeta_3^i}{b-b\zeta_4^j} = \frac{a}{b}
    \cdot \frac{1-\zeta_3^i}{1-\zeta_4^j} \notin \Q$ für alle
    $i \in \set{1, 2}$ und alle $j \in \set{1, 2, 3}$ gilt. Dann zeigt der
    Beweis von Satz 30.20, dass $a+b$ primitiv ist.

(b) Ist $a \in \C$ eine Nullstelle von $f = x^4-2x^2+2$, so sind auch $-a,
    \overline{a}$ und $-\overline{a}$ Nullstellen von $f$ und es gilt
    $K = \Q(a, -a, \overline{a}, -\overline{a}) = \Q(a, \overline{a})$.

    Dann könnte man wieder nachrechnen, dass $a+3\overline{a}$ die Bedingung an
    primitive Elemente aus Satz 30.20 erfüllt.


## Aufgabe 2

Sei $a \in K(u, v) = K[u, v]$. Dann gilt $a = f(u, v)$
für ein $f = \sum_{t \in \supp(f)} c_t t \in K[x, y]$ mit $c_t \in K$ für $t \in
\supp(f)$. Wegen
$$a^p = \left( \sum_{t=x^iy^j \in \supp(f)} c_t u^iv^j \right)^p =
\sum_{t=x^iy^j \in \supp(f)} c_t^p(u^p)^i(v^p)^i \in K$$
ist $\deg(\mu_a) \leqslant p$ und daher $K(a) \subsetneq K(u, v)$.


## Aufgabe 3

Wenn das folgende richtig ist, gibt es nicht nur im Allgemeinen keinen solchen
Isomorphismus, sondern nie.

Sei $z_1 \in L_1$ eine Nullstelle von $f_1$ in $L_1$. Dann ist $f_1 =
(x-z_1)(x+z_1) = (x+z_1)^2$ und $L_1 = K(z_1) = K \oplus z_1 K$. Ferner
sei $z_2 \in L_2$ eine Nullstelle von $f_2$ in $L_2$. Angenommen, es gäbe einen
solchen $K$-Isomorphismus $\vfi: L_1 \to L_2$. Dann
wäre auch $\vfi^{-1}$ ein $K$-Isomorphismus. Wir hätten $\vfi^{-1}(z_2) \in L_1
= K \oplus z_1 K$, also $\vfi^{-1}(z_2) = b + z_1c$ mit $b, c \in K$, und
daher $$\vfi^{-1}(z_2) = \vfi^{-1}\left(z_2^2 + a_2\right) =
\left(\vfi^{-1}(z_2)\right)^2 + a_2 = (b+z_1c)^2 + a_2 = b^2 + z_1^2c^2 + a_2 =
b^2 + a_1c^2 + a_2 \in K.$$
Somit wäre $z_2 = \vfi\left(\vfi^{-1}(z_2)\right) \in K$ und $f_2$ nicht
irreduzibel.


## Aufgabe 4

(a) Für $h_1, h_2 \in I$ ist $h_1(f, g) \cdot h_2(f, g) = (h_1 \cdot h_2)(f, g)
    = 0$, schließlich ist der Substitutionshomomorphismus ein Homomorphismus.
    Daher gilt $h_1(f, g) = 0$ oder $h_2(f, g) = 0$, also $h_1 \in I$ oder $h_2
    \in I$.

(b) Wir wollen die Äquivalenz
    $$I \text{ maximal } \qquad\iff\qquad (f, g) \in K^2$$
    zeigen.

    Sei $I$ maximal. Nach Korollar 28.10 (a) ist $I \cap K[x]$ ein Ideal in
    $K[x]$ ungleich $\ideal{0}$, also $I \cap K[x] = \ideal{a}$ für ein $a \in
    K[x] \nozero$ und es gilt $a(f) = 0$. Wäre $\deg(a) = 0$, so hätte $a$ keine
    Nullstelle, es ist also $\deg(a) > 0$. Wäre $\deg(f) > 0$, so hätten wir
    $\deg(a(f)) = \deg(a)\deg(f) > 0$, also $a(f) \ne 0$. Somit folgt $f \in K$.
    Analog zeigt man $g \in K$.

    Umgekehrt gelte $(f, g) \in K^2$. Offensichtlich gilt $1 \notin I$ und wir
    haben $\ideal{x-f, y-b} \subseteq I \subsetneq K[x, y]$. Wegen
    $$K[x, y] / \ideal{x - f, y - g} \cong K[x, y] / \ideal{x, y} \cong K$$
    ist $\ideal{x - f, y - g}$ maximal und wir erhalten $I = \ideal{x-f, y-b}$.

## Aufgabe 5

Das Polynom $f$ besitze nur eine Nullstelle. Damit ist $f$ inseparabel und laut
Satz 30.11 von der Form $f = g\left( x^{p^e} \right)$ mit $e \in \N_+$ und $g
\in K[x]$ irreduzibel und separabel. Wäre $\deg(g) > 1$, so hätte $g$ mehr als
eine Nullstelle in $\overline{K}$ und damit auch $f$. Weil $f$ normiert ist,
gilt $g = x - a$ für ein $a \in K$. Wäre $a \in K^p$, also $a = b^p$ für ein $b
\in K$, so wäre
$$f = x^{p^e} - a = \left( x^{(p^{e-1})p} - b^p \right) = \left(
x^{p^{e-1}} - b \right)^p$$
nicht irreduzibel.
