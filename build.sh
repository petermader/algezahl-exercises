#!/usr/bin/env sh
[ -d "build" ] || mkdir build

out="blatt$1/blatt$1.pdf"
out_pm="blatt$1/mader_peter_$1.pdf"
pandoc -H ~/studium/cs-lecture-notes/include/preamble.tex -B include/header.tex blatt$1/*.md -o $out
cp "$out" "$out_pm"
