# Algebra und Zahlentheorie II -- Übungsblatt 4

## Aufgabe 1

(a) Falsch: Sei $K \subseteq \Q$ ein Teilkörper. Der Primkörper von $K$ ist
    $\Q$, also ist $\Q \subseteq K \subseteq \Q$ und damit $K = \Q$.

(b) Falsch: Wegen $(1, 0) \cdot (0, 1) = (0, 0) \in \R^2$ ist $\R^2$ kein
    Integritätsbereich und schon gar kein Körper.

(c) Falsch: Wäre für $(i, j) = (2, 3)$ ein Teilkörper von $L$ isomorph zu $K$,
    so wäre $L$ ein Vektorraum über $K$. Dann wäre $\abs{L} = p^3$ eine Potenz
    von $\abs{K} = p^2$.

(d) Falsch: Da 13 weder Fermatsche Primzahl noch eine Zweierpotenz als Teiler
    besitzt, ist laut dem Fazit in Bemerkung 26.22 das reguläre 13-Eck und damit
    auch $\zeta_{13}$ nicht mit Zirkel und Lineal konstruierbar.


## Aufgabe 2

\paragraph{Lemma:} Sind 0 und 1 und ein Winkel $\gamma$ gegeben, so ist
$\cos(\gamma)$ konstruierbar. Umgekehrt kann man aus $\set{0, 1, \cos(\gamma)}$
den Winkel $\gamma$ konstruieren.
\vspace{-.5cm}
\paragraph{Beweis.} Ohne Einschränkung gelte $\beta =
\sphericalangle{}1\,0\,P$ für einen Punkt $P$. Einer der Schnittpunkte des
Einheitskreises mit der Gerade $0P$ heiße $Q$. Ist $R$ der Lotfußpunkt des Lotes
von $Q$ auf die reelle Achse, so gilt $\abs{\cos(\gamma)} =
\abs{\overline{0R}} = \abs{R}$, denn $[0R]$ ist Ankathete des rechtwinkligen
Dreiecks $\triangle{}0RQ$ oder $\triangle{}0QR$ (je nach Wahl von $Q$),
dessen Hypothenuse die Länge 1 besitzt.

Ist umgekehrt $\cos(\gamma)$ gegeben, so kann man auf der reellen Achse das Lot
über $\cos(\gamma)$ errichten und mit dem Einheitskreis in einem Punkt $P$
schneiden. Somit ist der Winkel $\beta = \sphericalangle{}1\,0\,P$ oder $\beta =
\sphericalangle{}P\,0\,1$ (je nach Wahl von $P$) konstruiert. \qed

---

Es gelte $\alpha = \sphericalangle{}A\,B\,C$ mit Punkten $A, B, C$. Wir setzen
willkürlich $0 = B$ und $1 = A$.

Schreibe $\beta \coloneqq \frac{\alpha}{3}$. Die Rechnung in Beispiel 26.19
zeigt $\cos(\alpha) = 4(\cos(\beta))^3 - 3\cos(\beta)$. Das bedeutet, dass
$\cos(\beta)$ eine Nullstelle von $f \coloneqq 4x^3 - 3x - \cos(\alpha) \in
\Q(\cos(\alpha))[x]$ ist, und dass $\cos(\alpha) \in \Q(\cos(\beta))$ und daher
$\Q(\cos(\alpha), \cos(\beta)) = \Q(\cos(\beta))$ gilt. Aus Letzterem folgern
wir mit der Gradformel:
$$\begin{aligned}
[\Q(\cos(\alpha), \cos(\beta)) : \Q] &=
[\Q(\cos(\alpha), \cos(\beta)) : \Q(\cos(\beta))] \cdot [\Q(\cos(\beta)) : \Q]
\\
&= [\Q(\cos(\beta)) : \Q(\cos(\beta))] \cdot [\Q(\cos(\beta)): \Q] =
[\Q(\cos(\beta)) : \Q].
\end{aligned}$$
Da $\cos(\beta)$ eine Nullstelle von $f$ ist, gilt $[\Q(\cos(\beta)) : \Q] =
[\Q(\cos(\alpha))(\cos(\beta)) : \Q(\cos(\alpha))] \leqslant \deg(f) = 3$.
Demnach ist als erste Äquivalenz festzuhalten:
$$\begin{aligned}
f \text{ reduzibel über } \Q(\cos(\alpha)) &\iff f \text{ ist nicht das
Minimalpolynom von $\cos(\beta)$ über } \Q(\cos(\alpha)) \\
&\iff [\Q(\cos(\alpha))(\cos(\beta)) : \Q(\cos(\alpha))] < \deg(f) = 3 \iff
[\Q(\cos(\beta)) : \Q] < 3.
\end{aligned}$$
Die letzte Aussage ist nach Satz 26.16 äquivalent zur Konstruierbarkeit von
$\cos(\beta)$ aus $\set{0, 1}$. Die wiederum ist laut Lemma äquivalent zur
Dreiteilbarkeit von $\alpha = 3\beta$.


## Aufgabe 3

Wieder kann man das Fazit aus Bemerkung 26.22 bemühen: Wäre das reguläre
Siebeneck mit Zirkel und Lineal konstruierbar, so müsste 7 eine Zweierpotenz
oder eine Fermatsche Primzahl als Teiler besitzen. Das ist nicht der Fall.


## Aufgabe 4

Hier gilt wieder das alte _If I had more time, I would have written a shorter
letter._

(a) Um Formulierungsklarheit zu schaffen, soll die Formulierung der
    Strahlensätze auf
    [\underline{Wikipedia}](https://de.wikipedia.org/wiki/Strahlensatz).
    Der Schnittpunkt von $A'B'$ und $CC'$ heiße $X$, den von $AA'$ und $BB'$
    heiße $Y$.

    Seien $AB$ und $A'B'$ parallel. Dann kann man den dritten Strahlensatz auf
    die V-Figur mit Zentrum $C$ und den drei Geraden $CA$, $CC'$ und $CB$
    anwenden und $\frac{\;\overline{AC'}\;}{\overline{B'X}} =
    \frac{\;\overline{C'B}\;}{\overline{A'X}}$. Ferner kann man den dritten
    Strahlensatz auf die X-Figur mit Zentrum $Y$ und den Geraden $AA'$, $BB'$
    und $XC'$ anwenden und $\frac{\;\overline{AC'}\;}{\overline{A'X}} =
    \frac{\;\overline{C'B}\;}{\overline{B'X}}$ erhalten. Insgesamt haben wir
    $$\frac{\overline{AC'}^2}{\overline{B'X} \cdot \overline{A'X}} =
    \frac{\overline{C'B}^2}{\overline{A'X} \cdot \overline{B'X}}$$
    und daher $AC' = C'B$.

    Die Rückrichtung wird wohl ähnlich gehen.

(b) Zunächst zeigen wir, dass $M$ um 1 auf der imaginären Achse nach oben
    verschoben werden kann (Konstruktion von $\ii + M$) und dass das Haus um
    90\textdegree{} im Uhrzeigersinn gedreht werden kann (Konstruktion von
    $\frac{3}{2} + \frac{\ii}{2}$).

    (1) Konstruiere die Gerade $\ii\R$ durch 0 und $\ii$ und die Gerade $1 +
        \ii\R$ durch 1 und $1+\ii$.
    (2) Die Gerade durch $\ii$ und $\frac{1}{2} + \frac{3}{2}\ii$ schneidet $1 +
        \ii\R$ im Punkt $1 + 2\ii$. Die Gerade durch $1 + \ii$ und $\frac{1}{2}
        + \frac{3}{2}\ii$ schneidet $\ii\R$ im Punkt $2\ii$.
    (3) Die Gerade durch $0$ und $\frac{1}{2} + \frac{3}{2}\ii$ schneidet $1 +
        \ii\R$ im Punkt $1 + 3\ii$. Die Gerade durch $1$ und $\frac{1}{2}
        + \frac{3}{2}\ii$ schneidet $\ii\R$ im Punkt $3\ii$.
    (4) Die Gerade durch $2\ii$ und $1+3\ii$ schneidet die Gerade durch $3\ii$
        und $1+2\ii$ im Punkt $\frac{1}{2} + \frac{5}{2}\ii$.

    Auf diese Weise kann $\ii\N + M$ (das unendliche hohe Hochhaus des Nikolaus)
    konstruiert werden.

    (5) Die Gerade durch $5\ii$ und $\frac{1}{2} + \frac{7}{2}\ii$ schneidet die
        Gerade durch $2\ii$ und $1+\ii$ im Punkt $\frac{3}{2} + \frac{\ii}{2}$.

    Auf diese Weise kann man $\Z[i] + M = \Z[i] \cup \left(\Z[i] + \frac{1}{2} +
    \frac{\ii}{2} \right)$ konstruieren und so das Haus des
    Nikolaus in alle Richtungen erweitern.

    \paragraph{Projektion auf Koordinatenachsen:} Für gegebenes $A' = a+b\ii \in
    \C$ mit $a, b \in \R$ und $b > 0$ soll $a$ konstruiert werden. Der Fall
    $b < 0$ und die Projektion auf die imaginäre Achse verlaufen analog.

    (1) Setze $A \coloneqq \left\lceil a+1 \right\rceil$ und $B \eqqcolon A +
        2\ii \cdot \left\lceil b \right\rceil$. Der Schnittpunkt der Gerade
        $A'B$ mit der reellen Achse heiße $C$.
    (2) Setze $C' \coloneqq A + \ii \left\lceil b \right\rceil$ und konstruiere
        die Geraden $CC'$ und $AA'$. Der Schnittpunkt heiße $X$.
    (3) Konstruiere die Gerade $BB'$. Der Schnittpunkt mit der reellen Achse
        heiße $B'$.

    Nach Teilaufgabe (a) ist nun $A'B'$ parallel zu $AB$ und damit zur
    imaginären Achse. Dies zeigt $B' = a$.

    \paragraph{Errichtung der Senkrechten durch einen reellen Punkt:} Für $B'
    \in \R$ soll eine Gerade durch $B'$ konstruiert werden, die senkrecht auf
    der reellen Achse steht.

    (1) Setze $A \coloneqq \left\lceil B'+1 \right\rceil$, $C \coloneqq
        \left\lfloor B'-1\right\rfloor$, $C' \coloneqq A + \ii$ und $B \coloneqq
        A + 2\ii$.
    (2) Der Schnittpunkt der Geraden $BB'$ und $CC'$ heiße $X$. Der Schnittpunkt
        der Geraden $CB$ und $AX$ heiße $A'$.

    Nach Teilaufgabe (a) ist nun $B'A'$ parallel zu $AB$ und daher senkrecht auf
    der reellen Achse. Es ist klar, dass dieses Verfahren leicht abgeändert für
    die Errichtung von Senkrechten auf die imaginäre Achse angewendet werden
    kann.

    \paragraph{Übertragung von Punkten auf die andere Achse:} Übertragung wird
    hier als Rotation um 0 um 90\textdegree{} verstanden. Um für $B' \in
    \R_{>0}$ den Punkt $B' \cdot \ii$ zu konstruieren, kann man wie folgt
    vorgehen:

    (1) Konstruiere die Winkelhalbierende $w$ des ersten Quadranten, d.h. die
        Gerade durch 0 und $1+\ii$.
    (2) Konstruiere die Gerade $g$ durch $B'$ und $B \eqqcolon \ii \cdot
        \left\lceil B'+1 \right\rceil$.
    (3) Der Schnittpunkt von $w$ und $g$ heiße $X$.
    (4) Konstruiere die Gerade $h$ durch $X$ und $A \eqqcolon \left\lceil B'+1
        \right\rceil$.
    (5) Der Schnittpunkt von $h$ mit der imaginären Achse heiße $A'$.

    Teilaufgabe (a) mit $C = 0$, $B' = a$ liefert die Parallelität der Geraden
    $AB$ und $A'B'$. Daher ist $A' = B' \cdot \ii$. Analoges Vorgehen erlaubt
    die Übertragung beliebiger Punkte von einer Achse auf die andere.

    Jetzt geht's langsam ans Eingemachte:

    \paragraph{Konstruktion des additiv Inversen:} Die Zahl $a \in \R$ kann auf
    die imaginäre Achse und von dort wieder auf die reelle Achse übertragen
    werden, um $-a$ zu erhalten.

    \paragraph{Addition reeller Zahlen:} Seien $a, b \in \R$. Die folgende
    Methode setzt $a \ne b$ voraus. Gilt $a = b = \pm 1$, so ist $a+b \in
    \set{-2, 2}$ auf jeden Fall konstruierbar. Ansonsten konstruiere $(a + (-1))
    + (b + 1)$ und erhalte $a+b$.

    (1) Übertrage $a$ und $b$ auf die imaginäre Achse.
    (2) Konstruiere die Senkrechten auf $a, a\ii, b$ und $b\ii$. Sie schneiden
        sich in $a+b\ii$ und $b+a\ii$.
    (3) Die Gerade durch $a+b\ii$ und $b+a\ii$ schneidet die reelle Achse in
        $a+b$.

    \paragraph{Addition komplexer Zahlen:} Für $a+b\ii, c+d\ii \in \C$ mit $a,
    b, c, d \in \R$ projiziere die Punkte auf die Achsen, addiere
    komponentenweise und konstuiere die Senkrechten über $a+c$ und $(b+d)\ii$.
    Sie schneiden sich in $(a+c) + (b+d)\ii$.

    \paragraph{Multiplikation reeller Zahlen:} Wir beschränken uns auf echt
    positive Zahlen $p, q \in \R$. Wir imitieren die Konstruktion aus Satz 26.10
    mit $G = \ii\R$.

    (1) Konstuiere $q - 1$, $q\ii$ und $(q-1)\ii$.
    (2) Konstuiere die Senkrechten auf $p$ und $(q-1)\ii$. Sie schneiden sich in
        $p + (q-1)\ii$.
    (3) Die Gerade durch $\ii$ und $p$ ist parallel zu der Geraden durch $q\ii$
        und $p + (q-1)\ii$. Letztere schneidet die reelle Achse in $pq$.

    \paragraph{Multiplikation komplexer Zahlen:} Für $z=a+b\ii, w=c+d\ii \in \C$
    mit $a, b, c, d \in \R$ projiziere auf die Achsen und erhalte $a, b, c$ und
    $d$. Konstruiere dann $e = ac-bd$ und $f = ad+bc$. Dann ist $e+f\ii = zw$.

    \paragraph{Konstruktion des multiplikativ Inversen reeller Zahlen:} Sei $a
    \in \R^\times$. Die Gerade durch 0 und $a+\ii$ schneidet die Senkrechte auf
    1 im Punkt $1 + \frac{1}{a}\ii$.

    \paragraph{Konstruktion des multiplikativ Inversen komplexer Zahlen:} Für
    $z = a+b\ii \in \C^\times$ mit $a, b \in \R$ projiziere auf die Achsen und
    erhalte $a$ und $b$. Konstruiere dann $c = \frac{a}{a^2+b^2}$ sowie
    $d = \frac{-b}{a^2+b^2}$. Dann ist $c+d\ii = z^{-1}$.

\newpage

## Aufgabe 5

(a) Da $K$ die Elemente 0 und 1 enthält und unter Addition, Subtraktion,
    Multiplikation und Inversenbildung abgeschlossen ist, ist $K$ ein Körper.
    Wegen $\ii \in K$ gilt $\Q(i) \subseteq K$.

    Sind für Punkte $A, B, C, D \in \Q(i)$ die Geraden $g = AB$ und $h = CD$
    nicht parallel, so hat der Schnittpunkt $P$ von $g$ und $h$ als Lösung eines
    linearen Gleichungssystems über $\Q$ ebenfalls rationalen Real- und
    Imaginärteil. Da alle Punkte in $K$ aus $M \subseteq \Q(i)$ durch Schnitte
    von Geraden konstruierbar sind, haben alle Punkte in $K$ einen rationalen
    Real- und Imaginärteil. Daher gilt $K \subseteq \Q(i)$.

(b) Die Gerade $g$ verlaufe durch die Punkte $A, B \in K = \Q(i)$ mit $A \ne B$.
    Es gilt dann $g = \set{A + \lambda \cdot (B - A) \mid \lambda \in \R}$.
    Der Punkt $Q \coloneqq P + B - A \in \Q(i) = K$ ist konstruierbar und $PQ$
    ist die Parallele zu $g$ durch $P$.

    Der Richtungsvektor $(\Im(B-A), -\Re(B-A)) \in \Q(i)$ steht senkrecht
    auf dem Richtungsvektor $B-A$. Der Punkt $R = P + (\Im(B-A), -\Re(B-A)) \in
    \Q(i) = K$ ist konstruierbar und $PR$ ist das Lot von $P$ auf $g$.
