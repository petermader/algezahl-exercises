# Algebra und Zahlentheorie II -- Übungsblatt 9

## Aufgabe 1

(a) Laut CoCoA lässt sich $g = x^6 -7*x^5 -31*x^4 -183*x^3 +65*x^2 +25*x +625$
    verwenden.

(b) Verwende $g = x^3 -x^2 -4*x +9$. Es gilt
    $$g(\delta + 1) = \delta^3 + 3\delta^2 + 3\delta + 1 - \delta^2 - 2\delta -
    1 - 4\delta - 4 + 9 = \delta^3 + 2\delta^2 - 3\delta + 5 = g(\delta) = 0$$
    für $\delta \in \set{\alpha, \beta, \gamma}$.

(c) Laut CoCoA lässt sich $g = x^3 -10*x^2 -11*x -25$ verwenden.


## Aufgabe 5

Nach Satz 35.15 (b) ist $\Gal(L / K) \cong C_n$.

Betrachte $g = x^k - \alpha$. Es gilt $g(\alpha^\ell) = \alpha^n - a = f(\alpha)
= 0$. Das Minimalpolynom von $\alpha^\ell$ über $K$ hat daher höchstens Grad
$k$. Damit gilt $[K(\alpha^\ell) : K] \leqslant k$. Da $f$ irreduzibel ist, ist
$f$ das Minimalpolynom von $\alpha$ und die Menge
$A = \set{1, \alpha, \dots, \alpha^{n-1}}$ ist $K$-linear unabhängig.
Damit ist die Menge $\set{1, \alpha^\ell, \dots, \alpha^\ell(k-1)} \subseteq A$
ebenfalls $K$-linear unabhängig und es gilt $[K(\alpha^\ell) : K] \geqslant k$.

Für jeden Zwischenkörper $Z$ von $L / K$ mit $[Z \colon K] = k$ ist $\Gal(L /
Z)$ eine Untergruppe der abelschen Gruppe $\Gal(L / K) \cong C_n$ und daher ein
Normalteiler derselben. Nach Satz 32.18 (b) ist $Z / K$ galoissch und
es gilt $\abs{\Gal(Z / K)} = [Z : K] = k$ nach Satz 32.2. Da $\Gal(L / K) \cong
C_n$ als zyklische Gruppe nur eine Untergruppe der Ordnung $k$ besitzt, folgt $Z
= K(\alpha^\ell)$ nach dem Hauptsatz der Galoistheorie (Satz 32.16 (c)).
