# Algebra und Zahlentheorie II -- Übungsblatt 5

\paragraph{Globales Lemma:} Sei $L / K$ eine endliche Körpererweiterung, $a \in
L$ und $\mu_a \in K[x]$ das Minimalpolynom von $a$ über $K$. Dann gilt
$\deg(\mu_a) \mid [L : K]$. \vspace{-.5cm}
\paragraph{Beweis.} Da $L / K$ endlich ist, gibt es $a_1, \dots, a_n \in L$, die
$L$ als $K$-Vektorraum und damit erst recht als $K(a)$-Vektorraum erzeugen.
Somit ist auch $L / K(a)$ endlich und die Gradformel liefert
$$[L : K] = [L : K(a)] \cdot [K(a) : K] = [L : K(a)] \cdot \deg(\mu_a).$$
Dies zeigt $\deg(\mu_a) \mid [L : K]$. \qed

## Aufgabe 1

\paragraph{Lemma:} Seien $f \in \R[x]$ und $z \in \C$ mit $f(z) = 0$. Dann gilt
$f(\overline{z}) = 0$. \vspace{-.5cm}
\paragraph{Beweis.} Schreibe $f = \sum_{i=0}^{n} a_ix^i$ mit $n \geqslant 0$ und
$a_0, \dots, a_n \in \R$. Dann gilt
$$0 = \overline{0} = \overline{\sum_{i=0}^{n} a_iz^i} = \sum_{i=0}^{n}
\overline{a_n} \cdot \overline{z}^i = f(\overline{z}),$$
wobei verwendet wurde, dass die komplexe Konjugation ein
$\R$-Körperautomorphismus von $\C$ ist. \qed

---

(a) Betrachte $f = x^4-2x^2+9 \in \Q[x]$. Es gilt $f(a) = 0$ für alle $a \in
    \set{\sqrt{2}+\ii, \sqrt{2}-\ii, -\sqrt{2}+\ii, -\sqrt{2}-\ii}$. Damit ist
    $f$ irreduzibel über $\Q$ und das Minimalpolynom von $\sqrt{2} + \ii$.

(b) Die algebraischen Zahlen bilden laut Satz 28.8 einen Körper und sind daher
    unter Addition, Subtraktion und Multiplikation abgeschlossen. Mit $a + b\ii$
    ist nach dem Lemma auch $a-b\ii$ algebraisch.

    Somit sind auch $(a+b\ii)+(a-b\ii) = 2a$ und $a$ algebraisch. Damit ist
    wiederum $((a+b\ii)-a) \cdot (-\ii) = b\ii \cdot (-\ii) = b$ algebraisch.

## Aufgabe 2

(a) Das Reduktionskriterium mit $p = 2$ liefert die Irreduzibilität von $f$ über
    $\Q$.

(b) Das Polynom $f$ ist irreduzibel und normiert und daher das Minimalpolynom
    von $z$ über $\Q$. Dagegen würde $a = 0$ bedeuten, dass ein Teiler von $2x^2
    -3x + 2 \in \Q[x]$ das Minimalpolynom von $z$ über $\Q$ wäre, ein
    Widerspruch.

    Wenn man sich auf CoCoA verlassen kann, dann ist $a^{-1} = z^2 + 2z + 1$.

(c) Es ist $z^3-z+1 = 0$. Daher gilt $z^6 = (z^3)^2 = (z-1)^2 = z^2 - 2z + 1$
    und $z^4 = z \cdot z^3 = z(z-1) = z^2 - z$. Das Polynom $g = x^3 - 2x^2 + x
    - 1 \in \Q[x]$ ist irreduzibel nach dem Reduktionskriterium mit $p = 2$ und
    erfüllt $g(z^2) = z^6 - 2z^4 + z^2 - 1 = z^2 - 2z + 1 - 2z^2 + 2z + z^2 - 1
    = 0$ und ist daher das Minimalpolynom von $z^2$ über $\Q$.

## Aufgabe 3

Schreibe $L \coloneqq \Q(\sqrt{-1}, \sqrt{2}, \sqrt{3}, \sqrt{5}, \sqrt{7})$.
Es gilt
$$\begin{aligned}
[L : \Q] &= [L : \Q(\sqrt{-1}, \sqrt{2}, \sqrt{3}, \sqrt{5})] \cdot
[\Q(\sqrt{-1}, \sqrt{2}, \sqrt{3}, \sqrt{5}) : \Q] \\
&= 2 \cdot [\Q(\sqrt{-1}, \sqrt{2}, \sqrt{3}, \sqrt{5}) : \Q(\sqrt{-1},
\sqrt{2}, \sqrt{3})] \cdot [\Q(\sqrt{-1}, \sqrt{2}, \sqrt{3}) : \Q] \\
&= 2 \cdot 2 \cdot [\Q(\sqrt{-1}, \sqrt{2}, \sqrt{3}) : \Q(\sqrt{-1}, \sqrt{2})]
\cdot [\Q(\sqrt{-1}, \sqrt{2}) : \Q] \\
&= 2 \cdot 2 \cdot 2 \cdot [\Q(\sqrt{-1}, \sqrt{2}) : \Q(\sqrt{-1})] \cdot
[\Q(\sqrt{-1}) : \Q] = 2^5 = 32.
\end{aligned}$$
Wegen $z \in L$ gilt nach dem globalen Lemma $\deg(\mu_z) \mid [L : \Q] = 32$.

## Aufgabe 4

Sei $a \in R \subseteq L$. Da $L / K$ algebraisch ist, ist $a$ algebraisch über
$K$ und $K(a) / K$ ist endlich. Der Hilbertsche Nullstellensatz liefert $K(a) =
K[a]$. Dies zeigt $a^{-1} \in K(a) = K[a] \subseteq R$.


## Aufgabe 5

Hätte $f$ eine Nullstelle $a \in L$, so wäre $a$ algebraisch über $K$ und $g =
\frac{f}{\LC(f)}$ das Minimalpolynom von $a$ über $K$. Nach dem globalen Lemma
hätten wir $\deg(f) = \deg(g) \mid [L : K]$. Das ist ein Widerspruch zu
$\ggT(\deg(f), [L : K]) = 1$ und $\deg(f) > 1$.


## Aufgabe 6

(a) Wegen $K \subseteq Z_1$ gilt
    $$\begin{aligned}
    Z_1 \cdot Z_2 &= K(Z_1 \cup Z_2)
    = \bigcap \set{M \subseteq L \mid M
    \text{ ist ein Körper mit } K \cup (Z_1 \cup Z_2) \subseteq M} \\
    &= \bigcap \set{M \subseteq L \mid M
    \text{ ist ein Körper mit } (K \cup Z_1) \cup Z_2 \subseteq M} \\
    &= \bigcap \set{M \subseteq L \mid M
    \text{ ist ein Körper mit } Z_1 \cup Z_2 \subseteq M} = Z_1(Z_2).
    \end{aligned}$$
    Analog folgt $Z_1 \cdot Z_2 = Z_2(Z_1)$.

(b) Sei $M = \set{a \in L \mid a\text{ ist algebraisch über }K}$. Laut Satz 28.8
    ist $K(M) = M$ ein Körper. Da $Z_1 / K$ und $Z_2 / K$ algebraisch sind, gilt
    $Z_1 \cup Z_2 \subseteq M$, also $Z_1 \cdot Z_2 = K(Z_1 \cup Z_2) \subseteq
    K(M) = M$ und $Z_1 \cdot Z_2 / K$ ist algebraisch.

(c) Seien $B_1 = \set{v_1, \dots, v_{n_1}} \subseteq Z_1$ und $B_2 = \set{w_1,
    \dots, w_{n_2}} \subseteq Z_2$ Basen der $K$-Vektorräume $Z_1$ respektive
    $Z_2$.

    Zuerst wollen wir $K[Z_1 \cup Z_2] = \ideal{vw \mid v \in B_1, w \in B_2}_K$
    zeigen. Die Inklusion $\supseteq$ ist klar. Ist dagegen $a \in K[Z_1 \cup
    Z_2]$, so kann man $a = f(a_1, \dots, a_k, b_1, \dots, b_l)$ für ein $f \in
    K[x_1, \dots, x_{k+l}]$ und $a_1, \dots, a_k \in Z_1$ und $b_1, \dots, b_l
    \in Z_2$ schreiben. Damit ist $a$ die Summe von Ausdrücken der Form
    $c \cdot z_1 \cdot z_2$ mit $c \in K$, $z_1 \in Z_1 = \ideal{B_1}_K$ und
    $z_2 \in Z_2 = \ideal{B_2}_K$, also von $K$-Linearkombinationen in
    $\ideal{vw \mid v \in B_1, w \in B_2}$. Dies zeigt $a \in \ideal{vw \mid v
    \in B_1, w \in B_2}$.

    Wegen
    $$K[Z_1 \cup Z_2] \subseteq \ideal{vw \mid v \in B_1, w \in B_2}_K \subseteq
    K[B_1 \cup B_2] \subseteq K[Z_1 \cup Z_2]$$
    und dem Nullstellensatz ist damit $K[Z_1 \cup Z_2] = K[B_1 \cup B_2] = K(B_1
    \cup B_2)$. Weil $Z_1 \cdot Z_2 = K(Z_1 \cup Z_2)$ der kleinste Körper ist,
    der $K[Z_1 \cup Z_2] = K(B_1 \cup B_2)$ enthält, gilt
    $$Z_1 \cdot Z_2 = K(Z_1 \cup Z_2) = K(B_1 \cup B_2) = \ideal{vw \mid v \in
    B_1, w \in B_2}_K$$ und $[Z_1 \cdot Z_2 : K] = \dim_K(K(B_1 \cup B_2))
    \leqslant \abs{\set{vw \mid v \in B_1, w \in B_2}} \leqslant n_1 \cdot n_2$.
