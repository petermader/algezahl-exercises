## Jede endliche Gruppe $G$ ist isomorph zu einer Untergruppe von $\GL_{\abs{G}}(\F_2)$.

---

\paragraph{Definition.} Sei $n \in \N_+$ und $\sigma \in S_n$. Dann heißt
$P(\sigma) = (a_{ij}) \in \Mat_n(\F_2)$ mit
$$a_{ij} = \begin{cases}
    1 & \text{falls $\sigma(j) = i$,} \\
    0 & \text{sonst}
\end{cases}$$
die **Permutationsmatrix** von $\sigma$.

\paragraph{Beispiel.} Für $\sigma = (1\;2\;3)(4\;5) \in S_5$ gilt
$$P(\sigma) = \pmat{0&0&1&0&0\\1&0&0&0&0\\0&1&0&0&0\\0&0&0&0&1\\0&0&0&1&0}.$$

\paragraph{Beispiel.} Für $\Id \in S_n$ gilt $P(\Id) = I_n$.

\paragraph{Bemerkung.} Ist in der $j$-ten Spalte von $P(\sigma)$ die Eins in der
$i$-ten Zeile gesetzt, so bedeutet dies, dass $\sigma$ die Zahl $j$ auf $i$
abbildet.

\paragraph{Lemma:} Sei $n \in \N_+$ und $\sigma \in S_n$. Dann ist $P(\sigma)
\in \GL_n(\F_2)$.
\vspace{-.5cm}
\paragraph{Beweis.} Wegen der Bijektivität von $\sigma$ kommt in jeder Spalte
und Zeile genau eine Eins vor. Mit Laplace-Entwickung nach den Spalten oder
Zeilen erhält man $\det(P(\sigma)) = 1$. \qed

\newpage

\paragraph{Satz.} Für $n \in \N_+$ ist die Abbildung
$$\vfi: S_n \xhookrightarrow{} \GL_n(\F_2), \qquad \sigma \mapsto P(\sigma)$$
ein injektiver Gruppenhomomorphismus.
\vspace{-.5cm}
\paragraph{Beweis.} Es seien $\sigma, \tau \in S_n$ und $P(\sigma) = (a_{ij}),
P(\tau) = (b_{ij})$ die dazugehörigen Permutationsmatrizen. Ferner sei
$P(\sigma)P(\tau) = (c_{ij})$ das Produkt der Matrizen.

Wegen der Bijektivität von $\sigma$ und $\tau$ kommt in jeder Zeile von
$P(\sigma)$ und in jeder Spalte von $P(\tau)$ genau eine Eins vor. Für $i, j \in
\set{1, \dots, n}$ ist daher höchstens einer der Summanden von
$c_{ij} = \sum_{k=1}^{n} a_{ik}b_{kj}$ gleich Eins und es gilt

$$\begin{aligned}
    c_{ij} = \sum_{k=1}^{n} a_{ik}b_{kj} = 1
    &\iff \text{es gibt ein $k \in \set{1, \dots, n}$ mit } a_{ik} = b_{kj} = 1 \\
    &\iff \text{es gibt ein $k \in \set{1, \dots, n}$ mit $\sigma(k) = i$ und
    $\tau(j) = k$} \\
    &\iff \sigma(\tau(j)) = i.
\end{aligned}$$

Dies zeigt $(c_ij) = P(\sigma\tau)$, also
$$\vfi(\sigma)\vfi(\tau) = P(\sigma)P(\tau) = (c_{ij}) = P(\sigma\tau) = \vfi(\sigma\tau),$$
und $\vfi$ ist ein Gruppenhomomorphismus.

Für die Injektivität sei $\sigma \in \Ker(\vfi)$. Dann gilt $\vfi(\sigma) =
\vfi(\Id) = I_n$. Es ist $\sigma(i) = i$ für alle $i \in \set{1, \dots, n}$
und wir haben $\sigma = \Id$. Insgesamt erhalten wir
$$\set{\Id} \subseteq \Ker(\vfi) \subseteq \set{\Id}$$
und $\vfi$ ist injektiv. \qed

\paragraph{Korollar.} Jede endliche Gruppe $G$ mit $n = \abs{G}$ ist isomorph zu
einer Untergruppe von $\GL_n(\F_2)$, denn laut Satz 20.15 ist $G$ isomorph zu einer
Untergruppe von $S_n$ und laut dem obigen Satz ist $S_n$ isomorph zu
$\vfi(S_n) \subseteq \GL_n(\F_2)$.
