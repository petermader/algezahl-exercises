# Algebra und Zahlentheorie II -- Übungsblatt 10

\newcommand\Fix{\mathrm{Fix}}

## Aufgabe 1

Sei $L$ der Zerfällungskörper von $f$ und $\alpha \in L$ eine Nullstelle von
$f$. Ferner sei $\zeta$ eine primitive $n$-te Einheitswurzel über $\Q$. Dann ist
$L = K(\alpha, \zeta)$ und mit Satz 35.15 (b) ist
$$[L : K] = [L : K(\zeta)] \cdot [K(\zeta) : K] = n \cdot \vfi(n)$$
und außerdem $\Gal(L / K(\zeta)) \cong C_n$ sowie $\Gal(K(\zeta) / K) \cong
\Gal(\Q(\zeta) / \Q) \cong (\Z / n\Z)^\times \cong C_{\vfi(n)}$.

Sei $\rho$ ein Erzeuger von $\Gal(K(\zeta) / K)$, d.h. ein Automorphismus der
Ordnung $\vfi(n)$. Betrachte die Automorphismen $\sigma, \tau \in \Gal(L / K)
\eqqcolon G$ mit $\sigma: \alpha \mapsto \zeta\alpha, \zeta \mapsto \zeta$ und
$\tau: \alpha \mapsto \alpha, \zeta \mapsto \rho(\zeta)$. Es gilt
$\ord_G(\sigma) = n$ und $\ord_G(\tau) = \vfi(n)$, also $C_n \cong
\ideal{\sigma} \subseteq G$ und $C_{\vfi(n)} \cong \ideal{\tau} \subseteq G$.
Offenbar ist $\ideal{\sigma} \cap \ideal{\tau} = \ideal{\Id_L}$. Mit Aufgabe 1
aus Algebra und Zahlentheorie I, Blatt 10, haben wir
$$\abs{\ideal{\sigma} \cdot \ideal{\tau}} = \frac{\abs{\ideal{\sigma}} \cdot
\abs{\ideal{\tau}}}{\abs{\ideal{\sigma} \cap \ideal{\tau}}} = n \cdot \vfi(n) =
\abs{G},$$
also $G = \ideal{\sigma} \cdot \ideal{\tau}$. Weil $K(\zeta) / K$ galoissch ist,
ist $\Gal(L / K(\zeta)) = \ideal{\sigma}$ ein Normalteiler von $G = \Gal(L / K)$
nach Satz 32.18 (b). Jetzt liefert Satz 21.15 $G = \ideal{\sigma} \rtimes
\ideal{\tau} \cong C_n \rtimes C_{\vfi(n)}$.


## Aufgabe 2

Da $G = \Gal(F / K)$ auflösbar und nicht-trivial ist, gibt es eine normale
Untergruppe $U \subsetneq G$, sodass $G / U$ abelsch ist. Nach dem Hauptsatz der
Galoistheorie (Theorem 32.16) ist dann $E = \mathrm{Fix}_U(F)$ ein
Zwischenkörper mit $\Gal(F / E) = U \subsetneq G$ und $E \supsetneq K$. Nach
Satz 32.18 ist $E / K$ galoissch und $\Gal(F / K) \cong \Gal(F / K) \;/\; \Gal(F
/ E) = G / U$ abelsch.


## Aufgabe 3

Sei $a = 123456789123456789$.

(a) Mit $g = x^{15}-1 \in \Q[x]$ ist $\Gal(g) \cong (\Z / 15\Z)^\times \cong C_2
    \oplus C_4$. Betrachte nun $f \cdot (x-a)$. Offenbar ist $\Gal(f) = \Gal(g)
    \cong C_2 \oplus C_4$. Das Polynom $f$ wurde mit hoher Wahrscheinlichkeit
    in der Vorlesung noch nicht untersucht.

(b) Laut Beispiel 35.19 oder Satz 31.21 gilt $\Gal(g) \cong S_5$ für das Polynom
    $g = x^5 -4x + 2 \in \Q[x]$. Betrachte nun $f = g \cdot
    (x-a)$. Offenbar ist $\Gal(f) = \Gal(g) \cong
    S_5$. Das Polynom $f$ wurde mit hoher Wahrscheinlichkeit in der
    Vorlesung noch nicht untersucht.

## Aufgabe 4

Es ist $f \coloneqq x^4 - 5 = (x-\alpha)(x+\alpha)(x-\ii\alpha)(x+\ii\alpha)$
mit $\alpha = \sqrt[4]{5}$ und daher $L = \Q(\alpha, \ii\alpha) = \Q(\alpha,
\ii)$. Wegen $\Q(\alpha) \subseteq \R$ und $\ii \in L \setminus \R$ ist
$\Q(\alpha) \subsetneq L$.
Also ist $[L : \Q(\alpha)] = 2$ und es gilt $[L : \Q] = [L : \Q(\alpha)
] \cdot [\Q(\alpha) : \Q] = 2 \cdot 4 = 8$. Eine $\Q$-Vektorraumbasis ist
gegeben durch $B = \set{\ii^k\alpha^\ell \mid k \in \set{0, 1}, \ell \in \set{0,
1, 2, 3}}$.

Die Automorphismen $\rho, \sigma \in G = \Gal(L / \Q)$ mit $\rho : \alpha
\mapsto \ii\alpha, \ii \mapsto \ii$ und $\sigma : \alpha \mapsto \alpha, \ii
\mapsto -\ii$ entsprechen den Permutationen $(1\;2\;3\;4), (2\;4) \in S_4$.
Jetzt können wir Aufgabe 1 aus Algebra und Zahlentheorie~I, Blatt 10, verwenden:
Die Gruppe $\ideal{\rho, \sigma} \cong D_4$ hat Ordnung $8 = [L : \Q]$, woraus
wir $\Gal(L / \Q) = \ideal{\rho, \sigma}$ schließen, und die Untergruppen, denen
nach dem Hauptsatz genau die Zwischenkörper von $L / \Q$ entsprechen, sind

(1) $U = \set{\Id} \cong C_1$: Es ist $\Fix_U(L) = L$.
(2) $U = \ideal{\rho^2} \cong C_2$: Wir betrachten die Elemente $B$, die von
    $\rho^2 : \alpha \mapsto -\alpha, \ii \mapsto \ii$ fixiert werden. Das sind
    $1, \alpha^2, \ii, \ii\alpha^2$, es gilt also $K \coloneqq \Q(\ii, \alpha^2)
    \subseteq \Fix_U(L)$. Wegen $[K : \Q] = 4$ und $[\Fix_U(L) : \Q] < 8$ gilt
    sogar $\Fix_U(L) = K = \Q(\ii, \alpha^2)$.
(3) $U = \ideal{\sigma} \cong C_2$: Da $\sigma$ genau die reellen Basiselemente
    fixiert, liefern zu (2) analoge Überlegungen $\Fix_U(L) = \Q(\alpha)$.
(4) $U = \ideal{\rho\sigma} \cong C_2$: Jetzt wird es ein bisschen unschön. Es
    gilt $\rho\sigma : \alpha \mapsto \ii\alpha, \ii \mapsto -\ii$. Mit
    Satz 36.6 (b) ist
    $$\begin{aligned}
    \Fix_U(L) &= \sum_{b \in B} K \cdot \Spur_U(b) = \sum_{b \in B} K \cdot
    \sum_{\vfi \in U} \vfi(b) = \sum_{b \in B} K \cdot (b + \rho\sigma(b)) \\
    &= \ideal{1 + 1, \alpha + \ii\alpha, \alpha^2 -
    \alpha^2, \alpha^3 - \ii\alpha^3, \ii\alpha + \alpha,
    \ii\alpha^2 + \ii\alpha^2, \ii\alpha^3 - \alpha^3}_K \\
    &= \ideal{1, \alpha+\ii\alpha, \ii\alpha^2, \alpha^3-\ii\alpha^3}_K =
    \Q(\alpha+\ii\alpha, \ii\alpha^2).
    \end{aligned}$$
(5) $U = \ideal{\rho^2\sigma} \cong C_2$: Analog zu (4) erhalten wir $\Fix_U(L)
    = \Q(\alpha^2, \ii\alpha)$.
(6) $U = \ideal{\rho^3\sigma} \cong C_2$: Analog zu (4) erhalten wir $\Fix_U(L)
    = \Q(\alpha-\ii\alpha, \ii\alpha^2)$.
(7) $U = \ideal{\rho} \cong C_4$: Der Automorphismus $\rho$ fixiert $\ii$, aber
    nicht $\alpha$, es gilt $\Fix_U(L) = \Q(\ii)$.
(8) $U = \ideal{\rho^2, \sigma} \cong V_4$: $\rho^2$ fixiert $1, \ii, \alpha^2,
    \ii\alpha^2$, $\sigma$ fixiert $1, \alpha, \alpha^2, \alpha^3$. Daher ist
    $\Fix_U(L) = \Q(\alpha^2)$.
(9) $U = \ideal{\rho^2, \rho\sigma} \cong V_4$: $\rho\sigma$ fixiert
    $\alpha+\ii\alpha$ und $\ii\alpha^2$, $\rho^2$ fixiert davon nur
    $\ii\alpha^2$. Daher ist $\Fix_U(L) = \Q(\ii\alpha^2)$.
(10) $U = \ideal{\rho, \sigma} \cong D_4$: Es ist $\Fix_U(L) = \Q$.


## Aufgabe 5

\paragraph{Lemma 1:} Eine Gruppe der Ordnung 8 besitzt normale Untergruppen der
Ordnungen 2 und 4. \vspace{-0.3cm}
\paragraph{Beweis.} Es sei $H$ eine Gruppe der Ordnung 8. Nach Satz 23.5 besitzt
$H$ ein nicht-triviales Zentrum $Z$. Weil auch $Z$ eine 2-Gruppe ist, existiert
nach dem Satz von Cauchy (Satz 23.5) ein Element $a \in H$ mit $\ord_H(a) = 2$
und $\ideal{a} \subseteq Z \subseteq H$ ist eine Untergruppe der Ordnung 2, die
als Untergruppe von $Z$ auch normal in $H$ ist.

Gibt es ein Element $b \in H$ mit $\ord_H(b) = 4$, so ist mit $\ideal{b}
\subseteq H$ eine Untergruppe der Ordnung 4 gefunden. Ansonsten hat jedes
Element die Ordnung 2 und $H$ ist abelsch (Satz 14.10). Zwei verschiedene solche
Elemente $c, d \in H$ erzeugen dann eine Untergruppe $\ideal{c, d} \subseteq H$
der Ordnung 4. Jede Untergruppe der Ordnung 4 hat Index 2 in $H$ und ist daher
normal in $H$. \qed

---

Sei $G = \Gal(L / K)$. Nach dem Hauptsatz und Satz 32.18 ist die Existenz
normaler Untergruppen von $U$ der Ordnungen 20, 10 und 5 zu zeigen.

Wegen $\abs{G} = 40 = 2^3 \cdot 5$ ist die Anzahl der
$5$-Sylowuntergruppen ein Teiler von 8 und kongruent zu 1 modulo 5. Es gibt
daher eine eindeutige 5-Sylowuntergruppe $U_5 \subseteq G$, die auch gleich
normal ist. Nun sei $H$ eine der 2-Sylowuntergruppen. Es ist $\abs{H} = 8$, nach
Lemma 1 existieren normale Untergruppen $U_2, U_4 \subsetneq H \subseteq G$ der
Ordnungen 2 respektive 4. Als Primärgruppen zu unterschiedlichen Primzahlen
haben $U_5$ und $U_2$ beziehungsweise $U_5$ und $U_4$ trivialen Schnitt. Daher
sind $U_5U_2$ sowie $U_5U_4$ Untergruppen von $G$. Da $U_5U_4$ Index 2 in $G$
hat, ist $U_5U_4$ ein Normalteiler.

Laut GAP haben alle 14 Gruppen der Ordnung 40 einen Normalteiler der Ordnung 10.
Ich verstehe allerdings nicht, warum es den gibt und ob sich dafür $U_5U_2$
verwenden lässt.
