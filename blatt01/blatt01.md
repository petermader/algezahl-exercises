# Algebra und Zahlentheorie II -- Blatt 1

## Aufgabe 1

(a) Es gilt $1980 = 2^2 \cdot 3^2 \cdot 5 \cdot 11$. Wir betrachten die Anzahl
    der Möglichkeiten für die Primärkomponenten einer abelschen Gruppe der
    Ordnung 1980 gemäß Satz 22.5.

    Der Exponent 2 besitzt die Partitionen $2$ und $1 + 1$. Nach Satz 22.9 gibt
    es für die 2-Primärkomponente und die 3-Primärkomponente genau zwei
    Möglichkeiten, für die 5-Primärkomponente und die 11-Primärkomponente genau
    eine Möglichkeit.

    Es gibt daher genau $2 \cdot 2 \cdot 1 \cdot 1 = 4$ Isomorphieklassen für
    abelsche Gruppen der Ordnung 1980.

    In der Algebra-Klausur der Bayerischen Staatsexamens aus dem Jahr 1980
    erscheint die Aufgabe im folgenden Wortlaut:

    > Bestimmen Sie, wie viele nichtisomorphe abelsche Gruppen der Ordnung 1980
    > existieren, und geben Sie aus jeder Isomorphieklasse ein Beispiel.

(b) Es gilt $\abs{(\Z / 9\Z)^\times} = \vfi(9) = 6$, und da $C_6$ die einzige
    abelsche Gruppe der Ordnung 6 ist, gilt \mbox{$(\Z / 9\Z)^\times \cong
    C_6$}. Mit dem Chinesischen Restsatz erhalten wir
    $$(\Z / 45\Z)^\times \cong (\Z / 5\Z)^\times \oplus (\Z / 9\Z)^\times \cong
    C_4 \oplus C_6 \cong C_4 \oplus C_2 \oplus C_3.$$

\newpage


## Aufgabe 2

Im Folgenden bezeichne $P(n)$ die Anzahl der Partitionen einer natürlichen
Zahl $n$.

(a) Wegen $64 = 2^6$ und Satz 22.9 ist $P(6)$ zu bestimmen, eine etwas
    mühselige Arbeit, wenn man für $P$ keine geschlossene Formel kennt.

    Die Zahl 5 besitzt die folgenden sieben Partitionen:
    $$5 = 4 + 1 = 3 + 1 + 1 = 2 + 1 + 1 + 1 = 1 + 1 + 1 + 1 = 3 + 2 = 2 + 2 +
    1.$$
    Jede dieser Partitionen kann durch Addition mit 1 zu einer Partition von 6
    erweitert werden. Die Zahl 6 besitzt außerdem die vier Partitionen
    $$6 = 3 + 3 = 2 + 2 + 2 = 4 + 2.$$
    Somit gibt es genau elf Isomorphieklassen von abelschen Gruppen der
    Ordnung 64.

(b) Es sei $n$ die kleinste solche Zahl. Schreibe $n = p_1^{\alpha_1} \cdots
    p_s^{\alpha_s}$ mit $s \in \N$ und $\alpha_i \geqslant 1$ und paarweise
    verschiedenen Primzahlen $p_i$ als Primfaktorzerlegung von $n$.

    Nach Satz 22.9 ist $6 = \prod_{i=1}^s P(\alpha_i)$. Es gibt zwei Fälle:

    * $P(\alpha_i) = 6$ für ein $i$ und $P(\alpha_j) = 1$ für alle $j \ne i$.
      Da $P$ echt monoton ist und $P(4) = 5$ und $P(5) = 7$ erfüllt, kann $P$
      den Wert 6 nicht annehmen und der Fall ist unmöglich.

    * $P(\alpha_i) = 3$ für ein $i$ und $P(\alpha_j) = 2$ für ein $j$ und
      $P(\alpha_k) = 1$ für alle $k \notin \set{i, j}$. Dieser Fall ist möglich
      für $\alpha_i = 3$ und $\alpha_j = 2$.

      Um $n$ zu minimieren, wählen wir $s = 2$ und $\set{p_1, p_2} = \set{2, 3}$
      und $n = 2^3 \cdot 3^2 = 72$.

\newpage


## Aufgabe 3

\paragraph{Lemma 1:} Betrachte $U = \ideal{(2, -3)} \subseteq \Z^2$. Es ist
$\Z^2 / U \cong \Z$.\vspace{-.5cm}
\paragraph{Beweis.} Wir zeigen, dass die Abbildung $\vfi: \Z \to \Z^2 / U,
\; k \mapsto (-k, k) + U$ ein Isomorphismus von Gruppen ist. Dass $\vfi$ ein
Gruppenhomomorphismus ist, sieht man an
$$\vfi(k+l) = (-k-l,k+l)+U = ((-k,k)+U) + ((-l,l)+U) = \vfi(k) + \vfi(l)$$
für $k, l \in \Z$. Die Äquivalenzkette
$$\begin{aligned}
k \in \Ker(\vfi) &\iff \vfi(k) = (0, 0) + U \iff (-k, k) \in U \\
&\iff (-k, k) = (2m, -3m) \text{ für ein $m\in\Z$} \iff k = 0
\end{aligned}$$
für alle $k \in \Z$ zeigt dann die Injektivität von $\vfi$.

Die Surjektivität wird jetzt ein bisschen ekelhaft. Sei $(a, b)+U \in \Z^2 / U$.
Teile $b$ mit Rest durch 3 und erhalte $b = 3q+r$ mit $q \in \Z, r \in
\set{0, 1, 2}$. Mit der Abkürzung $c = a+2q$ gilt dann
$$(a, b) + U = (a+2q-2q, 3q+r) + U = (a+2q, r) + (-2q, 3q) + U = (a+2q, r) +
U = (c, r) + U.$$
Nun setze $k = -3c - 2r$. Es gilt
$$\begin{aligned}
\vfi(k) &= (-k,k)+U = (3c + 2r, -3c - 2r) + U = (3c+2r,r) + (0,-3c-3r) + U \\
&= (3c+2r,r) + (-2c-2r,0) + U = (c,r)+U = (a,b)+U
\end{aligned}$$
und wir sind fertig. \qed

\paragraph{Lemma 2:} Sind $G, H$ abelsche Gruppen und $U \subseteq G, V
\subseteq H$ Untergruppen, so ist $$(G \oplus H) / (U \oplus V) \cong (G / U)
\oplus (H / V).$$\vspace{-1.2cm}
\paragraph{Beweis.} Die Abbildung
$$\eps: G \oplus H \to (G / U) \oplus (H / V), \; (a, b) \mapsto (a+U, b+V)$$
ist offensichtlich ein Epimorphismus von Gruppen. Für $(a, b) \in G \oplus H$
gilt ferner
$$(a, b) \in \Ker(\eps) \iff (a+U, b+V) = \vfi(a,b) = (U, V) \iff (a, b) \in U
\oplus V$$
und irgendein Isomorphiesatz wird's schon richten. \qed

---

Die Aufgabenstellung soll wohl so verstanden werden, dass $A$ Rang 3 haben soll.

Unter Zuhilfenahme beider Lemmas gilt
$$A/U \cong \Z^3 / \ideal{(2,-3,0), (0,0,4)} = (\Z^2 \oplus \Z) /
(\ideal{(2,-3)} \oplus 4\Z) \cong (\Z^2 / \ideal{(2,-3)}) \oplus (\Z / 4\Z)
\cong \Z \oplus C_4.$$

\newpage


## Aufgabe 4

Die Aufgabenstellung\footnote{Jetzt sind \textit{Erzeuger} schon nicht mehr gut
genug, man sagt \textit{Erzeugende}.} soll wohl so verstanden
werden, dass $A$ isomorph ist zu der Gruppe $G = \Z^3 / U$ mit
$$U = \ideal{(1,1,3), (2,3,1), (5,1,-4), (0,5,2)}.$$
Somit können $a, b, c$ mit $(1,0,0)+U, (0,1,0)+U$ respektive $(0,0,1)+U$
identifiziert werden.

Es gilt
$$\begin{aligned}
(3, 0, 0) &= 3 \cdot (1,1,3) - 5 \cdot (2,3,1) + 2 \cdot (5,1,-4) + 2 \cdot
(0,5,2) \in U,\\
(0, 3, 0) &= (1,1,3) -3 \cdot (2,3,1) + (5,1,-4) + 2 \cdot (0,5,2) \in U,\\
(0, 0, 3) &= 4 \cdot (1,1,3) - 7 \cdot (2,3,1) + 2 \cdot (5,1,-4) + 3 \cdot
(0,5,2) \in U.
\end{aligned}$$

Somit gilt $3a = 3b = 3c = 0 + U$ und $a, b, c$ besitzen jeweils die Ordnung 1
oder 3. Aus den Relationen $a+b+3c=0$ und $2a+3b+c=0$ folgt daher $a+b=0$ und
$2a+c=0$, also $b = -a = -c$. Das lässt noch die Möglichkeiten $A \cong C_1$ und
$A \cong C_3$.

Wir werden nun noch zeigen, dass $U \ne \Z^3$. Dann ist nämlich $A \cong \Z^3 /
U \not\cong C_1$, also $A \cong C_3$.

Mit den obigen Linearkombinationen, insbesondere der zweiten, die den Austausch
von $(5,1,-4)$ durch $(0,3,0)$ erlaubt, kann das gegebene Erzeugendensystem von
$U$ vereinfacht werden; wir haben
$$U = \ideal{(1,1,0), (2,0,1), (0,3,0), (0,2,2)}.$$
Angenommen, es gälte $U = \Z^3$. Dann gäbe es $x, y, z, w \in \Z$ mit
$$(0,1,0) = x(1,1,0) + y(2,0,1) + z(0,3,0) + w(0,2,2),$$
oder einfacher
$$\left\{\;\begin{aligned}
    &x + 2y &= 0, \\
    &x + 3z + 2w &= 1, \\
    &y + 2w &= 0.
\end{aligned}\right.$$
Zöge man von der zweiten Gleichung die erste und die dritte ab, so erhielte man
$-3y + 3z = 1$. Hurra, Widerspruch!

\newpage


## Pensum V

(a) Sit $G$ congregatio ordinis $1001$, atque designet $s_7$
    multitudinem subcongregationum syloviensium congregationis $G$ ad numerum
    primum 7, $s_{11}$ ad 11, $s_{13}$ ad 13.

    Secundum syloviensem propositionem tertiam (propositio 23.11) erit $s_7 \mid
    11 \cdot 13$, quo $s_7 \in \set{1, 11, 13, 143}$ concludimus. Additicia
    finitio $s_7 \equiv 1 \pmod{7}$ poscens solum $s_7 = 1$ patitur.

    Similiter multitudinibus $s_{11}$ ac $s_{13}$ procedentes singulares
    subcongregationes sylovienses ad 7 et 11 et 13 esse invenimus. Quas $U_7$ et
    $U_{11}$ et $U_{13}$ appellemus. Subcongregationum earum quaeque suam
    coniugationis classem fingit secundum syloviensem propositionem secundam
    (propositio 23.9), quare quaeque subcongregatio normalis est.

    Cum congregationes primae ad numeros primos diversos $U_7$ et $U_{11}$ et
    $U_{13}$ sint, earum duo commissuram modo $\set{e}$ habent. Ergo congregatio
    $G$ productum penitum derectum earum trium subcongregationum est atque
    invenimus
    $$G = U_7 \cdot U_{11} \cdot U_{13} \cong C_7 \times C_{11} \times C_{13}
    \cong C_{1001}.$$

(b) Secundum propositionem cauchiensem (propositio 23.3) in congregatione $G$
    ordinis $2002 = 2 \cdot 7 \cdot 11 \cdot 13$ elementa $a_7, a_{11},
    a_{13} \in G$ ordines 7 et 11 et 13 habentia sunt. Subcongregatio
    $\ideal{a_7, a_{11}, a_{13}} \subseteq G$ est ordinis 1001 ac indicis 2;
    ergo normalis est.

\newpage


## Aufgabe 6

Die 2-Sylowuntergruppen von $S_5$ besitzen acht Elemente. Nach dem dritten Satz
von Sylow (Satz 23.11) gilt \mbox{$s_2 \mid 15$} für die Anzahl $s_2$ der
2-Sylowuntergruppen von $S_5$. Es gibt also höchstens 15 dieser
2-Sylowuntergruppen.

Betrachte die drei Untergruppen $V_1 = \ideal{(1\;2\;3\;4), (1\;3)}, V_2 =
\ideal{(1\;3\;2\;4), (1\;2)}$ und $V_3 = \ideal{(1\;2\;4\;3), (1\;4)}$. Diese
sind paarweise verschieden, aber alle isomorph zu $D_4$ und daher
2-Sylowuntergruppen von $S_5$.

Man erhält zwölf weitere, verschiedene 2-Sylowuntergruppen, indem man jeweils
eine der Zahlen $1, 2, 3$ oder 4 durch 5 ersetzt, d.h. durch $(i\;5)V_1(i\;5),
(i\;5)V_2(i\;_5)$ und $(i\;5)V_3(i\;5)$ für $i \in \set{1, 2, 3, 4}$.

Insgesamt haben wir 15 dieser 2-Sylowuntergruppen; sie sind allesamt isomorph zu
$D_4$.

Die 5-Sylowuntergruppen von $S_5$ besitzen fünf Elemente und sind isomorph zu
$C_5$. Sie enthalten also jeweils vier der 24 5-Zyklen. Somit gibt es genau
sechs 5-Sylowuntergruppen in $S_5$.

\newpage


## Aufgabe 7

Die zu zeigende Aussage gilt nur unter der zusätzlichen Voraussetzung, dass $p
\mid \ord(N)$. Sonst ist nämlich $K \cap N = \set{e}$ keine Sylowuntergruppe im
Sinne von Definition 23.7.

Unter der Voraussetzung $p \mid \ord(N)$ liefert Lemma 23.10 ein $a \in G$,
sodass $V \coloneqq N \cap (aKa^{-1})$ eine \mbox{$p$-Sylowuntergruppe} von $N$
ist. Offenbar ist $K \cap N$ eine Untergruppe von $N$, es bleibt also noch
$\abs{K \cap N} = \abs{V}$ zu zeigen.

Es gilt
$$V = N \cap (aKa^{-1}) = (aNa^{-1}) \cap (aKa^{-1}) = a(N \cap K)a^{-1}$$
und wegen der Bijektivität der Konjugation mit $a$ schließlich
$$\abs{N \cap K} = \abs{a(N \cap K)a^{-1}} = \abs{V}.$$

\newpage


## Aufgabe 8

Fall (2) ist falsch formuliert, sowohl in der Aufgabenstellung als auch in
Bemerkung 23.14. Für den Gruppenhomomorphismus muss $\Phi \colon \Z / p\Z \to
\Aut(\Z / q\Z) \cong C_{q-1}$ gelten.

Der Fall (1) ist lediglich ein Sonderfall von (2), es braucht nur $\Phi$ trivial
gewählt zu werden, um $\Z / q\Z \rtimes_\Phi \Z / p\Z = \Z / q\Z \times \Z / p\Z
\cong \Z / pq\Z$ zu erhalten.

Nach dem ersten Satz von Sylow (Satz 23.8) existieren eine $p$-Sylowuntergruppe
$U_p$ und eine $q$-Sylowuntergruppe $U_q$ von $G$. Die $q$-Sylowuntergruppe ist
sogar eindeutig: Für die Anzahl $s_q$ der $q$-Sylowuntergruppen von $G$ gilt
nach dem dritten Satz von Sylow (Satz 23.11), dass $s_q \mid p$ und $s_q = 1 +
kq$ für ein $k \in \N_0$. Der Fall $k > 0$ würde $s_q > q > p$ nach sich ziehen,
im Widerspruch zu $s_q \mid p$.

Die eindeutige $q$-Sylowuntergruppe $U_q$ bildet nach dem zweiten Satz von Sylow
(Satz 23.9, jetzt haben wir alle drei Sylow-Sätze verwendet!) ihre eigene
Konjugationsklasse, ist also normal in $G$.

Weil wir Satz 21.15 anwenden wollen, ist noch $G = U_q \cdot U_p$ und $U_q \cap
U_p = \set{e}$ zu zeigen. Die nicht-neutralen Elemente von $U_q$ besitzen
allesamt die Ordnung $q$, die nicht-neutralen Elemente von $U_p$ die Ordnung
$p$. Somit gilt $U_q \cap U_p = \set{e}$.

Unter Zuhilfenahme von Blatt 10, Aufgabe 5 (a), erhalten wir
noch
$$\abs{U_q \cdot U_p} = \frac{\abs{U_q} \cdot \abs{U_p}}{\abs{U_q \cap U_p}} =
qp = \abs{G},$$
also $U_q \cdot U_p = G$. Satz 21.15 liefert nun
$$G = U_q \rtimes U_p \cong \Z / q\Z \rtimes_\Phi \Z / p\Z$$
für ein geeignetes $\Phi$ gemäß Satz 21.15.

\newpage


## Aufgabe 9

Die bereits mehrmals getätigten Überlegungen zur Eindeutigkeit einer
Sylowuntergruppe finden hier wieder Anwendung: Bei einer Gruppe $G$ der Ordnung
$297 = 27 \cdot 11$ gilt nach dem dritten Satz von Sylow (Satz 23.11) für die
Anzahl $s_3$ beziehungsweise $s_{11}$ der $3$- beziehungsweise
$11$-Sylowuntergruppen:
$$s_3 \mid 11, \qquad s_3 \equiv 1 \pmod{3}, \qquad s_{11} \mid 27, \qquad
s_{11} \equiv 1 \pmod{11}.$$
Diese Einschränkungen lassen mal wieder nur $s_3 = s_{11} = 1$ zu. Die
eindeutigen Sylowuntergruppen $U_3$ und $U_{11}$ zu 3 beziehungsweise 11 bilden
ihre eigenen Konjugationsklassen und sind daher normal. Ferner sind sie als
Primärgruppen laut Satz 24.13 auflösbar und haben trivialen Durchschnitt.

Es gilt daher $G \cong U_3 \times U_{11}$. Da $U_{11}$ und
$$G / U_{11} \cong (U_3 \times U_{11}) / (\set{e} \times U_{11}) \cong U_3$$
auflösbar sind, liefert Korollar 24.11, dass auch $G$ auflösbar ist.

\newpage


## Aufgabe 10

(a) Ein allerletztes Mal: Es gibt nur eine $5$-Sylowuntergruppe in $G$, denn für
    die Anzahl der $5$-Sylowuntergruppen $s_5$ muss laut dem dritten Satz von
    Sylow (Satz 23.11) wieder $s_5 \mid 4$ und $s_5 \equiv 1 \pmod{5}$.

    Daher ist die $5$-Sylowuntergruppe $U_5$ normal, laut Satz 24.13 außerdem
    auflösbar. Außerdem $G / U_5$ wegen $\abs{G / U_5} = 4$ abelsch und daher
    auflösbar. Korollar 24.11 liefert die Auflösbarkeit von $G$.

(b) Auch dieses Argument wurde schon mehrmals vorgebracht: Ist $U_2$ ein solcher
    Normalteiler, so haben $U_2$ und $U_5$ trivialen Durchschnitt, es gilt also
    $G \cong U_2 \times U_5$.

    Die Ordnungen von $U_2$ und $U_5$ sind Primzahlquadrate. Nach Satz 20.14
    sind also $U_2$ und $U_5$ und daher $G \cong U_2 \times U_5$ abelsch.

\newpage


## Aufgabe 5

(a) Sei $G$ eine Gruppe der Ordnung $1001$. Es bezeichne $s_7, s_{11}, s_{13}$
    die Anzahl der $7$-, $11$- respektive $13$-Sylowuntergruppen von $G$.

    Nach dem dritten Satz von Sylow (Satz 23.11) gilt $s_7 \mid 11 \cdot 13$.
    Das lässt die Möglichkeiten $s_7 \in \set{1, 11, 13, 143}$ offen. Die
    weitere Einschränkung $s_7 \equiv 1 \pmod{7}$ lässt aber nur $s_7 = 1$ zu.

    Analoge Überlegungen für $s_{11}$ und $s_{13}$ führen dazu, dass es jeweils
    eine eindeutige $7$-, $11$- oder $13$-Sylowuntergruppe $U_7$, $U_{11}$
    respektive $U_{13}$ gibt. Diese Untergruppen bilden laut dem zweiten Satz
    von Sylow (Satz 23.9) jeweils ihre eigene Konjugationsklasse und sind daher
    normal in $G$.

    Als Primärgruppen zu unterschiedlichen Primzahlen haben $U_7$, $U_{11}$
    und $U_{13}$ paarweise nur $\set{e}$ als Durchschnitt. Daher ist $G$ inneres
    direktes Produkt dieser drei Untergruppen und es gilt
    $$G = U_7 \cdot U_{11} \cdot U_{13} \cong C_7 \times C_{11} \times C_{13}
    \cong C_{1001}.$$

(b) In einer Gruppe $G$ der Ordnung $2002 = 2 \cdot 7 \cdot 11 \cdot 13$ gibt es
    nach dem Satz von Cauchy (Satz 23.3) Elemente $a_7, a_{11}, a_{13} \in G$
    der Ordnungen 7, 11 respektive 13. Die Untergruppe $\ideal{a_7, a_{11},
    a_{13}} \subseteq G$ hat Ordnung 1001 und Index 2 und ist daher ein
    Normalteiler.
