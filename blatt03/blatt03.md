# Algebra und Zahlentheorie II -- Übungsblatt 3

## Aufgabe 1

Ist $\sqrt{b} \in \Q$, so ist $\Q(\sqrt{b}) = \Q = \Q + \sqrt{b}\Q$. Ist
$\sqrt{b} \notin \Q$, so ist $\Q(\sqrt{b}) = \Q \oplus \sqrt{b}\Q$ nach Beispiel
25.20.

Nach Bemerkung 25.15 (b) bildet $\vfi$ den Primkörper $Q$ identisch ab. Es gilt
daher $\vfi(\sqrt{a})^2 = \vfi(a) = a = \sqrt{a}^2$, also $\abs{\vfi(\sqrt{a})}
= \abs{\sqrt{a}}$, und daher $\sqrt{a} \in \Q(\sqrt{b}) = \Q + \sqrt{b}\Q$.

Mit $\vfi$ ist auch $\vfi^{-1}$ ein Körperisomorphismus und dasselbe Argument
liefert $\sqrt{b} \in \Q(\sqrt{a}) = \Q + \sqrt{a}\Q$. Also gibt es $c, d, e, f$
mit $\sqrt{a} = c + d\sqrt{b}$ und $\sqrt{b} = e + f\sqrt{a}$. Es gilt
$$a = \sqrt{a}^2 = (c + d\sqrt{b})^2 = c^2 + 2cd\sqrt{b} + d^2b.$$
Wir unterscheiden vier Fälle:

* Der Fall $c = 0, d = 0$ würde $a = b = 0$ nach sich ziehen und ist daher
  unmöglich.
* Im Fall $c = 0, d \ne 0$ ist $a = d^2b$ und $\frac{a}{b} = d^2$ ist ein
  Quadrat in $\Q$.
* Im Fall $c \ne 0, d = 0$ ist $a = c^2$ und $\sqrt{b} = e + f\sqrt{a} = e +
  f\abs{c} \in \Q$. Damit sind $a$ und $b$ Quadrate in $\Q$ und in der Folge
  auch $\frac{a}{b}$.
* Im Fall $c \ne 0, d \ne 0$ ist $\sqrt{b} = \frac{a - c^2 - d^2b}{2cd} \in \Q$
  und damit $\sqrt{a} = c + d\sqrt{b} \in \Q$. Somit sind $a$ und $b$ Quadrate
  in $\Q$ und in der Folge auch $\frac{a}{b}$.

## Aufgabe 2

(a) Offenbar ist $\Q(\sqrt{a} + \sqrt{b}) \subseteq \Q(\sqrt{a}, \sqrt{b})$.

    Für die Rückrichtung unterscheiden wir:

    * Gilt $a = b$, so ist $\sqrt{a} = \sqrt{b} = \frac{1}{2} \cdot 2 \cdot
      \sqrt{b} \in \Q(2\sqrt{b}) = \Q(\sqrt{a} + \sqrt{b})$, also $\Q(\sqrt{a},
      \sqrt{b}) \subseteq \Q(\sqrt{a}+\sqrt{b})$.

    * Gilt $a \ne b$, so ist
      $$\sqrt{a} = \frac{(\sqrt{a}+\sqrt{b})((\sqrt{a}+\sqrt{b})^2 - 2a -
      b)}{b-a} \in \Q(\sqrt{a} + \sqrt{b}),$$
      damit $\sqrt{b} = (\sqrt{a} + \sqrt{b}) - \sqrt{a} \in \Q(\sqrt{a} +
      \sqrt{b})$ und schließlich $\Q(\sqrt{a}, \sqrt{b}) \subseteq
      \Q(\sqrt{a+b})$.

\newpage

## Aufgabe 3

\paragraph{Lemma:} Sei $p$ eine Primzahl und $V$ ein endlicher
$\F_p$-Vektorraum. Dann gilt $\sum_{v \in V} v = 0$. \vspace{-0.5cm}
\paragraph{Beweis.} Es gilt $V \cong (\F_p)^n$ für $n = \dim_{\F_p}(V)$. Wir
beweisen die Aussage per Induktion über $n$.

Ist $n = 0$, so ist $V = \set{0}$ und die Aussage ist richtig.

Ist $n > 0$, so gilt $V \cong \F_p \oplus W$ mit $W = (\F_p)^{n-1}$. Laut
Induktionsvoraussetzung ist $\sum_{w \in W} w = 0$. Für $a \in \F_p$ gilt
$$\sum_{w\in W} a = \sum_{i=1}^{\abs{W}} a = \sum_{i=1}^{p^{n-1}} a = p^{n-1}a =
0.$$
Dies zeigt
$$\sum_{v\in V} v = \sum_{v\in \F_p \oplus W} v = \sum_{a \in \F_p} \sum_{w \in
W} (a, w) = \sum_{a\in\F_p} \left( \sum_{w\in W} a, \sum_{w\in W} w \right)
= \sum_{a \in \F_p} (0, 0) = 0$$
und wir sind fertig. \qed

---

(a) Das Polynom $f = x^6 + x + 1 \in \F_2[x]$ ist irreduzibel; es hat über
    $\F_2$ keine Nullstelle und die laut A&Z II, Blatt 4, Aufgabe 6, einzigen
    in $\F_2[x]$ irreduziblen Polynome ($x^2+x+1, x^3+x+1, x^3+x^2+1$) teilen
    $f$ nicht.

    Daher ist $K = \F_2[x] / \ideal{f}$ ein Körper und 6-dimensionaler
    $\F_2$-Vektorraum und hat damit $2^6 = 64$ Elemente. Seine Einheitengruppe
    $H = K^\times$ enthält 63 Elemente und ist zyklisch und daher isomorph zu
    $G$.

    Der Körper $L = K(x)$ ist unendlich, enthält aber $K$ und damit auch $H
    \cong G$.

(b) Im Fall $G = K^\times$ kann man das Lemma verwenden. Andernfalls vielleicht
    auch. Keine Ahnung.


## Aufgabe 5

Einfache Aufgaben wie die Konstruktion des Mittelpunkts einer Strecke oder das
Aneinanderhängen zweier Strecken werden als gegeben vorausgesetzt.

Das Dreieck habe die Eckpunkte $A, B, C$ und die Strecken $[BC], [AC], [AB]$.

(1) Fälle das Lot von $C$ auf die Gerade $AB$. Der Lotfußpunkt heiße $D$. Dann
    ist $h_{[AB]} = [CD]$ die Höhe auf $[AB]$.

(2) Konstruiere den Mittelpunkt $M$ von $[AB]$. Ein Rechteck mit den
    Seitenlängen $\overline{MB} = \frac{1}{2} \cdot \overline{AB}$ und
    $\overline{CD}$ hat den gleichen Flächeninhalt wie $\triangle ABC$.

(3) Erweitere die Strecke $[MB]$ um die Länge $\overline{CD}$ zu der Strecke
    $[ME]$. Es gilt $\overline{ME} = \overline{MB} + \overline{CD}$.

(4) Konstruiere einen Halbkreis über $[ME]$.

(5) Konstruiere die Gerade $g$ durch $B$, die senkrecht auf $[ME]$ steht. Der
    Schnittpunkt von $g$ mit dem Halbkreis heiße $F$. Nach dem Satz des Thales
    ist $\triangle MEF$ ein rechtwinkliges Dreieck. Die Strecke $[BF]$ ist die
    Höhe der Hypotenuse $[ME]$. Nach dem Höhensatz gilt $\overline{BF} =
    \sqrt{\overline{MB} \cdot \overline{BE}}$.

(6) Konstruiere ein Quadrat mit der Seitenlänge $\overline{BF}$. Es besitzt den
    Flächeninhalt $\overline{MB} \cdot \overline{BE} = \overline{MB} \cdot
    \overline{CD}$, der nach (2) dem Flächeninhalt von $\triangle ABC$
    entspricht.

Der Leser möge nachsehen, dass mangels Zeit keine veranschaulichende
\textsc{Tikz}-Grafik erstellt wurde.
