# Algebra und Zahlentheorie -- Übungsblatt 2

## Aufgabe 1

(a) Für $\sigma, \tau \in U$ gilt offenbar $\sigma\tau(1) = 1$ und
    $\sigma^{-1}(1) = 1$, also $\sigma\tau, \sigma^{-1} \in U$. Damit ist $U$
    eine Untergruppe.

    Es gilt $(2\;3) \in U$, aber $(1\;2)(2\;3)(1\;2) = (1\;3) \notin U$, und
    damit $(1\;2)U(1\;2) \not\subseteq U$. Somit ist $U$ kein Normalteiler in
    $S_4$.

(b) Die Untergruppe $\ideal{(1\;3), (1\;2\;3\;4)} \cong D_4$ besitzt acht
    Elemente.

(c) Wegen $\abs{S_4} = 24 = 2^3 \cdot 3$ haben die 3-Sylowuntergruppen von $S_4$
    genau drei Elemente. Eine solche Untergruppe ist $\ideal{(1\;2\;3)} =
    \set{\Id, (1\;2\;3), (1\;3\;2)}$.

(d) Die Gruppe $U = \ideal{(1\;2)(3\;4), (1\;3)(2\;4), (1\;4)(2\;3)} \cong V_4$
    ist ein Normalteiler in $A_4$, weil sie die Vereinigung zweier
    Konjugationsklassen (Doppeltranspositionen und Identität) ist.

    Wegen $\abs{A_4 / U} = \frac{12}{4} = 3$ ist $A_4 / U \cong C_3$ abelsch.
    Ferner sind $U / \set{\Id} \cong U \cong V_4$ und $S_4 / A_4 \cong C_2$
    abelsch. Somit ist die Kette $\set{\Id} \triangleleft U \triangleleft A_4
    \triangleleft S_4$ eine Auflösung von $S_4$.

## Aufgabe 2

\paragraph{Lemma:} Seien $G$ und $H$ abelsche Gruppen und $\Phi \colon H \to
\Aut(G)$ ein Homomorphismus. Dann ist $G \rtimes_\Phi H$ abelsch genau dann,
wenn $\Phi$ trivial ist. \vspace{-.5cm}
\paragraph{Beweis.} Ist $\Phi$ trivial, so ist das Produkt direkt und daher
abelsch. Ist $\Phi$ nicht trivial, also $\Phi(h) \ne \Id_G$ für ein $h \in H$,
so gibt es ein $g \in G$ mit $\Phi(h)(g) \ne g$. Damit gilt
$$(g, h) *_\Phi (e, h) = (g + \Phi(h)(e), 2h) = (g, 2h) \ne
(\Phi(h)(g), 2h) = (e + \Phi(h)(g), 2h)= (e, h) *_\Phi (g, h)$$
und $G \rtimes_\Phi H$ ist nicht abelsch. \qed

---

(a) Sei $G$ eine Gruppe der Ordnung 333. Für die Anzahl $s_{37}$ der
    37-Sylowuntergruppen von $G$ gilt $s_{37} \mid 9$ und $s_{37} \in 37\Z + 1$
    nach dem dritten Satz von Sylow (Satz 23.11). Dies zeigt $s_{37} = 1$ und
    die eindeutige 37-Sylowuntergruppe $U_{37}$ ist als einziges Element ihrer
    Konjugationsklasse (zweiter Satz von Sylow, Satz 23.9) ein nicht-trivialer
    Normalteiler von $G$.

(b) Die Gruppen $C_3 \times C_3$ und $C_9$ sind nicht isomorph. Damit sind auch
    $G \coloneqq C_3 \times C_3 \times C_{37}$ und $C_{333} \cong C_9 \times
    C_{37}$ nicht isomorph. Damit ist $G$ mit $\abs{G} = 333$ abelsch, aber
    nicht zyklisch.

(c) Wegen $\Aut(C_{37}) \cong C_{36}$ (bewiesen im ersten Teil der Vorlesung,
    Blatt 11, Aufgabe 2) gibt es ein $\vfi \in \Aut(C_{37})$ der Ordnung 3.
    Ferner gilt $C_3 \oplus C_3 = \ideal{e_1, e_2}$. Damit ist der
    Homomorphismus
    $$\Phi \colon (C_3 \oplus C_3) \to \Aut(C_{37}), \qquad e_1 \mapsto \vfi,
    \qquad e_2 \mapsto \Id_{C_{37}}$$
    wohldefiniert und nicht trivial. Nach dem Lemma ist dann $C_{37}
    \rtimes_\Phi (C_3 \oplus C_3)$ nicht abelsch.

\newpage


## Aufgabe 3

(a) Gäbe es einen Homomorphismus $\vfi \colon \Z /3\Z \to \Z / 9\Z$ mit $\pi
    \circ \vfi = \Id_{\Z / 3\Z}$, so wäre $\pi(\vfi(1 + 3\Z)) = 1 + 3\Z$, also
    $\vfi(1 + 3\Z) \in \pi^{-1}(1 + 3\Z) = \set{1+9\Z, 4+9\Z, 7+9\Z}$.

    Da $\Z / 3\Z$ als Körper nur zwei Ideale besitzt, wäre $\vfi$ wäre entweder
    die Nullabbildung oder injektiv. Wegen $\pi \circ \vfi = \Id_{\Z / 3\Z}$ ist
    die Nullabbildung ausgeschlossen. Somit wäre $\vfi$ injektiv, also $\ord_{\Z
    / 9}(\vfi(1 + 3\Z)) = \ord_{\Z / 3\Z}(1 + 3\Z) = 3$, und damit
    $$\vfi(1+3\Z) \in \set{3 + 9\Z, 6 + 9\Z},$$
    im Widerspruch zu $\vfi(1 + 3\Z) \in \set{1+9\Z, 4+9\Z, 7+9\Z}$.

(b) Für die Anzahl $s_7$ der 7-Sylowuntergruppen von $G$ gilt nach dem dritten
    Satz von Sylow $s_7 \in \set{1, 2, 4}$ und $s_7 \equiv 1 \pmod{7}$, also
    $s_7 = 1$. Somit ist die eindeutige 7-Sylowuntergruppe $P$ normal in $G$.

(c) Statt einer Antwort mal wieder ein

    \paragraph{Kleiner Scherz.} Präsident der Universität zu den Physikern:
    "Warum braucht ihr immer so viel Geld? Labore, Forschungsreaktor, so viel
    teure Ausstattung! Warum könnt ihr nicht wie die Mathematiker sein, die
    brauchen nur Stifte, Papier und Mülleimer! Oder noch besser, wie die
    Philosophen, die brauchen nur Stifte und Papier."

## Aufgabe 4

Das ist der Inhalt von Satz 24.13.

\newpage


## Aufgabe 5

\paragraph{Lemma 1:} In einer Gruppe $G$
gebe es genau die $q$-Sylowuntergruppen $U_1, \dots, U_k$, die jeweils die
Ordnung $q^2$ besitzen. Dann gilt $\abs{\bigcup_{i=1}^k U_i} \geqslant
kq(q-1) + q$. \vspace{-.5cm}
\paragraph{Beweis.} Betrachte $V \coloneqq \bigcap_{i=1}^k U_k$. Da die $U_1,
\dots, U_k$ paarweise verschieden sind, ist $V$ eine echte Untergruppe der $U_i$
und es ist $\abs{V} \in \set{1, q}$.

* Im Fall $\abs{V} = q$ sind die Mengen $U_1 \setminus V, \dots, U_k \setminus
  V$ paarweise disjunkt: Sind $i, j \in \set{1, \dots, k}$ und $a \in (U_i
  \setminus V) \cap (U_j \setminus V) = (U_i \cap U_j \setminus V)$, so haben
  wir
  $$V \subsetneq V \cup \set{a} \subseteq U_i \cap U_j.$$
  Für die Schnittgruppe $U_i \cap U_j$ gilt damit $\abs{U_i \cap U_j} > \abs{V}
  = q$, also $\abs{U_i \cap U_j} = q^2$. Dies zeigt $U_i = U_j$ und $i = j$.

  Insgesamt erhalten wir
  $$\abs{\bigcup_{i=1}^k U_i} = \abs{\left(\bigcupdot_{i=1}^k (U_i \setminus
  V)\right) \cupdot V} = \left(\sum_{i=1}^{k} q^2 - q\right) + q = kq(q-1) +
  q.$$

* Im Fall $\abs{V} = 1$ ist $k \geqslant 2$. Betrachte für $i \in \set{1, \dots,
  k}$ die Menge $A_i \coloneqq U_i \setminus \bigcup_{j \ne i} U_j$ der
  Elemente, die nur in $U_i$ vorkommen. Da $U_i$ mit den anderen
  $q$-Sylowuntergruppen nur genau $q$ Elemente oder genau ein Element gemeinsam
  haben kann, gilt $\abs{A_i} = \set{q^2 - q, q^2 - 1}$.

  Gilt $\abs{A_i} = q^2 - 1$ für alle $i \in \set{1, \dots, k}$, so gilt wegen
  $k \geqslant 2$ und $q \geqslant 2$ die Ungleichung $kq - q = (k-1)q \geqslant
  k$ und daher
  $$kq(q-1) + q = kq^2 - kq + q \leqslant kq^2 - k = k(q^2-1)\abs{\left(
  \bigcupdot_{i=1}^k A_i \right) \cupdot \set{e}} \leqslant \abs{\bigcup_{i=1}^k
  U_i}.$$

  Gilt dagegen $\abs{A_i} = q^2 - q$ für ein $i \in \set{1, \dots, k}$, so hat
  $U_i$ mit mindestens einer anderen der $q$-Sylowuntergruppen einer
  $q$-elementige Schnittmenge $B$. Nun haben wir
  $$kq(q-1) + q \leqslant \left(\sum_{i=1}^{k} \abs{A_i}\right) + \abs{B} =
  \abs{\left(\bigcupdot_{i=1}^k A_i\right) \cupdot B} \leqslant
  \abs{\bigcup_{i=1}^k U_i}.$$
  Und so nimmt auch dieser schlimme Beweis sein lang erwartetes Ende. \qed

\paragraph{Lemma 2:} Eine Gruppe mit $pq^2$ Elementen hat eine nicht-triviale
Sylowuntergruppe als Normalteiler. \vspace{-.5cm}
\paragraph{Beweis.} Sei $s_q$ die Anzahl der
$q$-Sylowuntergruppen von $G$. Nach dem dritten Satz von Sylow gilt $s_q
\mid p$, also $s_q \in \set{1, p}$. Gilt $s_q = 1$, so ist die eindeutige
$q$-Sylowuntergruppe normal in $G$. Im Fall $s_q = p$ seien $U_1, \dots, U_p$
die $q$-Sylowuntergruppen von $G$.

Für die Anzahl $s_p$ der $p$-Sylowuntergruppen von $G$ gilt $s_p \mid q^2$,
also $s_p \in \set{1, q, q^2}$. Laut Lemma 1 gilt
$$G \setminus \left( \bigcup_{i=1}^p U_i \right) \leqslant pq^2 -
(pq(q-1)+q) = pq-q = q(p-1) < pq - 1.$$
Die $p$-Sylowuntergruppen dürfen zusammen und inklusive neutrales Element
nicht einmal $pq$ Elemente haben. Daher gilt $s_p = 1$ und die eindeutige
$p$-Sylowuntergruppe ist ein nicht-trivialer Normalteiler von $G$. \qed

---

(a) Siehe Lemma 2.

(b) Sei $G$ eine Gruppe mit $pq^2$ Elementen. Laut Lemma 2 besitzt $G$ eine
    Sylowuntergruppe $U$ als nicht-trivialen Normalteiler. Die Gruppen $U$ und
    $G / U$ sind nach Satz 24.13 als endliche Primärgruppen auflösbar. Damit ist
    laut Korollar 24.11 auch $G$ auflösbar.
